import subprocess


def main():
    MYPY = get_prog("python-mypy", "mypy")
    BLACK = get_prog("python-black", "black")
    ISORT = get_prog("python-isort", "isort")

    subprocess.run([MYPY, "-p", "root", "--show-column-numbers"], check=True)
    subprocess.run([BLACK, "."], check=True)
    subprocess.run([ISORT, "--py", "36", "."], check=True)


def get_prog(pkg, prog):
    return subprocess.run(
        ["devpacks", "pkg-path", pkg, "-p", prog],
        encoding="utf-8",
        check=True,
        stdout=subprocess.PIPE,
    ).stdout.strip()


if __name__ == "__main__":
    main()
