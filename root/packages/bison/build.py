import os
import textwrap
from collections import OrderedDict
from pathlib import Path
from typing import Dict, NamedTuple, Type

from ...lib.main_driver import main_driver
from ...lib.test import BuildContext, PackageDescr, PackageTestCase


class BuildConfig(NamedTuple):
    make_version: str
    gcc_version: str


build_configs: Dict[str, BuildConfig] = OrderedDict(
    [("3.0.4", BuildConfig(make_version="4.1", gcc_version="4.9.4")),],
)


def make_pkg(ctx: BuildContext, pkg_version: str) -> None:
    build_config = build_configs[pkg_version]

    make_dir = ctx.devpacks_pkg("make", build_config.make_version)
    if not ctx.use_system_gcc:
        gcc_dir = ctx.devpacks_pkg("gcc", build_config.gcc_version)

    url = f"https://ftp.gnu.org/gnu/bison/bison-{pkg_version}.tar.xz"

    src_dir = ctx.tmp_dir / "src"
    ctx.shell.mkdir(src_dir)

    ctx.download_extract(url, src_dir)

    ctx.shell.env["PATH"] = os.pathsep.join(
        ([] if ctx.use_system_gcc else [str(gcc_dir / "bin")])
        + [str(make_dir / "bin"), ctx.shell.env["PATH"]]
    )

    with ctx.shell.cd(src_dir):
        ctx.shell.run(
            "./configure",
            ["--prefix=" + str(ctx.pkg_dir / "usr"), "--build=x86_64-linux-gnu", "--disable-nls",],
        )
        ctx.shell.run("make", [])
        ctx.shell.run("make", ["install"])

    ctx.shell.rename(ctx.pkg_dir / "usr" / "lib", ctx.pkg_dir / "lib")

    ctx.shell.mkdir(ctx.pkg_dir / "bin")

    ctx.shell.write_file(
        ctx.pkg_dir / "bin" / "bison",
        textwrap.dedent(
            f"""\
            #!/bin/sh
            DIR=`dirname "$0"`
            export BISON_PKGDATADIR="$DIR/../usr/share/bison"
            exec "$DIR"/../usr/bin/bison "$@"
            """
        ),
    )
    os.chmod(ctx.pkg_dir / "bin" / "bison", 0o775)

    # This replaces the original "yacc" script which has an absolute to bison
    ctx.shell.write_file(
        ctx.pkg_dir / "bin" / "yacc",
        textwrap.dedent(
            f"""\
            #!/bin/sh
            DIR=`dirname "$0"`
            exec "$DIR"/bison -y "$@"
            """
        ),
    )
    os.chmod(ctx.pkg_dir / "bin" / "yacc", 0o775)


def create_tests(pkg_dir: Path, pkg_version: str) -> Type[PackageTestCase]:
    class TestCase(PackageTestCase):
        def test_bison(self) -> None:
            self.assert_run_program_first_line(
                pkg_dir, "bison", ["--version"], f"bison (GNU Bison) {pkg_version}"
            )

        def test_yacc(self) -> None:
            self.assert_run_program_first_line(
                pkg_dir, "yacc", ["--version"], f"bison (GNU Bison) {pkg_version}"
            )

        def test_liby_a(self) -> None:
            exists = os.path.isfile(os.path.join(pkg_dir, "lib", "liby.a"))
            self.assertTrue(exists, "liby.a lib file exists")

    return TestCase


pkg = PackageDescr(
    name="bison",
    versions=list(build_configs.keys()),
    make_pkg=make_pkg,
    create_tests=create_tests,
    sha256s={
        "https://ftp.gnu.org/gnu/bison/bison-3.0.4.tar.xz": "a72428c7917bdf9fa93cb8181c971b6e22834125848cf1d03ce10b1bb0716fe1",
    },
)

if __name__ == "__main__":
    main_driver(pkg)
