import distutils.version
import os.path
from collections import OrderedDict
from pathlib import Path
from typing import Dict, NamedTuple, Type

from ...lib.main_driver import main_driver
from ...lib.test import BuildContext, PackageDescr, PackageTestCase


class BuildConfig(NamedTuple):
    gcc_version: str
    make_version: str
    gmp_version: str
    mpfr_version: str
    # TODO libreadline
    # TODO libsigsegv


build_configs: Dict[str, BuildConfig] = OrderedDict(
    [
        (
            "4.0.2",
            BuildConfig(
                gcc_version="7.5.0", make_version="4.1", gmp_version="6.1.2", mpfr_version="4.0.2",
            ),
        ),
        (
            "4.1.4",
            BuildConfig(
                gcc_version="7.5.0", make_version="4.1", gmp_version="6.1.2", mpfr_version="4.0.2",
            ),
        ),
    ],
)


def make_pkg(ctx: BuildContext, pkg_version: str) -> None:
    build_config = build_configs[pkg_version]

    gcc_dir = ctx.devpacks_pkg("gcc", build_config.gcc_version)
    make_dir = ctx.devpacks_pkg("make", build_config.make_version)
    gmp_dir = ctx.devpacks_pkg("gmp", build_config.gmp_version)
    mpfr_dir = ctx.devpacks_pkg("mpfr", build_config.mpfr_version)

    url = f"https://ftp.gnu.org/gnu/gawk/gawk-{pkg_version}.tar.xz"

    src_dir = ctx.tmp_dir / "src"
    ctx.shell.mkdir(src_dir)

    ctx.download_extract(url, src_dir)

    ctx.shell.env["PATH"] = os.path.pathsep.join(
        [str(gcc_dir / "bin"), str(make_dir / "bin"), ctx.shell.env["PATH"],]
    )

    ctx.shell.env["LIBRARY_PATH"] = os.path.pathsep.join(
        [str(gmp_dir / "lib"), str(mpfr_dir / "lib")]
    )

    ctx.shell.env["C_INCLUDE_PATH"] = os.path.pathsep.join(
        [str(gmp_dir / "include"), str(mpfr_dir / "include")]
    )

    with ctx.shell.cd(src_dir):
        ctx.shell.run(
            "./configure", ["--prefix=" + str(ctx.pkg_dir),],
        )
        ctx.shell.run("make", [])
        ctx.shell.run("make", ["install"])


def create_tests(pkg_dir: Path, pkg_version: str) -> Type[PackageTestCase]:
    build_config = build_configs[pkg_version]

    class TestCase(PackageTestCase):
        def test_gawk(self) -> None:
            self.assert_run_program_stdout_substring(
                pkg_dir, "gawk", ["--version"], f"GNU Awk {pkg_version}"
            )

        def test_gawk_mpfr(self) -> None:
            loose_version = distutils.version.LooseVersion(pkg_version).version
            if loose_version < [4, 1, 0]:  # type: ignore
                self.skipTest("gawk added support for MPFR in version 4.1.0")

            self.assert_run_program_stdout_substring(
                pkg_dir, "gawk", ["--version"], f"GNU MPFR {build_config.mpfr_version}"
            )

    return TestCase


pkg = PackageDescr(
    name="gawk",
    versions=list(build_configs.keys()),
    make_pkg=make_pkg,
    create_tests=create_tests,
    sha256s={
        "https://ftp.gnu.org/gnu/gawk/gawk-4.0.2.tar.xz": "21e1f28c51b5160f0a4bf1a735c6109b46a3bd6a43de808eabc21c17bb026d13",
        "https://ftp.gnu.org/gnu/gawk/gawk-4.1.4.tar.xz": "53e184e2d0f90def9207860531802456322be091c7b48f23fdc79cda65adc266",
    },
)

if __name__ == "__main__":
    main_driver(pkg)
