import distutils.version
import filecmp
import glob
import os.path
import subprocess
import textwrap
from collections import OrderedDict
from pathlib import Path
from typing import Dict, NamedTuple, Optional, Type

from ...lib.main_driver import main_driver
from ...lib.test import BuildContext, PackageDescr, PackageTestCase
from ...lib.tmp_dir import TmpDir


class BuildConfig(NamedTuple):
    make_version: str
    zip_version: str
    unzip_version: str
    gmp_version: str
    mpfr_version: str
    mpc_version: str
    isl_version: str
    cloog_version: Optional[str]
    glibc_version: str
    binutils_version: str
    gcc_version: str


build_configs: Dict[str, BuildConfig] = OrderedDict(
    [
        (
            "4.9.4",  # 2016-08-03
            BuildConfig(
                # Library versions are taken from the file
                # "contrib/download_prerequisites" in the gcc source tree:
                #
                # <https://gcc.gnu.org/git/?p=gcc.git;a=blob;f=contrib/download_prerequisites;h=98813a02d1d92fe9d37b9646e837d887661704b0;hb=4f18db57daffc62a373e30d93c9552f2b94adf09>
                make_version="3.82",
                zip_version="3.0",
                unzip_version="6.0",
                gmp_version="4.3.2",
                mpfr_version="2.4.2",
                mpc_version="0.8.1",
                isl_version="0.12.2",
                cloog_version="0.18.1",
                glibc_version="2.13",
                binutils_version="2.27",  # 2016-08-03
                gcc_version="4.9.4",
            ),
        ),
        (
            "5.5.0",  # 2017-10-10
            # Library versions are taken from the file
            # "contrib/download_prerequisites" in the gcc source tree:
            #
            # <https://gcc.gnu.org/git/?p=gcc.git;a=blob;f=contrib/download_prerequisites;h=69403301b813a2b99975570145078d5d88deb3fb;hb=926d9947847a0683cf34de6fc231209747576088>
            BuildConfig(
                make_version="3.82",
                zip_version="3.0",
                unzip_version="6.0",
                gmp_version="4.3.2",
                mpfr_version="2.4.2",
                mpc_version="0.8.1",
                isl_version="0.14",
                cloog_version=None,
                glibc_version="2.13",
                binutils_version="2.29.1",  # 2017-09-25
                gcc_version="4.9.4",
            ),
        ),
        (
            "6.5.0",  # 2018-10-26
            # Library versions are taken from the file
            # "contrib/download_prerequisites" in the gcc source tree:
            #
            # <https://gcc.gnu.org/git/?p=gcc.git;a=blob;f=contrib/download_prerequisites;h=1080dded1da086f39190bcfeae0748100302c83b;hb=e31ae982c446804d1b259554203401392b078364>
            BuildConfig(
                make_version="3.82",
                zip_version="3.0",
                unzip_version="6.0",
                gmp_version="4.3.2",
                mpfr_version="2.4.2",
                mpc_version="0.8.1",
                isl_version="0.15",
                cloog_version=None,
                glibc_version="2.13",
                binutils_version="2.31.1",  # 2018-07-18
                gcc_version="5.5.0",
            ),
        ),
        (
            "7.5.0",  # 2019-11-14
            # Library versions are taken from the file
            # "contrib/download_prerequisites" in the gcc source tree:
            #
            # <https://gcc.gnu.org/git/?p=gcc.git;a=blob;f=contrib/download_prerequisites;h=3430ce1f19e3235b4dc98960b858238bc754ad53;hb=b2d961e7342b5ba4e57adfa81cb189b738d10901>
            BuildConfig(
                make_version="3.82",
                zip_version="3.0",
                unzip_version="6.0",
                gmp_version="6.1.0",
                mpfr_version="3.1.4",
                mpc_version="1.0.3",
                isl_version="0.16.1",
                cloog_version=None,
                glibc_version="2.13",
                binutils_version="2.33.1",  # 2019-10-12
                gcc_version="6.5.0",
            ),
        ),
        (
            "8.4.0",  # 2020-03-04
            # Library versions are taken from the file
            # "contrib/download_prerequisites" in the gcc source tree:
            #
            # <https://gcc.gnu.org/git/?p=gcc.git;a=blob;f=contrib/download_prerequisites;h=b50f47cda79547f0540c7f5b7ee0c2928f5bdd3e;hb=8cd3bffead2ed1d1998c190865694f920fbc93ab>
            BuildConfig(
                make_version="3.82",
                zip_version="3.0",
                unzip_version="6.0",
                gmp_version="6.1.0",
                mpfr_version="3.1.4",
                mpc_version="1.0.3",
                isl_version="0.18",
                cloog_version=None,
                glibc_version="2.13",
                binutils_version="2.34",  # 2020-02-01
                gcc_version="7.5.0",
            ),
        ),
        (
            "9.3.0",  # 2020-03-12
            # Library versions are taken from the file
            # "contrib/download_prerequisites" in the gcc source tree:
            #
            # <https://gcc.gnu.org/git/?p=gcc.git;a=blob;f=contrib/download_prerequisites;h=72976c46c92c920b30e658bc72aa5c13cacc189b;hb=4212a6a3e44f870412d9025eeb323fd4f50a61da>
            BuildConfig(
                make_version="3.82",
                zip_version="3.0",
                unzip_version="6.0",
                gmp_version="6.1.0",
                mpfr_version="3.1.4",
                mpc_version="1.0.3",
                isl_version="0.18",
                cloog_version=None,
                glibc_version="2.13",
                binutils_version="2.34",  # 2020-02-01
                gcc_version="8.4.0",
            ),
        ),
        (
            "10.2.0",  # 2020-07-23
            # Library versions are taken from the file
            # "contrib/download_prerequisites" in the gcc source tree:
            #
            # <https://gcc.gnu.org/git/?p=gcc.git;a=blob;f=contrib/download_prerequisites;h=aa0356e62665a8fcd96eba59568d91d5b7f419e0;hb=ee5c3db6c5b2c3332912fb4c9cfa2864569ebd9a>
            BuildConfig(
                make_version="3.82",
                zip_version="3.0",
                unzip_version="6.0",
                gmp_version="6.1.0",
                mpfr_version="3.1.4",
                mpc_version="1.0.3",
                isl_version="0.18",
                cloog_version=None,
                glibc_version="2.13",
                binutils_version="2.34",  # 2020-02-01
                gcc_version="9.3.0",
            ),
        ),
    ]
)


def make_pkg(ctx: BuildContext, pkg_version: str) -> None:
    build_config = build_configs[pkg_version]

    make_dir = ctx.devpacks_pkg("make", build_config.make_version)
    zip_dir = ctx.devpacks_pkg("zip", build_config.zip_version)
    unzip_dir = ctx.devpacks_pkg("unzip", build_config.unzip_version)
    glibc_dir = ctx.devpacks_pkg("glibc", build_config.glibc_version)
    binutils_dir = ctx.devpacks_pkg("binutils", build_config.binutils_version)
    if not ctx.use_system_gcc:
        gcc_dir = ctx.devpacks_pkg("gcc", build_config.gcc_version)

    url = f"http://ftp.gnu.org/gnu/gcc/gcc-{pkg_version}/gcc-{pkg_version}.tar.gz"

    build_dir = ctx.tmp_dir / "build"
    ctx.shell.mkdir(build_dir)

    ctx.download_extract(url, build_dir)

    if distutils.version.LooseVersion(build_config.gmp_version) < distutils.version.LooseVersion(
        "5.0.0"
    ):
        gmp_url = f"https://gmplib.org/download/gmp/archive/gmp-{build_config.gmp_version}.tar.xz"
    else:
        gmp_url = f"https://gmplib.org/download/gmp/gmp-{build_config.gmp_version}.tar.xz"
    ctx.shell.mkdir(build_dir / "gmp")
    ctx.download_extract(gmp_url, build_dir / "gmp")

    mpfr_url = f"https://www.mpfr.org/mpfr-{build_config.mpfr_version}/mpfr-{build_config.mpfr_version}.tar.gz"
    ctx.shell.mkdir(build_dir / "mpfr")
    ctx.download_extract(mpfr_url, build_dir / "mpfr")

    if build_config.mpc_version == "0.8.1":
        mpc_url = (
            f"https://gcc.gnu.org/pub/gcc/infrastructure/mpc-{build_config.mpc_version}.tar.gz"
        )
    else:
        mpc_url = f"https://ftp.gnu.org/gnu/mpc/mpc-{build_config.mpc_version}.tar.gz"
    ctx.shell.mkdir(build_dir / "mpc")
    ctx.download_extract(mpc_url, build_dir / "mpc")

    isl_url = f"http://isl.gforge.inria.fr/isl-{build_config.isl_version}.tar.gz"
    ctx.shell.mkdir(build_dir / "isl")
    ctx.download_extract(isl_url, build_dir / "isl")

    if build_config.cloog_version:
        cloog_url = (
            f"http://www.bastoul.net/cloog/pages/download/cloog-{build_config.cloog_version}.tar.gz"
        )
        ctx.shell.mkdir(build_dir / "cloog")
        ctx.download_extract(cloog_url, build_dir / "cloog")

    ctx.shell.mkdir(ctx.pkg_dir / "sysroot")
    ctx.shell.copytree(glibc_dir, ctx.pkg_dir / "sysroot" / "usr")

    with ctx.shell.cd(build_dir):

        # Some of these configure flags were inspired from:
        # <https://github.com/theopolis/build-anywhere/>
        #
        # To see which flags they used, download the release, and run `gcc -v"
        #
        # For the record, here is the output of "v5 (Jun 29, 2019)":
        #
        #     $ x86_64-anywhere-linux-gnu-gcc -v
        #     Using built-in specs.
        #     COLLECT_GCC=./x86_64-anywhere-linux-gnu/bin/x86_64-anywhere-linux-gnu-gcc
        #     COLLECT_LTO_WRAPPER=/media/wd/wutu/tmp/x86_64-anywhere-linux-gnu/bin/../libexec/gcc/x86_64-anywhere-linux-gnu/8.3.0/lto-wrapper
        #     Target: x86_64-anywhere-linux-gnu
        #     Configured with: /opt/build-anywhere/crosstool-ng/.build/x86_64-anywhere-linux-gnu/src/gcc/configure --build=x86_64-build_pc-linux-gnu --host=x86_64-build_pc-linux-gnu --target=x86_64-anywhere-linux-gnu --prefix=/opt/build-anywhere/x86_64-anywhere-linux-gnu --with-sysroot=/opt/build-anywhere/x86_64-anywhere-linux-gnu/x86_64-anywhere-linux-gnu/sysroot --enable-languages=c,c++ --with-pkgversion='crosstool-NG 1.24.0.6-afaf7b9' --enable-__cxa_atexit --disable-libmudflap --disable-libgomp --disable-libssp --disable-libquadmath --disable-libquadmath-support --disable-libsanitizer --enable-libmpx --with-gmp=/opt/build-anywhere/crosstool-ng/.build/x86_64-anywhere-linux-gnu/buildtools --with-mpfr=/opt/build-anywhere/crosstool-ng/.build/x86_64-anywhere-linux-gnu/buildtools --with-mpc=/opt/build-anywhere/crosstool-ng/.build/x86_64-anywhere-linux-gnu/buildtools --with-isl=/opt/build-anywhere/crosstool-ng/.build/x86_64-anywhere-linux-gnu/buildtools --disable-lto --with-host-libstdcxx='-static-libgcc -Wl,-Bstatic,-lstdc++ -lm' --enable-threads=posix --enable-target-optspace --disable-plugin --disable-nls --disable-multilib --with-local-prefix=/opt/build-anywhere/x86_64-anywhere-linux-gnu/x86_64-anywhere-linux-gnu/sysroot --enable-long-long
        #     Thread model: posix
        #     gcc version 8.3.0 (crosstool-NG 1.24.0.6-afaf7b9)

        configure_flags = [
            "--prefix=" + str(ctx.pkg_dir),
            "--build=x86_64-linux-gnu",
            "--host=x86_64-linux-gnu",
            "--target=x86_64-linux-gnu",
            "--with-sysroot=" + str(ctx.pkg_dir / "sysroot"),
            "--with-local-prefix=" + str(ctx.pkg_dir / "sysroot"),
            "--enable-languages=c,c++",
            "--enable-__cxa_atexit",
            "--disable-libmudflap",
            "--disable-libgomp",
            "--disable-libssp",
            "--disable-libquadmath",
            "--disable-libquadmath-support",
            "--disable-libsanitizer",
            "--enable-lto",
            "--enable-threads=posix",
            "--disable-plugin",
            "--disable-nls",
            "--disable-multilib",
            "--enable-long-long",
            "--enable-host-shared",
        ]

        ctx.shell.env["PATH"] = os.pathsep.join(
            ([] if ctx.use_system_gcc else [str(gcc_dir / "bin")])
            + [
                str(make_dir / "bin"),
                str(zip_dir / "bin"),
                str(unzip_dir / "bin"),
                ctx.shell.env["PATH"],
            ]
        )

        ctx.shell.run("./configure", configure_flags)

        ctx.shell.run("make", [])
        ctx.shell.run("make", ["install"])

    # Delete all "lib64/*.la" files (they have broken absolute paths and should
    # not be used)
    for la_file in glob.glob(str(ctx.pkg_dir / "lib64" / "*.la")):
        ctx.shell.remove(la_file)

    # Move all "lib64/*.so*" files to a separate "lib-dynamic"
    #
    # This enables dynamic linking with "libstdc++" and "libgcc", but only
    # when explicitly requested (by providing the flag
    # -L<path_to>/lib-dynamic)
    ctx.shell.mkdir(ctx.pkg_dir / "lib-dynamic")
    for so_file in glob.glob(str(ctx.pkg_dir / "lib64" / "*.so*")):
        ctx.shell.rename(so_file, ctx.pkg_dir / "lib-dynamic" / os.path.basename(so_file))

    # This is a hack that forces static linking of "libgcc".
    #
    # Normally, gcc (or actually g++) wants to link dynamically with libgcc
    # and automatically uses the flags: "-lgcc -lgcc_s"
    #
    # For static linking of libgcc, we need to use: "-lgcc -lgcc_eh".
    #
    # The official way to do this is to use the flag "--static-libgcc". But we
    # want static linking out-of-the-box.
    #
    # So the hack I came up with is to make it so that when gcc invokes the
    # dynamic libgcc link flags ("-lgcc -lgcc_s") we actually get the
    # behaviour of static linking. This wrapper scripts causes the "-lgcc_s"
    # to redirect to "-lgcc_eh", giving us the desired outcome.
    #
    ctx.shell.write_file(
        ctx.pkg_dir / "lib64" / "libgcc_s.so",
        textwrap.dedent(
            f"""\
            /* This wrapper is a hack to get static linking of libgcc by default  */
            GROUP ( -lgcc_eh )
            """
        ),
    )

    # Copy binutils into the package
    #
    for dir_entry in os.scandir(binutils_dir / "bin"):
        src_file = binutils_dir / "bin" / dir_entry.name
        dst_file = ctx.pkg_dir / "bin" / dir_entry.name

        if os.path.exists(dst_file):
            raise RuntimeError(
                f"binutils bin file {dir_entry.name} already exists inside gcc pkg_dir"
            )

        if dir_entry.is_dir():
            ctx.shell.copytree(src_file, dst_file)
        else:
            ctx.shell.copy(src_file, dst_file)

    if os.path.exists(ctx.pkg_dir / "x86_64-linux-gnu"):
        raise RuntimeError(
            f"binutils directory {'x86_64-linux-gnu'} already exists inside gcc pkg_dir"
        )

    ctx.shell.copytree(binutils_dir / "x86_64-linux-gnu", ctx.pkg_dir / "x86_64-linux-gnu")

    # This is an optional step that saves disk space. We search for duplicate
    # files and convert them into symbolic links
    #
    for dir_entry in os.scandir(ctx.pkg_dir / "x86_64-linux-gnu" / "bin"):
        if dir_entry.is_file():
            file1 = ctx.pkg_dir / "bin" / dir_entry.name
            file2 = ctx.pkg_dir / "x86_64-linux-gnu" / "bin" / dir_entry.name

            if os.path.exists(file1) and filecmp.cmp(str(file1), str(file2), shallow=False):
                ctx.shell.remove(file2)
                ctx.shell.symlink("../../bin/" + str(dir_entry.name), file2)


def create_tests(pkg_dir: Path, pkg_version: str) -> Type[PackageTestCase]:
    class TestCase(PackageTestCase):
        def test_gcc(self):
            self.assert_run_program_first_line(
                pkg_dir, "gcc", ["--version"], f"gcc (GCC) {pkg_version}"
            )

        def test_gcc_compile_hello_world(self) -> None:
            with TmpDir() as tmpdir:
                with open(tmpdir / "hello.c", "w", encoding="utf-8") as f:
                    f.write(
                        textwrap.dedent(
                            """\
                            #include <stdio.h>
                            int main() {
                                printf("Hello World!\\n");
                                return 0;
                            }
                            """
                        )
                    )

                subprocess.run(
                    [pkg_dir / "bin" / "gcc", "hello.c", "-o", "hello"],
                    check=True,
                    cwd=tmpdir,
                    encoding="utf-8",
                    stdout=subprocess.PIPE,
                )

                cmd = subprocess.run(
                    [tmpdir / "hello"], check=True, encoding="utf-8", stdout=subprocess.PIPE,
                )

                self.assertEqual(cmd.stdout, "Hello World!\n")

    return TestCase


pkg = PackageDescr(
    name="gcc",
    versions=list(build_configs.keys()),
    make_pkg=make_pkg,
    create_tests=create_tests,
    sha256s={
        "https://gmplib.org/download/gmp/archive/gmp-4.3.2.tar.xz": "f69eff1bc3d15d4e59011d587c57462a8d3d32cf2378d32d30d008a42a863325",
        "https://gmplib.org/download/gmp/gmp-6.1.0.tar.xz": "68dadacce515b0f8a54f510edf07c1b636492bcdb8e8d54c56eb216225d16989",
        "https://gmplib.org/download/gmp/gmp-6.1.2.tar.xz": "87b565e89a9a684fe4ebeeddb8399dce2599f9c9049854ca8c0dfbdea0e21912",
        "https://www.mpfr.org/mpfr-2.4.2/mpfr-2.4.2.tar.gz": "246d7e184048b1fc48d3696dd302c9774e24e921204221540745e5464022b637",
        "https://www.mpfr.org/mpfr-3.1.4/mpfr-3.1.4.tar.gz": "0d4de7e1476f79d24c38d5bc04a06fcc9a1bb9cf35fd654ceada29af03ad1844",
        "https://www.mpfr.org/mpfr-4.0.2/mpfr-4.0.2.tar.gz": "ae26cace63a498f07047a784cd3b0e4d010b44d2b193bab82af693de57a19a78",
        "https://www.mpfr.org/mpfr-4.1.0/mpfr-4.1.0.tar.gz": "3127fe813218f3a1f0adf4e8899de23df33b4cf4b4b3831a5314f78e65ffa2d6",
        "https://gcc.gnu.org/pub/gcc/infrastructure/mpc-0.8.1.tar.gz": "e664603757251fd8a352848276497a4c79b7f8b21fd8aedd5cc0598a38fee3e4",
        "https://ftp.gnu.org/gnu/mpc/mpc-1.0.3.tar.gz": "617decc6ea09889fb08ede330917a00b16809b8db88c29c31bfbb49cbf88ecc3",
        "https://ftp.gnu.org/gnu/mpc/mpc-1.1.0.tar.gz": "6985c538143c1208dcb1ac42cedad6ff52e267b47e5f970183a3e75125b43c2e",
        "https://ftp.gnu.org/gnu/mpc/mpc-1.2.0.tar.gz": "e90f2d99553a9c19911abdb4305bf8217106a957e3994436428572c8dfe8fda6",
        "http://isl.gforge.inria.fr/isl-0.12.2.tar.gz": "9dcfc146678dd3e8afe5ce67db8279b5cb78b8ce32e444232face190e705b784",
        "http://isl.gforge.inria.fr/isl-0.14.tar.gz": "e744e3efba722fb9376f5d0a48c87229d4f3dc3454d3aa547a88eec01f630fbc",
        "http://isl.gforge.inria.fr/isl-0.15.tar.gz": "02fb67c487df76745b6bae87d3a33d50e00ff14791544eae44fcc076edbf7987",
        "http://isl.gforge.inria.fr/isl-0.16.tar.gz": "6dbfbba4a39a269b0344a21792478d052001135fdce08a693b6d83effa43da72",
        "http://isl.gforge.inria.fr/isl-0.16.1.tar.gz": "fb34703bd694622aef92164bad983c15ec04274edbe19e614d9ecda54b85603d",
        "http://isl.gforge.inria.fr/isl-0.18.tar.gz": "f81ca9a8ff02c18c78655d023e266fd4177f4dcc057bcab8c14eb48fb6e9c7ef",
        "http://www.bastoul.net/cloog/pages/download/cloog-0.18.1.tar.gz": "02500a4edd14875f94fe84cbeda4290425cb0c1c2474c6f75d75a303d64b4196",
        "http://ftp.gnu.org/gnu/gcc/gcc-4.9.4/gcc-4.9.4.tar.gz": "1680f92781b92cbdb57d7e4f647c650678c594154cb0d707fd9a994424a9860d",
        "http://ftp.gnu.org/gnu/gcc/gcc-5.5.0/gcc-5.5.0.tar.gz": "3aabce75d6dd206876eced17504b28d47a724c2e430dbd2de176beb948708983",
        "http://ftp.gnu.org/gnu/gcc/gcc-6.5.0/gcc-6.5.0.tar.gz": "4eed92b3c24af2e774de94e96993aadbf6761cdf7a0345e59eb826d20a9ebf73",
        "http://ftp.gnu.org/gnu/gcc/gcc-7.5.0/gcc-7.5.0.tar.gz": "4f518f18cfb694ad7975064e99e200fe98af13603b47e67e801ba9580e50a07f",
        "http://ftp.gnu.org/gnu/gcc/gcc-8.4.0/gcc-8.4.0.tar.gz": "41e8b145832fc0b2b34c798ed25fb54a881b0cee4cd581b77c7dc92722c116a8",
        "http://ftp.gnu.org/gnu/gcc/gcc-9.3.0/gcc-9.3.0.tar.gz": "5258a9b6afe9463c2e56b9e8355b1a4bee125ca828b8078f910303bc2ef91fa6",
        "http://ftp.gnu.org/gnu/gcc/gcc-10.2.0/gcc-10.2.0.tar.gz": "27e879dccc639cd7b0cc08ed575c1669492579529b53c9ff27b0b96265fa867d",
    },
)

if __name__ == "__main__":
    main_driver(pkg)
