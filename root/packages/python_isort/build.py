import os.path
import textwrap
from collections import OrderedDict
from pathlib import Path
from typing import Dict, Type

from ...lib.ecosystems.python.pip import PipBuildConfig, make_pip_pkg, pip_sha256s
from ...lib.main_driver import main_driver
from ...lib.test import BuildContext, PackageDescr, PackageTestCase

build_configs: Dict[str, PipBuildConfig] = OrderedDict(
    [
        (
            "5.2.0",
            PipBuildConfig(
                python_version="3.6.8", pip_version="20.1.1", setuptools_version="49.2.0",
            ),
        )
    ]
)


def make_pkg(ctx: BuildContext, pkg_version: str) -> None:
    build_config = build_configs[pkg_version]
    make_pip_pkg(ctx, "isort", pkg_version, build_config)


def create_tests(pkg_dir: Path, pkg_version: str) -> Type[PackageTestCase]:
    class TestCase(PackageTestCase):
        def test_isort(self) -> None:
            out = self.run_program(pkg_dir, "isort", ["--version"])
            self.assertRegex(out, f"VERSION {pkg_version}")

    return TestCase


pkg = PackageDescr(
    name="python_isort",
    versions=list(build_configs.keys()),
    make_pkg=make_pkg,
    create_tests=create_tests,
    sha256s=pip_sha256s,
)

if __name__ == "__main__":
    main_driver(pkg)
