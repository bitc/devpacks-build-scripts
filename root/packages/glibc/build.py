import os.path
import subprocess
import textwrap
from collections import OrderedDict
from pathlib import Path
from typing import Dict, NamedTuple, Type

from ...lib.main_driver import main_driver
from ...lib.test import BuildContext, PackageDescr, PackageTestCase


class BuildConfig(NamedTuple):
    make_version: str
    gcc_version: str
    gawk_version: str
    linux_version: str


# Linux version 4.7 was chosen because that is what is used here:
# <https://github.com/theopolis/build-anywhere>
#
# (and they seem to know what they are doing)

build_configs: Dict[str, BuildConfig] = OrderedDict(
    [
        (
            "2.13",
            BuildConfig(
                make_version="3.82",
                gcc_version="4.9.4",
                gawk_version="4.0.2",
                linux_version="4.7.9",
            ),
        ),
        # (
        #     "2.19",
        #     BuildConfig(
        #         make_version="4.1",
        #         gcc_version="4.9.4",
        #         gawk_version="4.0.2",
        #         linux_version="4.7.9"
        #     ),
        # ),
    ]
)


def make_pkg(ctx: BuildContext, pkg_version: str) -> None:
    build_config = build_configs[pkg_version]

    make_dir = ctx.devpacks_pkg("make", build_config.make_version)
    gawk_dir = ctx.devpacks_pkg("gawk", build_config.gawk_version)
    if not ctx.use_system_gcc:
        gcc_dir = ctx.devpacks_pkg("gcc", build_config.gcc_version)

    url = f"https://ftp.gnu.org/gnu/glibc/glibc-{pkg_version}.tar.xz"

    linux_url = (
        f"https://cdn.kernel.org/pub/linux/kernel/v4.x/linux-{build_config.linux_version}.tar.xz"
    )

    src_dir = ctx.tmp_dir / "src"
    build_dir = ctx.tmp_dir / "build"
    ctx.shell.mkdir(src_dir)
    ctx.shell.mkdir(build_dir)

    ctx.download_extract(url, src_dir)

    if pkg_version == "2.13":
        print("Applying patch file patch-glibc-2.13-obstack.patch")
        with open(
            os.path.join(
                os.path.dirname(os.path.realpath(__file__)), "patch-glibc-2.13-obstack.patch"
            ),
            "r",
        ) as f:
            subprocess.run(["patch", "-p1"], stdin=f, cwd=src_dir, check=True)

    ctx.shell.mkdir(ctx.tmp_dir / "linux-src")
    ctx.download_extract(linux_url, ctx.tmp_dir / "linux-src")

    ctx.shell.env["PATH"] = os.pathsep.join(
        ([] if ctx.use_system_gcc else [str(gcc_dir / "bin")])
        + [str(make_dir / "bin"), str(gawk_dir / "bin"), ctx.shell.env["PATH"]]
    )

    with ctx.shell.cd(ctx.tmp_dir / "linux-src"):
        ctx.shell.run("make", ["headers_install", "INSTALL_HDR_PATH=" + str(ctx.pkg_dir)])

    with ctx.shell.cd(build_dir):
        configure_flags = [
            "--prefix=" + str(ctx.pkg_dir),
            "--host=x86_64-linux-gnu",
            # TODO Figure out how to get these flags working so that we use
            # the linux headers that we downloaded, instead of the native ones
            # from the build machine. (Note that we are currently correctly
            # installing the downloaded linux headers into pkg_dir)
            #
            # "--with-headers=" + str(ctx.pkg_dir),
            # "--enable-kernel=" + build_config.linux_version,
        ]

        ctx.shell.run("../src/configure", configure_flags)

        ctx.shell.run("make", [])
        ctx.shell.run("make", ["install"])

    # The paths end up being wrong in the following files. I couldn't find a
    # way to get them to be created correctly, so the easiest thing is to just
    # overwrite them here.
    #
    # The two files are: "lib/libc.so" and "lib/libpthread.so"
    #
    # Hopefully different glibc versions won't introduce any more of these (or
    # change the contents[*])
    #
    # [*] Thankfully, we don't have to worry about this so much, since the
    # plan is to only have a single version of glibc (as ancient as possible)
    # in devpacks

    ctx.shell.write_file(
        ctx.pkg_dir / "lib" / "libc.so",
        textwrap.dedent(
            f"""\
            /* GNU ld script
               Use the shared library, but some functions are only in
               the static library, so try that secondarily.  */
            OUTPUT_FORMAT(elf64-x86-64)
            GROUP ( libc.so.6 libc_nonshared.a  AS_NEEDED ( ld-linux-x86-64.so.2 ) )
            """
        ),
    )

    ctx.shell.write_file(
        ctx.pkg_dir / "lib" / "libpthread.so",
        textwrap.dedent(
            f"""\
            /* GNU ld script
               Use the shared library, but some functions are only in
               the static library, so try that secondarily.  */
            OUTPUT_FORMAT(elf64-x86-64)
            GROUP ( libpthread.so.0 libpthread_nonshared.a )
            """
        ),
    )


def create_tests(pkg_dir: Path, pkg_version: str) -> Type[PackageTestCase]:
    class TestCase(PackageTestCase):
        def test_stdio_h(self) -> None:
            exists = os.path.isfile(os.path.join(pkg_dir, "include", "stdio.h"))
            self.assertTrue(exists, "stdio.h header file exists")

        def test_socket_h(self) -> None:
            exists = os.path.isfile(os.path.join(pkg_dir, "include", "sys", "socket.h"))
            self.assertTrue(exists, "socket.h header file exists")

        def test_libc_a(self) -> None:
            exists = os.path.isfile(os.path.join(pkg_dir, "lib", "libc.a"))
            self.assertTrue(exists, "libc.a lib file exists")

        def test_libm_a(self) -> None:
            exists = os.path.isfile(os.path.join(pkg_dir, "lib", "libm.a"))
            self.assertTrue(exists, "libm.a lib file exists")

    return TestCase


pkg = PackageDescr(
    name="glibc",
    versions=list(build_configs.keys()),
    make_pkg=make_pkg,
    create_tests=create_tests,
    sha256s={
        "https://cdn.kernel.org/pub/linux/kernel/v4.x/linux-4.7.9.tar.xz": "0638d52a86079d726817d895ae4030b09e3e649222a9590603dd7771155ae28d",
        "https://ftp.gnu.org/gnu/glibc/glibc-2.13.tar.xz": "98ee47fc77b01d5805e2cad3ed8e5515220ff32626a049dff3d5529b8f8bdcc1",
        "https://ftp.gnu.org/gnu/glibc/glibc-2.19.tar.xz": "2d3997f588401ea095a0b27227b1d50cdfdd416236f6567b564549d3b46ea2a2",
    },
)

if __name__ == "__main__":
    main_driver(pkg)
