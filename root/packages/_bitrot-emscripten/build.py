# Build script inspired by:
#
#   <https://github.com/NixOS/nixpkgs/blob/master/pkgs/development/compilers/emscripten-fastcomp/default.nix>
#   <https://github.com/NixOS/nixpkgs/blob/master/pkgs/development/compilers/emscripten/default.nix>

import os
import os.path
import shutil
import subprocess
import textwrap
import unittest

import pkglib


class Package(pkglib.PackageBuilder):
    name = "emscripten"
    version = "1.38.12"

    cmake_version = "3.10.1"
    python2_version = "2.7.15"
    node_version = "8.12.0"
    zulu_openjdk_version = "9.0.1.3"

    sha256s = {}

    git_hashes = {
        #          ( kripken/emscripten,                         kripken/emscripten-fastcomp,                kripken/emscripten-fastcomp-clang        )
        "1.37.22": (
            "37b3124e757c1d5b2468d0137f01122fbef9d960",
            "5faa5f48611ec00b918453b42a38500ff60db42a",
            "3659f873b523e5fc89ffa16baab8901fbd084251",
        ),
        "1.37.28": (
            "279e6a00b322f7fed56b3418f1e65645c7ae1985",
            "3f6b90623596cdf2490dbc2338a784e9008c2ebe",
            "29de7f517cc4f0ed17a4123e73844713da6c3430",
        ),
        "1.38.12": (
            "0102bdfc6973acb467e03e35688575849624379c",
            "49340ac23ed3065116323699f0c44498d5f15d37",
            "c0a68f4121de3f6fb2f2a503e89d1b0e5cdf68a9",
        ),
    }

    def make_pkg(self):
        cmake_bin = subprocess.run(
            ["devpacks", "quick-pkg-path", "cmake", "-t", Package.cmake_version, "-p", "cmake",],
            encoding="utf-8",
            check=True,
            stdout=subprocess.PIPE,
        ).stdout

        python2_bin = subprocess.run(
            [
                "devpacks",
                "quick-pkg-path",
                "python",
                "-t",
                Package.python2_version,
                "-p",
                "python",
            ],
            encoding="utf-8",
            check=True,
            stdout=subprocess.PIPE,
        ).stdout
        python2_dir = subprocess.run(
            ["devpacks", "quick-pkg-path", "python", "-t", Package.python2_version],
            encoding="utf-8",
            check=True,
            stdout=subprocess.PIPE,
        ).stdout
        node_dir = subprocess.run(
            ["devpacks", "quick-pkg-path", "node", "-t", Package.node_version],
            encoding="utf-8",
            check=True,
            stdout=subprocess.PIPE,
        ).stdout
        openjdk_dir = subprocess.run(
            ["devpacks", "quick-pkg-path", "zulu-openjdk", "-t", Package.zulu_openjdk_version,],
            encoding="utf-8",
            check=True,
            stdout=subprocess.PIPE,
        ).stdout
        cmake_dir = subprocess.run(
            ["devpacks", "quick-pkg-path", "cmake", "-t", Package.cmake_version],
            encoding="utf-8",
            check=True,
            stdout=subprocess.PIPE,
        ).stdout

        with pkglib.TmpDir() as tmp_dir:
            subprocess.run(
                [
                    "git",
                    "clone",
                    "--branch",
                    self.version,
                    "--depth",
                    "1",
                    "https://github.com/kripken/emscripten.git",
                ],
                cwd=tmp_dir,
                check=True,
            )
            subprocess.run(
                ["git", "checkout", Package.git_hashes[self.version][0]],
                cwd=os.path.join(tmp_dir, "emscripten"),
                check=True,
            )

            subprocess.run(
                [
                    "git",
                    "clone",
                    "--branch",
                    self.version,
                    "--depth",
                    "1",
                    "https://github.com/kripken/emscripten-fastcomp.git",
                ],
                cwd=tmp_dir,
                check=True,
            )
            subprocess.run(
                ["git", "checkout", Package.git_hashes[self.version][1]],
                cwd=os.path.join(tmp_dir, "emscripten-fastcomp"),
                check=True,
            )

            subprocess.run(
                [
                    "git",
                    "clone",
                    "--branch",
                    self.version,
                    "--depth",
                    "1",
                    "https://github.com/kripken/emscripten-fastcomp-clang.git",
                ],
                cwd=tmp_dir,
                check=True,
            )
            subprocess.run(
                ["git", "checkout", Package.git_hashes[self.version][2]],
                cwd=os.path.join(tmp_dir, "emscripten-fastcomp-clang"),
                check=True,
            )

            for f in os.listdir(os.path.join(tmp_dir, "emscripten")):
                if os.path.isdir(os.path.join(tmp_dir, "emscripten", f)):
                    shutil.copytree(
                        os.path.join(tmp_dir, "emscripten", f), os.path.join(self.pkg_dir, f),
                    )
                else:
                    shutil.copy(os.path.join(tmp_dir, "emscripten", f), self.pkg_dir)

            shutil.copytree(
                python2_dir,
                os.path.join(self.pkg_dir, pkglib.package_slug("python", Package.python2_version)),
                symlinks=True,
            )
            shutil.copytree(
                node_dir,
                os.path.join(self.pkg_dir, pkglib.package_slug("node", Package.node_version)),
                symlinks=True,
            )
            shutil.copytree(
                openjdk_dir,
                os.path.join(
                    self.pkg_dir, pkglib.package_slug("zulu-openjdk", Package.zulu_openjdk_version),
                ),
                symlinks=True,
            )

            # Patch emscripten runtime config:
            def patch_config_file(config_file):
                with open(config_file, "r", encoding="utf-8") as f:
                    lines = f.readlines()

                if len([l for l in lines if l.strip() == "EM_CONFIG = '~/.emscripten'"]) != 1:
                    raise RuntimeError(
                        f"Couldn't find location of EM_CONFIG assignment in {config_file}"
                    )

                new_lines = []
                for l in lines:
                    if l.strip() == "EM_CONFIG = '~/.emscripten'":
                        code = textwrap.dedent(
                            f"""\
                            EM_CONFIG = '''
                            EMSCRIPTEN_ROOT = '{{0}}'
                            LLVM_ROOT = '{{1}}'
                            PYTHON = '{{2}}'
                            NODE_JS = '{{3}}'
                            JS_ENGINES = [NODE_JS]
                            COMPILER_ENGINE = NODE_JS
                            CLOSURE_COMPILER = '{{4}}'
                            JAVA = '{{5}}'
                            '''.format(
                                __rootpath__,
                                os.path.join(__rootpath__, 'emscripten-fastcomp'),
                                os.path.join(__rootpath__, '{pkglib.package_slug('python', Package.python2_version)}', 'bin', 'python'),
                                os.path.join(__rootpath__, '{pkglib.package_slug('node', Package.node_version)}', 'bin', 'node'),
                                os.path.join(__rootpath__, 'third_party/closure-compiler/compiler.jar'),
                                os.path.join(__rootpath__, '{pkglib.package_slug('zulu-openjdk', Package.zulu_openjdk_version)}', 'bin', 'java'))
                            """
                        )
                        code = (" " * (len(l) - len(l.lstrip()))) + code
                        new_lines.append(code)
                    else:
                        new_lines.append(l)

                with open(config_file, "w", encoding="utf-8") as f:
                    f.write("".join(new_lines))

            patch_config_file(os.path.join(self.pkg_dir, "tools", "shared.py"))

            os.makedirs(os.path.join(self.pkg_dir, "bin"), exist_ok=True)
            bins = [
                ("em++", "em++.py"),
                ("em-config", "em-config"),
                ("emar", "emar.py"),
                ("embuilder", "embuilder.py"),
                ("emcc", "emcc.py"),
                ("emcmake", "emcmake.py"),
                ("emconfigure", "emconfigure.py"),
                ("emlink", "emlink.py"),
                ("emmake", "emmake.py"),
                ("emranlib", "emranlib"),
                ("emrun", "emrun.py"),
                ("emscons", "emscons"),
            ]
            for bin_name, target_file in bins:
                with open(os.path.join(self.pkg_dir, "bin", bin_name), "w", encoding="utf-8") as f:
                    f.write(
                        textwrap.dedent(
                            f"""\
                        #!/bin/sh
                        DIR=`dirname "$0"`
                        export EM_CACHE="$DIR"/../emscripten_cache
                        export EM_PORTS="$DIR"/../emscripten_ports
                        export EMCC_WASM_BACKEND=0
                        export EM_EXCLUSIVE_CACHE_ACCESS=1
                        exec "$DIR"/../{pkglib.package_slug('python', Package.python2_version)}/bin/python "$DIR"/../{target_file} "$@"
                        """
                        )
                    )
                os.chmod(os.path.join(self.pkg_dir, "bin", bin_name), 0o775)

            # Build emscripten-fastcomp together with emscripten-fastcomp-clang:

            shutil.copytree(
                os.path.join(tmp_dir, "emscripten-fastcomp"), os.path.join(tmp_dir, "src"),
            )
            shutil.copytree(
                os.path.join(tmp_dir, "emscripten-fastcomp-clang"),
                os.path.join(tmp_dir, "src", "tools", "clang"),
            )
            os.mkdir(os.path.join(tmp_dir, "src", "build"))

            subprocess.run(
                [
                    cmake_bin,
                    "..",
                    "-DCMAKE_BUILD_TYPE=Release",
                    "-DLLVM_TARGETS_TO_BUILD=X86;JSBackend",
                    "-DLLVM_INCLUDE_EXAMPLES=OFF",
                    "-DLLVM_INCLUDE_TESTS=OFF",
                    "-DCLANG_INCLUDE_TESTS=OFF",
                ],
                cwd=os.path.join(tmp_dir, "src", "build"),
                check=True,
            )

            cpu_count = os.cpu_count()
            if cpu_count is None:
                cpu_count = 1

            subprocess.run(
                ["make", "-j" + str(cpu_count)],
                cwd=os.path.join(tmp_dir, "src", "build"),
                check=True,
            )

            shutil.copytree(
                os.path.join(tmp_dir, "src", "build", "bin"),
                os.path.join(self.pkg_dir, "emscripten-fastcomp"),
            )

            env = dict(os.environ)
            env["PATH"] = os.path.join(cmake_dir, "bin") + os.pathsep + env.get("PATH", "")
            env["EM_CACHE"] = os.path.join(self.pkg_dir, "emscripten_cache")
            env["EM_PORTS"] = os.path.join(self.pkg_dir, "emscripten_ports")
            env["EM_EXCLUSIVE_CACHE_ACCESS"] = "1"
            # TODO We should use EMCC_LOCAL_PORTS and pre-download all the
            # ports files with verified sha256 hashes. This will produce result
            # in a more reproducable build. (And then we don't need to set
            # SSL_CERT_DIR since we won't download anything at this late stage)
            env["SSL_CERT_DIR"] = "/etc/ssl/certs"
            subprocess.run(
                [python2_bin, os.path.join(self.pkg_dir, "embuilder.py"), "build", "ALL",],
                env=env,
                check=True,
            )

    def create_tests(self, pkg_dir):
        class PackageTestCase(pkglib.PackageTestCase):
            def test_emcc(self):
                out = self.run_program(pkg_dir, "emcc", ["--version"])
                out_version = " ".join(out.split("\n")[0].split()[0:5])
                self.assertEqual(
                    out_version, f"emcc (Emscripten gcc/clang-like replacement) {Package.version}",
                )

            def test_compile(self):
                with pkglib.TmpDir() as tmp_dir:
                    with open(os.path.join(tmp_dir, "test.c"), "w") as f:
                        f.write(
                            textwrap.dedent(
                                """\
                            #include <stdio.h>
                            int main() {
                                printf("Hello emscripten\\n");
                                return 0;
                            }
                            """
                            )
                        )
                    subprocess.run(
                        [os.path.join(pkg_dir, "bin", "emcc"), "test.c"], cwd=tmp_dir, check=True,
                    )

                    test_out = subprocess.run(
                        [
                            os.path.join(
                                pkg_dir,
                                pkglib.package_slug("node", Package.node_version),
                                "bin",
                                "node",
                            ),
                            "a.out.js",
                        ],
                        cwd=tmp_dir,
                        check=True,
                        encoding="utf-8",
                        stdout=subprocess.PIPE,
                    ).stdout

                    self.assertEqual(test_out.strip(), "Hello emscripten")

            def test_em_plus_plus(self):
                with pkglib.TmpDir() as tmp_dir:
                    with open(os.path.join(tmp_dir, "test.cc"), "w") as f:
                        f.write(
                            textwrap.dedent(
                                """\
                            #include <iostream>
                            int main() {
                                std::cout << "Hello emscripten++" << std::endl;
                                return 0;
                            }
                            """
                            )
                        )
                    subprocess.run(
                        [os.path.join(pkg_dir, "bin", "em++"), "test.cc"], cwd=tmp_dir, check=True,
                    )

                    test_out = subprocess.run(
                        [
                            os.path.join(
                                pkg_dir,
                                pkglib.package_slug("node", Package.node_version),
                                "bin",
                                "node",
                            ),
                            "a.out.js",
                        ],
                        cwd=tmp_dir,
                        check=True,
                        encoding="utf-8",
                        stdout=subprocess.PIPE,
                    ).stdout

                    self.assertEqual(test_out.strip(), "Hello emscripten++")

            def test_em_config(self):
                self.assert_run_program(
                    pkg_dir, "em-config", ["ASM_JS_TARGET"], "asmjs-unknown-emscripten"
                )

            def test_closure(self):
                with pkglib.TmpDir() as tmp_dir:
                    with open(os.path.join(tmp_dir, "test.c"), "w") as f:
                        f.write(
                            textwrap.dedent(
                                """\
                            #include <stdio.h>
                            int main() {
                                printf("Hello closure\\n");
                                return 0;
                            }
                            """
                            )
                        )
                    subprocess.run(
                        [os.path.join(pkg_dir, "bin", "emcc"), "-O2", "--closure", "1", "test.c",],
                        cwd=tmp_dir,
                        check=True,
                    )

                    test_out = subprocess.run(
                        [
                            os.path.join(
                                pkg_dir,
                                pkglib.package_slug("node", Package.node_version),
                                "bin",
                                "node",
                            ),
                            "a.out.js",
                        ],
                        cwd=tmp_dir,
                        check=True,
                        encoding="utf-8",
                        stdout=subprocess.PIPE,
                    ).stdout

                    self.assertEqual(test_out.strip(), "Hello closure")

            @unittest.skip("Not Implemented Yet")
            def test_emar(self):
                raise NotImplementedError('TODO Test "emar" program')

            def test_embuilder(self):
                subprocess.run(
                    [os.path.join(pkg_dir, "bin", "embuilder"), "build", "libc"], check=True,
                )

            @unittest.skip("Not Implemented Yet")
            def test_emcmake(self):
                raise NotImplementedError('TODO Test "emcmake" program')

            @unittest.skip("Not Implemented Yet")
            def test_emconfigure(self):
                raise NotImplementedError('TODO Test "emconfigure" program')

            @unittest.skip("Not Implemented Yet")
            def test_emlink(self):
                raise NotImplementedError('TODO Test "emlink" program')

            @unittest.skip("Not Implemented Yet")
            def test_emmake(self):
                raise NotImplementedError('TODO Test "emmake" program')

            @unittest.skip("Not Implemented Yet")
            def test_emranlib(self):
                raise NotImplementedError('TODO Test "emranlib" program')

            @unittest.skip("Not Implemented Yet")
            def test_emrun(self):
                raise NotImplementedError('TODO Test "emrun" program')

            @unittest.skip("Not Implemented Yet")
            def test_emscons(self):
                raise NotImplementedError('TODO Test "emscons" program')

        return PackageTestCase


if __name__ == "__main__":
    Package().run()
