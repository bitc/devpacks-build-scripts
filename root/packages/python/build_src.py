import distutils.version
import os.path
import textwrap
from collections import OrderedDict
from pathlib import Path
from typing import Dict, List, NamedTuple

from ...lib.test import BuildContext


class BuildConfigPython2(NamedTuple):
    ncurses_version: str
    readline_version: str
    zlib_version: str
    openssl_version: str


class BuildConfigPython3(NamedTuple):
    gcc_version: str
    make_version: str
    ncurses_version: str
    readline_version: str
    zlib_version: str
    bzip2_version: str
    openssl_version: str
    xz_version: str
    sqlite_version: str


build_configs_python2: Dict[str, BuildConfigPython2] = OrderedDict(
    [
        (
            "2.7.15",
            BuildConfigPython2(
                ncurses_version="6.2",
                readline_version="8.0",
                zlib_version="1.2.11",
                openssl_version="1.0.2p",
            ),
        ),
        (
            "2.7.16",
            BuildConfigPython2(
                ncurses_version="6.2",
                readline_version="8.0",
                zlib_version="1.2.11",
                openssl_version="1.0.2p",
            ),
        ),
        (
            "2.7.17",
            BuildConfigPython2(
                ncurses_version="6.2",
                readline_version="8.0",
                zlib_version="1.2.11",
                openssl_version="1.0.2p",
            ),
        ),
        (
            "2.7.18",
            BuildConfigPython2(
                ncurses_version="6.2",
                readline_version="8.0",
                zlib_version="1.2.11",
                openssl_version="1.0.2p",
            ),
        ),
    ],
)

build_configs_python3: Dict[str, BuildConfigPython3] = OrderedDict(
    [
        (
            "3.6.4",
            BuildConfigPython3(
                gcc_version="7.5.0",
                make_version="4.1",
                ncurses_version="6.2",
                readline_version="8.0",
                zlib_version="1.2.11",
                bzip2_version="1.0.8",
                openssl_version="1.1.1j",
                xz_version="5.2.3",
                sqlite_version="3.34.1",
            ),
        ),
        (
            "3.6.8",
            BuildConfigPython3(
                gcc_version="7.5.0",
                make_version="4.1",
                ncurses_version="6.2",
                readline_version="8.0",
                zlib_version="1.2.11",
                bzip2_version="1.0.8",
                openssl_version="1.1.1j",
                xz_version="5.2.3",
                sqlite_version="3.34.1",
            ),
        ),
        (
            "3.6.13",
            BuildConfigPython3(
                gcc_version="7.5.0",
                make_version="4.1",
                ncurses_version="6.2",
                readline_version="8.0",
                zlib_version="1.2.11",
                bzip2_version="1.0.8",
                openssl_version="1.1.1j",
                xz_version="5.2.3",
                sqlite_version="3.34.1",
            ),
        ),
    ]
)


def python_src_url(version: str) -> str:
    return f"https://www.python.org/ftp/python/{version}/Python-{version}.tar.xz"


def make_pkg_src(ctx: BuildContext, pkg_version: str) -> None:
    python_major = distutils.version.LooseVersion(pkg_version).version[0]
    if int(python_major) >= 3:
        make_python3_src(ctx, pkg_version)
    else:
        make_python2_src(ctx, pkg_version)


def make_python2_src(ctx: BuildContext, pkg_version: str):
    build_config = build_configs_python2[pkg_version]

    readline_dir = ctx.devpacks_pkg("readline", build_config.readline_version)
    ncurses_dir = ctx.devpacks_pkg("ncurses", build_config.ncurses_version)
    openssl_dir = ctx.devpacks_pkg("openssl", build_config.openssl_version)
    zlib_dir = ctx.devpacks_pkg("zlib", build_config.zlib_version)

    ctx.download_root(python_src_url(pkg_version))

    ctx.shell.append_file(
        ctx.pkg_dir / "Modules" / "Setup.dist",
        textwrap.dedent(
            f"""\
            SSL={openssl_dir}
            _ssl _ssl.c \\
                    -DUSE_SSL -I$(SSL)/include -I$(SSL)/include/openssl \\
                    -L$(SSL)/lib -lssl -lcrypto

            zlib zlibmodule.c -I$(prefix)/include -L$(exec_prefix)/lib -lz
            """
        ),
    )

    optimizations: List[str] = []

    # Comment out the below line during testing for faster builds
    optimizations = ["--enable-optimizations"]

    ctx.shell.env["CPPFLAGS"] = f'-I{readline_dir / "include"} ' + ctx.shell.env.get("CPPFLAGS", "")
    ctx.shell.env["LDFLAGS"] = f'-L{readline_dir / "lib"} ' + ctx.shell.env.get("LDFLAGS", "")

    ctx.shell.env["CPPFLAGS"] = f'-I{ncurses_dir / "include"} ' + ctx.shell.env.get("CPPFLAGS", "")
    ctx.shell.env["LDFLAGS"] = f'-L{ncurses_dir / "lib"} ' + ctx.shell.env.get("LDFLAGS", "")

    ctx.shell.env["CPPFLAGS"] = f'-I{zlib_dir / "include"} ' + ctx.shell.env.get("CPPFLAGS", "")
    ctx.shell.env["LDFLAGS"] = f'-L{zlib_dir / "lib"} ' + ctx.shell.env.get("LDFLAGS", "")

    ctx.shell.run("./configure", ["--disable-shared"] + optimizations)
    ctx.shell.run("make", [])

    os.makedirs(ctx.pkg_dir / "bin", exist_ok=True)

    # For some reason the build decides to name the executable "python.exe" on macOS
    if ctx.os == "darwin":
        ext = ".exe"
    else:
        ext = ""

    ctx.shell.write_file(
        ctx.pkg_dir / "bin" / "python",
        textwrap.dedent(
            f"""\
            #!/bin/sh
            DIR=`dirname "$0"`
            exec "$DIR"/../python{ext} "$@"
            """
        ),
    )
    os.chmod(ctx.pkg_dir / "bin" / "python", 0o775)


def make_python3_src(ctx: BuildContext, pkg_version: str):
    build_config = build_configs_python3[pkg_version]

    gcc_dir = ctx.devpacks_pkg("gcc", build_config.gcc_version)
    make_dir = ctx.devpacks_pkg("make", build_config.make_version)
    readline_dir = ctx.devpacks_pkg("readline", build_config.readline_version)
    ncurses_dir = ctx.devpacks_pkg("ncurses", build_config.ncurses_version)
    xz_dir = ctx.devpacks_pkg("xz", build_config.xz_version)
    openssl_dir = ctx.devpacks_pkg("openssl", build_config.openssl_version)
    zlib_dir = ctx.devpacks_pkg("zlib", build_config.zlib_version)
    bzip2_dir = ctx.devpacks_pkg("bzip2", build_config.bzip2_version)
    sqlite_dir = ctx.devpacks_pkg("sqlite", build_config.sqlite_version)

    ctx.download_root(python_src_url(pkg_version))

    ctx.shell.append_file(
        ctx.pkg_dir / "Modules" / "Setup.dist",
        textwrap.dedent(
            f"""\
            readline readline.c -lreadline -lncursesw

            SSL={openssl_dir}
            _ssl _ssl.c \\
                -DUSE_SSL -I$(SSL)/include -I$(SSL)/include/openssl \\
                -L$(SSL)/lib -lssl -lcrypto

            _crypt _cryptmodule.c -lcrypt

            nis nismodule.c -lnsl

            _curses _cursesmodule.c -lncursesw -lncursesw
            _curses_panel _curses_panel.c -lpanelw -lncursesw

            zlib zlibmodule.c -I$(prefix)/include -L$(exec_prefix)/lib -lz
            """
        ),
    )

    optimizations: List[str] = []

    # Comment out the below line during testing for faster builds
    optimizations = ["--enable-optimizations"]

    ctx.shell.env["CPPFLAGS"] = f'-I{readline_dir / "include"} ' + ctx.shell.env.get("CPPFLAGS", "")
    ctx.shell.env["LDFLAGS"] = f'-L{readline_dir / "lib"} ' + ctx.shell.env.get("LDFLAGS", "")

    ctx.shell.env["CPPFLAGS"] = (
        f'-I{ncurses_dir / "include"} -I{ncurses_dir / "include" / "ncursesw"} '
        + ctx.shell.env.get("CPPFLAGS", "")
    )
    ctx.shell.env["LDFLAGS"] = f'-L{ncurses_dir / "lib"} ' + ctx.shell.env.get("LDFLAGS", "")

    ctx.shell.env["CPPFLAGS"] = f'-I{zlib_dir / "include"} ' + ctx.shell.env.get("CPPFLAGS", "")
    ctx.shell.env["LDFLAGS"] = f'-L{zlib_dir / "lib"} ' + ctx.shell.env.get("LDFLAGS", "")

    ctx.shell.env["CPPFLAGS"] = f'-I{bzip2_dir / "include"} ' + ctx.shell.env.get("CPPFLAGS", "")
    ctx.shell.env["LDFLAGS"] = f'-L{bzip2_dir / "lib"} ' + ctx.shell.env.get("LDFLAGS", "")

    ctx.shell.env["CPPFLAGS"] = f'-I{xz_dir / "include"} ' + ctx.shell.env.get("CPPFLAGS", "")
    ctx.shell.env["LDFLAGS"] = f'-L{xz_dir / "lib"} ' + ctx.shell.env.get("LDFLAGS", "")

    ctx.shell.env["CPPFLAGS"] = f'-I{openssl_dir / "include"} ' + ctx.shell.env.get("CPPFLAGS", "")
    ctx.shell.env["LDFLAGS"] = f'-L{openssl_dir / "lib"} ' + ctx.shell.env.get("LDFLAGS", "")

    ctx.shell.env["CPPFLAGS"] = f'-I{sqlite_dir / "include"} ' + ctx.shell.env.get("CPPFLAGS", "")
    ctx.shell.env["LDFLAGS"] = f'-L{sqlite_dir / "lib"} ' + ctx.shell.env.get("LDFLAGS", "")

    ctx.shell.env["PATH"] = os.path.pathsep.join(
        [str(Path(gcc_dir) / "bin"), str(Path(make_dir) / "bin"), ctx.shell.env["PATH"],]
    )

    ctx.shell.run(
        "./configure", ["--disable-shared", "--enable-loadable-sqlite-extensions"] + optimizations
    )
    ctx.shell.run("make", [])

    os.makedirs(ctx.pkg_dir / "bin", exist_ok=True)

    # For some reason the build decides to name the executable "python.exe" on macOS
    if ctx.os == "darwin":
        ext = ".exe"
    else:
        ext = ""

    ctx.shell.append_file(
        ctx.pkg_dir / "bin" / "python",
        textwrap.dedent(
            f"""\
            #!/bin/sh
            DIR=`dirname "$0"`
            exec "$DIR"/../python{ext} "$@"
            """
        ),
    )
    os.chmod(ctx.pkg_dir / "bin" / "python", 0o775)
