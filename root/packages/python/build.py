import distutils.version
import platform
import subprocess
from pathlib import Path
from typing import Type

from ...lib.main_driver import main_driver
from ...lib.test import BuildContext, PackageDescr, PackageTestCase
from .build_src import build_configs_python2, build_configs_python3, make_pkg_src
from .build_windows import make_pkg_windows


def make_pkg(ctx: BuildContext, pkg_version: str) -> None:
    if ctx.os == "windows":
        make_pkg_windows(ctx, pkg_version)
    else:
        make_pkg_src(ctx, pkg_version)


def create_tests_python2(pkg_dir: Path, pkg_version: str) -> Type[PackageTestCase]:
    build_config = build_configs_python2[pkg_version]

    class TestCase(PackageTestCase):
        def test_python(self) -> None:
            cmd = subprocess.run(
                [pkg_dir / "bin" / "python", "--version"],
                check=True,
                encoding="utf-8",
                stderr=subprocess.PIPE,
            )
            self.assertEqual(cmd.stderr.strip(), f"Python {pkg_version}")

        def test_ssl(self) -> None:
            self.assert_run_program(
                pkg_dir, "python", ["-c", "import ssl; print(bool(ssl.RAND_status()))"], "True",
            )

            # On Windows we use the prebuilt binary from upstream and don't know which openssl version it is
            if platform.system() != "Windows":
                self.assert_run_program(
                    pkg_dir,
                    "python",
                    ["-c", 'import ssl; print(ssl.OPENSSL_VERSION.split(" ")[1])'],
                    f"{build_config.openssl_version}",
                )

        def test_zlib(self) -> None:
            # On Windows we use the prebuilt binary from upstream and don't know which zlib version it is
            if platform.system() != "Windows":
                self.assert_run_program(
                    pkg_dir,
                    "python",
                    ["-c", "import zlib; print(zlib.ZLIB_VERSION)"],
                    f"{build_config.zlib_version}",
                )

            self.assert_run_program(
                pkg_dir,
                "python",
                [
                    "-c",
                    'import zlib; print(zlib.decompress(zlib.compress(b"abc123")).decode("utf-8"))',
                ],
                "abc123",
            )

    return TestCase


def create_tests_python3(pkg_dir: Path, pkg_version: str) -> Type[PackageTestCase]:
    build_config = build_configs_python3[pkg_version]

    class TestCase(PackageTestCase):
        def test_python(self) -> None:
            self.assert_run_program(pkg_dir, "python", ["--version"], f"Python {pkg_version}")

        def test_crypt(self) -> None:
            if platform.system() == "Windows":
                return

            self.assert_run_program(
                pkg_dir,
                "python",
                ["-c", "import crypt; print(crypt.METHOD_MD5 in crypt.methods)"],
                "True",
            )

        def test_curses(self) -> None:
            if platform.system() == "Windows":
                return

            self.assert_run_program(
                pkg_dir, "python", ["-c", "import curses; print(curses.OK)"], "0",
            )

        def test_ssl(self) -> None:
            self.assert_run_program(
                pkg_dir,
                "python",
                ["-c", "import ssl; print(len(ssl.RAND_pseudo_bytes(1)[0]))"],
                "1",
            )

            # On Windows we use the prebuilt binary from upstream and don't know which openssl version it is
            if platform.system() != "Windows":
                self.assert_run_program(
                    pkg_dir,
                    "python",
                    ["-c", 'import ssl; print(ssl.OPENSSL_VERSION.split(" ")[1])'],
                    f"{build_config.openssl_version}",
                )

        def test_lzma(self) -> None:
            self.assert_run_program(
                pkg_dir,
                "python",
                [
                    "-c",
                    'import lzma; print(lzma.decompress(lzma.compress(b"abc123")).decode("utf-8"))',
                ],
                "abc123",
            )

        def test_zlib(self) -> None:
            # On Windows we use the prebuilt binary from upstream and don't know which zlib version it is
            if platform.system() != "Windows":
                self.assert_run_program(
                    pkg_dir,
                    "python",
                    ["-c", "import zlib; print(zlib.ZLIB_VERSION)"],
                    f"{build_config.zlib_version}",
                )

            self.assert_run_program(
                pkg_dir,
                "python",
                [
                    "-c",
                    'import zlib; print(zlib.decompress(zlib.compress(b"abc123")).decode("utf-8"))',
                ],
                "abc123",
            )

        def test_bz2(self) -> None:
            self.assert_run_program(
                pkg_dir,
                "python",
                [
                    "-c",
                    'import bz2; print(bz2.decompress(bz2.compress(b"abc123")).decode("utf-8"))',
                ],
                "abc123",
            )

        def test_sqlite3(self) -> None:
            # On Windows we use the prebuilt binary from upstream and don't know which zlib version it is
            if platform.system() != "Windows":
                self.assert_run_program(
                    pkg_dir,
                    "python",
                    ["-c", "import sqlite3; print(sqlite3.sqlite_version)"],
                    f"{build_config.sqlite_version}",
                )

            self.assert_run_program(
                pkg_dir, "python", ["-c", "import sqlite3; print(sqlite3.threadsafety)",], "1",
            )

        def test_openssl_hashlib(self) -> None:
            self.assert_run_program(
                pkg_dir,
                "python",
                ["-c", "import _hashlib; print(_hashlib.openssl_sha1(b'hello').hexdigest())",],
                "aaf4c61ddcc5e8a2dabede0f3b482cd9aea9434d",
            )

    return TestCase


def create_tests(pkg_dir: Path, pkg_version: str) -> Type[PackageTestCase]:
    python_major = distutils.version.LooseVersion(pkg_version).version[0]

    if int(python_major) >= 3:
        return create_tests_python3(pkg_dir, pkg_version)
    else:
        return create_tests_python2(pkg_dir, pkg_version)


pkg = PackageDescr(
    name="python",
    versions=list(build_configs_python2.keys()) + list(build_configs_python3.keys()),
    make_pkg=make_pkg,
    create_tests=create_tests,
    sha256s={
        "https://www.python.org/ftp/python/2.7.15/Python-2.7.15.tar.xz": "22d9b1ac5b26135ad2b8c2901a9413537e08749a753356ee913c84dbd2df5574",
        "https://www.python.org/ftp/python/3.6.13/Python-3.6.13.tar.xz": "a47a43a53abb42286a2c11965343ff56711b9e64e8d11bf2c6701a4fb8ce1a0f",
        "https://www.python.org/ftp/python/3.6.4/python-3.6.4-amd64.exe": "de02d46fe9a6269a7c0792d8be155b0d1fa20a68623a0fad33ab02f92ef58d49",
        "https://www.python.org/ftp/python/3.6.4/Python-3.6.4.tar.xz": "159b932bf56aeaa76fd66e7420522d8c8853d486b8567c459b84fe2ed13bcaba",
        "https://www.python.org/ftp/python/3.6.8/python-3.6.8-amd64.exe": "96088a58b7c43bc83b84e6b67f15e8706c614023dd64f9a5a14e81ff824adadc",
        "https://www.python.org/ftp/python/3.6.8/Python-3.6.8.tar.xz": "35446241e995773b1bed7d196f4b624dadcadc8429f26282e756b2fb8a351193",
    },
)

if __name__ == "__main__":
    main_driver(pkg)
