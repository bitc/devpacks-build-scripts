import os
import shutil
import textwrap

from ...lib.pkglib import strip_win_extended_path
from ...lib.test import BuildContext


def make_pkg_windows(ctx: BuildContext, pkg_version: str) -> None:
    url = {
        "x86_64": f"https://www.python.org/ftp/python/{pkg_version}/python-{pkg_version}-amd64.exe"
    }[ctx.arch]

    ctx.download_file(url, ctx.pkg_dir / "python-installer.exe")
    print("Running Installer...")
    # See: <https://docs.python.org/3/using/windows.html#installing-without-ui>

    install_dir = ctx.pkg_dir / "python-installed"
    target_dir = strip_win_extended_path(install_dir)
    ctx.shell.run(
        ctx.pkg_dir / "python-installer.exe",
        [
            "/quiet",
            "InstallAllUsers=0",
            "TargetDir=" + str(target_dir),
            "DefaultAllUsersTargetDir=" + str(target_dir),
            "DefaultJustForMeTargetDir=" + str(target_dir),
            "DefaultCustomTargetDir=" + str(target_dir),
            "AssociateFiles=0",
            "CompileAll=1",
            "PrependPath=0",
            "Shortcuts=0",
            "Include_doc=0",
            "Include_debug=0",
            "Include_dev=1",
            "Include_exe=1",
            "Include_launcher=0",
            "InstallLauncherAllUsers=0",
            "Include_lib=1",
            "Include_pip=1",
            "Include_symbols=0",
            "Include_tcltk=1",
            "Include_test=1",
            "Include_tools=1",
            "LauncherOnly=0",
        ],
    )

    print("Done")
    print("Copying installed files")
    for f in os.listdir(install_dir):
        if os.path.isdir(install_dir / f):
            shutil.copytree(install_dir / f, ctx.pkg_dir / f)
        else:
            shutil.copy(install_dir / f, ctx.pkg_dir)
    print("Done")
    print("Running Un-Installer...")
    ctx.shell.run(ctx.pkg_dir / "python-installer.exe", ["/uninstall", "/quiet"])
    print("Done")

    if os.path.isdir(install_dir):
        shutil.rmtree(install_dir)

    ctx.shell.mkdir(ctx.pkg_dir / "bin")

    ctx.shell.remove(ctx.pkg_dir / "python-installer.exe")

    # Create wrapper script for running python
    ctx.shell.write_file(
        ctx.pkg_dir / "bin" / "python.cmd",
        textwrap.dedent(
            f"""\
            @ECHO OFF
            SETLOCAL
            SET "PROG_EXE=%~dp0..\\python"
            "%PROG_EXE%" %*
            """
        ),
    )
