import os.path
import textwrap
from collections import OrderedDict
from pathlib import Path
from typing import Dict, Type

from ...lib.ecosystems.python.pip import PipBuildConfig, make_pip_pkg, pip_sha256s
from ...lib.main_driver import main_driver
from ...lib.test import BuildContext, PackageDescr, PackageTestCase

build_configs: Dict[str, PipBuildConfig] = OrderedDict(
    [
        (
            "1.15.42",
            PipBuildConfig(
                python_version="3.6.8", pip_version="20.1.1", setuptools_version="49.2.0",
            ),
        ),
        (
            "1.19.13",
            PipBuildConfig(
                python_version="3.6.8", pip_version="21.0.1", setuptools_version="50.3.2",
            ),
        ),
    ]
)


def make_pkg(ctx: BuildContext, pkg_version: str) -> None:
    build_config = build_configs[pkg_version]
    make_pip_pkg(ctx, "awscli", pkg_version, build_config, ["aws"])


def create_tests(pkg_dir: Path, pkg_version: str) -> Type[PackageTestCase]:
    class TestCase(PackageTestCase):
        def test_aws(self) -> None:
            self.assert_run_program_first_line_prefix(
                pkg_dir, "aws", ["--version"], f"aws-cli/{pkg_version}"
            )

    return TestCase


pkg = PackageDescr(
    name="awscli",
    versions=list(build_configs.keys()),
    make_pkg=make_pkg,
    create_tests=create_tests,
    sha256s=pip_sha256s,
)

if __name__ == "__main__":
    main_driver(pkg)
