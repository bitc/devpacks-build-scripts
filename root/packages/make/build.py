import os
from collections import OrderedDict
from pathlib import Path
from typing import Dict, NamedTuple, Type

from ...lib.main_driver import main_driver
from ...lib.test import BuildContext, PackageDescr, PackageTestCase


class BuildConfig(NamedTuple):
    make_version: str
    gcc_version: str


build_configs: Dict[str, BuildConfig] = OrderedDict(
    [
        # ("3.79", BuildConfig(make_version="4.0", gcc_version="4.9.4")),
        ("3.82", BuildConfig(make_version="4.0", gcc_version="4.9.4")),
        ("4.0", BuildConfig(make_version="4.0", gcc_version="4.9.4")),
        ("4.1", BuildConfig(make_version="4.1", gcc_version="4.9.4")),
        ("4.2.1", BuildConfig(make_version="4.2.1", gcc_version="4.9.4")),
        ("4.3", BuildConfig(make_version="4.3", gcc_version="4.9.4")),
    ],
)


def make_pkg(ctx: BuildContext, pkg_version: str) -> None:
    build_config = build_configs[pkg_version]

    make_dir = ctx.devpacks_pkg("make", build_config.make_version)
    if not ctx.use_system_gcc:
        gcc_dir = ctx.devpacks_pkg("gcc", build_config.gcc_version)

    url = f"https://ftp.gnu.org/gnu/make/make-{pkg_version}.tar.gz"

    src_dir = ctx.tmp_dir / "src"
    ctx.shell.mkdir(src_dir)

    ctx.download_extract(url, src_dir)

    ctx.shell.env["PATH"] = os.pathsep.join(
        ([] if ctx.use_system_gcc else [str(gcc_dir / "bin")])
        + [str(make_dir / "bin"), ctx.shell.env["PATH"]]
    )

    with ctx.shell.cd(src_dir):
        ctx.shell.run(
            "./configure", ["--prefix=" + str(ctx.pkg_dir), "--host=x86_64-linux-gnu",],
        )
        ctx.shell.run("make", [])
        ctx.shell.run("make", ["install"])


def create_tests(pkg_dir: Path, pkg_version: str) -> Type[PackageTestCase]:
    class TestCase(PackageTestCase):
        def test_make(self) -> None:
            self.assert_run_program_first_line(
                pkg_dir, "make", ["--version"], f"GNU Make {pkg_version}"
            )

    return TestCase


pkg = PackageDescr(
    name="make",
    versions=list(build_configs.keys()),
    make_pkg=make_pkg,
    create_tests=create_tests,
    sha256s={
        "https://ftp.gnu.org/gnu/make/make-3.79.tar.gz": "e4bde0f9d71e2e7c979ee97dc70d9ceb0b2278ddd3ddeb82992f2ea9a0d07ac2",
        "https://ftp.gnu.org/gnu/make/make-3.82.tar.gz": "3d991b33e604187c5881a0abc2e102d5b9776da5569640e73778f85d617242e7",
        "https://ftp.gnu.org/gnu/make/make-4.0.tar.gz": "fc42139fb0d4b4291929788ebaf77e2a4de7eaca95e31f3634ef7d4932051f69",
        "https://ftp.gnu.org/gnu/make/make-4.1.tar.gz": "9fc7a9783d3d2ea002aa1348f851875a2636116c433677453cc1d1acc3fc4d55",
        "https://ftp.gnu.org/gnu/make/make-4.2.1.tar.gz": "e40b8f018c1da64edd1cc9a6fce5fa63b2e707e404e20cad91fbae337c98a5b7",
        "https://ftp.gnu.org/gnu/make/make-4.3.tar.gz": "e05fdde47c5f7ca45cb697e973894ff4f5d79e13b750ed57d7b66d8defc78e19",
    },
)

if __name__ == "__main__":
    main_driver(pkg)
