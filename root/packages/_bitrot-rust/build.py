import distutils.version
import os.path
import subprocess
import textwrap
from pathlib import Path
from typing import Type

from ...lib.main_driver import main_driver
from ...lib.pkglib import prog_file
from ...lib.test import BuildContext, PackageDescr, PackageTestCase
from ...lib.tmp_dir import TmpDir

versions = ["1.45.0"]


def make_pkg(ctx: BuildContext, pkg_version: str) -> None:
    url = {
        (
            "linux",
            "x86_64",
        ): f"https://static.rust-lang.org/dist/rust-{pkg_version}-x86_64-unknown-linux-gnu.tar.gz"
    }[ctx.os, ctx.arch]

    ctx.download_root(url)

    ctx.shell.mkdir("bin")

    bins = [
        Path("./cargo/bin/cargo"),
        Path("./clippy-preview/bin/cargo-clippy"),
        Path("./rustfmt-preview/bin/cargo-fmt"),
        # Path("./clippy-preview/bin/clippy-driver"),
        Path("./rustc/bin/rustdoc"),
        Path("./rustfmt-preview/bin/rustfmt"),
        Path("./rustc/bin/rust-gdb"),
        Path("./rustc/bin/rust-lldb"),
    ]

    with open(ctx.pkg_dir / "bin" / "rustc", "w", encoding="utf-8") as f:
        f.write(
            textwrap.dedent(
                f"""\
                #!/bin/sh
                DIR=`dirname "$0"`
                exec "$DIR"/../rustc/bin/rustc -L "$DIR"/../rust-std-x86_64-unknown-linux-gnu/lib/rustlib/x86_64-unknown-linux-gnu/lib/ "$@"
                """
            )
        )
    os.chmod(os.path.join(ctx.pkg_dir, "bin", "rustc"), 0o775)

    for bin_name in bins:
        shortcut_file = bin_name.name
        with open(ctx.pkg_dir / "bin" / shortcut_file, "w", encoding="utf-8") as f:
            f.write(
                textwrap.dedent(
                    f"""\
                    #!/bin/sh
                    DIR=`dirname "$0"`
                    export PATH="$DIR"/../bin{os.pathsep}$PATH
                    exec "$DIR"/../{bin_name} "$@"
                    """
                )
            )
        os.chmod(os.path.join(ctx.pkg_dir, "bin", shortcut_file), 0o775)


def create_tests(pkg_dir: Path, pkg_version: str) -> Type[PackageTestCase]:
    class TestCase(PackageTestCase):
        def test_cargo(self) -> None:
            self.assert_run_program_first_line_prefix(
                pkg_dir, "cargo", ["--version"], f"cargo {pkg_version}"
            )

        def test_cargo_clippy(self) -> None:
            self.assert_run_program_first_line_prefix(
                pkg_dir, "cargo-clippy", ["--version"], f"clippy"
            )

        def test_cargofmt(self) -> None:
            self.assert_run_program_first_line_prefix(
                pkg_dir, "cargo-fmt", ["--version"], f"rustfmt"
            )

        # def test_clippy_driver(self):
        #     self.assert_run_program_first_line_prefix(pkg_dir, "clippy-driver", ["--version"], f"rustfmt")

        def test_rustdoc(self) -> None:
            self.assert_run_program_first_line_prefix(
                pkg_dir, "rustdoc", ["--version"], f"rustdoc {pkg_version}"
            )

        def test_rustfmt(self) -> None:
            self.assert_run_program_first_line_prefix(pkg_dir, "rustfmt", ["--version"], f"rustfmt")

        def test_rustc(self) -> None:
            self.assert_run_program_first_line_prefix(
                pkg_dir, "rustc", ["--version"], f"rustc {pkg_version}"
            )

        def test_rustc_compile_hello_world(self) -> None:
            with TmpDir() as tmpdir:
                with open(tmpdir / "hello.rs", "w", encoding="utf-8") as f:
                    f.write(
                        textwrap.dedent(
                            """\
                            fn main() {
                                println!("Hello World!");
                            }
                            """
                        )
                    )

                subprocess.run(
                    [pkg_dir / "bin" / "rustc", "hello.rs"],
                    check=True,
                    cwd=tmpdir,
                    encoding="utf-8",
                    stdout=subprocess.PIPE,
                )

                cmd = subprocess.run(
                    [tmpdir / "hello"], check=True, encoding="utf-8", stdout=subprocess.PIPE,
                )

                self.assertEqual(cmd.stdout, "Hello World!\n")

        def test_cargo_compile_hello_world(self) -> None:
            with TmpDir() as tmpdir:
                os.mkdir(tmpdir / "src")

                with open(tmpdir / "src" / "main.rs", "w", encoding="utf-8") as f:
                    f.write(
                        textwrap.dedent(
                            """\
                            fn main() {
                                println!("Hello World!");
                            }
                            """
                        )
                    )

                with open(tmpdir / "Cargo.toml", "w", encoding="utf-8") as f:
                    f.write(
                        textwrap.dedent(
                            """\
                            [package]
                            name = "hello"
                            version = "0.1.0"
                            edition = "2018"

                            [dependencies]
                            """
                        )
                    )

                subprocess.run(
                    [pkg_dir / "bin" / "cargo", "build"],
                    check=True,
                    cwd=tmpdir,
                    encoding="utf-8",
                    stdout=subprocess.PIPE,
                )

                cmd = subprocess.run(
                    [tmpdir / "target" / "debug" / "hello"],
                    check=True,
                    encoding="utf-8",
                    stdout=subprocess.PIPE,
                )

                self.assertEqual(cmd.stdout, "Hello World!\n")

        def test_check_cargo_clippy(self) -> None:
            with TmpDir() as tmpdir:
                os.mkdir(tmpdir / "src")

                with open(tmpdir / "src" / "main.rs", "w", encoding="utf-8") as f:
                    f.write(
                        textwrap.dedent(
                            """\
                            fn main() {
                                let i = 0;
                                while i > 10 {
                                    println!("let me loop forever!");
                                }
                            }
                            """
                        )
                    )

                with open(tmpdir / "Cargo.toml", "w", encoding="utf-8") as f:
                    f.write(
                        textwrap.dedent(
                            """\
                            [package]
                            name = "hello"
                            version = "0.1.0"
                            edition = "2018"

                            [dependencies]
                            """
                        )
                    )

                cmd = subprocess.run(
                    [pkg_dir / "bin" / "cargo", "clippy"],
                    cwd=tmpdir,
                    encoding="utf-8",
                    stderr=subprocess.PIPE,
                )

                self.assertNotEqual(cmd.returncode, 0)
                self.assertRegex(cmd.stderr, "clippy::while_immutable_condition")

    return TestCase


pkg = PackageDescr(
    name="rust",
    versions=versions,
    make_pkg=make_pkg,
    create_tests=create_tests,
    sha256s={
        "https://static.rust-lang.org/dist/rust-1.45.0-x86_64-unknown-linux-gnu.tar.gz": "c34ed8722759fd60c94dbc9069833da5b3b873dcd19afaa9b34c1ce2c2cfa229",
    },
)

if __name__ == "__main__":
    main_driver(pkg)
