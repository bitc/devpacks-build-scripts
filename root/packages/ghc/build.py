from pathlib import Path
from typing import Type

from ...lib.main_driver import main_driver
from ...lib.test import BuildContext, PackageDescr, PackageTestCase
from .build_linux import linux_build_configs, make_pkg_linux
from .build_windows import make_pkg_windows, windows_build_configs


def make_pkg(ctx: BuildContext, pkg_version: str) -> None:
    if ctx.os == "linux":
        make_pkg_linux(ctx, pkg_version)
    elif ctx.os == "windows":
        make_pkg_windows(ctx, pkg_version)
    else:
        raise NotImplementedError(f'Not implemented: GHC build for os "{ctx.os}"')


def create_tests(pkg_dir: Path, pkg_version: str) -> Type[PackageTestCase]:
    class TestCase(PackageTestCase):
        def test_ghc(self) -> None:
            self.assert_run_program_first_line(
                pkg_dir,
                "ghc",
                ["--version"],
                f"The Glorious Glasgow Haskell Compilation System, version {pkg_version}",
            )

        def test_ghci(self) -> None:
            self.assert_run_program_first_line(
                pkg_dir,
                "ghci",
                ["--version"],
                f"The Glorious Glasgow Haskell Compilation System, version {pkg_version}",
            )

        def test_ghc_pkg(self) -> None:
            self.assert_run_program_first_line(
                pkg_dir, "ghc-pkg", ["--version"], f"GHC package manager version {pkg_version}",
            )

        def test_haddock(self) -> None:
            out = self.run_program(pkg_dir, "haddock", ["--version"])
            out_version = " ".join(out.split("\n")[0].split()[0:1])
            self.assertEqual(out_version, "Haddock")

    return TestCase


pkg = PackageDescr(
    name="ghc",
    versions=list(
        dict.fromkeys(list(linux_build_configs.keys()) + list(windows_build_configs.keys()))
    ),
    make_pkg=make_pkg,
    create_tests=create_tests,
    sha256s={
        "https://downloads.haskell.org/~ghc/8.10.3/ghc-8.10.3-x86_64-unknown-mingw32.tar.xz": "927a6c699533a115cd49772ef2c753d9af2c13bf9f0b2d3bd13645cc6a144ee3",
        "https://downloads.haskell.org/~ghc/8.10.4/ghc-8.10.4-x86_64-unknown-mingw32.tar.xz": "e9175a276504c3390a5e0084954e6997d56078737dbe7158049518892cf6bfb2",
        "https://downloads.haskell.org/~ghc/9.0.1/ghc-9.0.1-src.tar.xz": "a5230314e4065f9fcc371dfe519748fd85c825b279abf72a24e09b83578a35f9",
        "https://downloads.haskell.org/~ghc/9.0.1/ghc-9.0.1-x86_64-unknown-mingw32.tar.xz": "4f4ab118df01cbc7e7c510096deca0cb25025339a97730de0466416296202493",
    },
)

if __name__ == "__main__":
    main_driver(pkg)
