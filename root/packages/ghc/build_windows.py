from collections import OrderedDict
from typing import Dict, NamedTuple

from ...lib.test import BuildContext


class WindowsBuildConfig(NamedTuple):
    pass


windows_build_configs: Dict[str, WindowsBuildConfig] = OrderedDict(
    [("8.10.3", WindowsBuildConfig())]
)


def make_pkg_windows(ctx: BuildContext, pkg_version: str) -> None:
    url = {
        "x86_64": f"https://downloads.haskell.org/~ghc/{pkg_version}/ghc-{pkg_version}-x86_64-unknown-mingw32.tar.xz"
    }[ctx.arch]

    ctx.download_root(url)
