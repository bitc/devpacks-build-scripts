import distutils.version
import os
import os.path
import shutil
import subprocess
import textwrap
from collections import OrderedDict
from pathlib import Path
from typing import Dict, NamedTuple, Tuple

from ...lib.test import BuildContext


class LinuxBuildConfig(NamedTuple):
    autoconf_version: str
    automake_version: str
    make_version: str
    gcc_version: str
    ncurses_version: str
    gmp_version: str
    python_version: str
    happy_version: str
    alex_version: str
    cabal_install_version: str
    ghc_version: str


linux_build_configs: Dict[str, LinuxBuildConfig] = OrderedDict(
    [
        (
            "8.10.3",
            LinuxBuildConfig(
                autoconf_version="2.69",
                automake_version="1.16.2",
                make_version="4.1",
                gcc_version="10.2.0",
                ncurses_version="6.2",
                gmp_version="6.1.2",
                python_version="3.6.8",
                happy_version="1.19.12",
                alex_version="3.2.5",
                cabal_install_version="3.4.0.0",
                ghc_version="8.10.3",
            ),
        ),
        (
            "8.10.4",
            LinuxBuildConfig(
                autoconf_version="2.69",
                automake_version="1.16.2",
                make_version="4.1",
                gcc_version="10.2.0",
                ncurses_version="6.2",
                gmp_version="6.1.2",
                python_version="3.6.8",
                happy_version="1.19.12",
                alex_version="3.2.5",
                cabal_install_version="3.4.0.0",
                ghc_version="8.10.3",
            ),
        ),
        (
            "9.0.1",
            LinuxBuildConfig(
                autoconf_version="2.69",
                automake_version="1.16.2",
                make_version="4.1",
                gcc_version="10.2.0",
                ncurses_version="6.2",
                gmp_version="6.1.2",
                python_version="3.6.8",
                happy_version="1.19.12",
                alex_version="3.2.5",
                cabal_install_version="3.4.0.0",
                ghc_version="8.10.3",  # TODO Change to 8.10.4
            ),
        ),
    ]
)

git_hashes: Dict[str, Tuple[str, str]] = {
    "8.10.1": ("ghc-8.10.1-release", "5c3cadf5db0d7eb859ff2c278ab07585c7df17b5"),
    "8.10.2": ("ghc-8.10.2-release", "29204b1c4f52ea34d84da33593052ee839293bf2"),
    "8.10.3": ("ghc-8.10.3-release", "6db6db46af6f8e3e24d7d16b0b43a984a9a14677"),
    "8.10.4": ("ghc-8.10.4-release", "6a01e28f4204ec17c587931311711fa76e0ea08d"),
    # "9.0.1": ("ghc-9.0.1-release", "da53a348150d30193a6f28e1b7ddcabdf45ab726"),
}


def make_pkg_linux(ctx: BuildContext, pkg_version: str) -> None:
    build_config = linux_build_configs[pkg_version]

    ncurses_dir = ctx.devpacks_pkg("ncurses", build_config.ncurses_version)
    gmp_dir = ctx.devpacks_pkg("gmp", build_config.gmp_version)
    cabal_dir = ctx.devpacks_pkg("haskell_cabal_install", build_config.cabal_install_version)
    autoreconf_dir = ctx.devpacks_pkg("autoconf", build_config.autoconf_version)
    automake_dir = ctx.devpacks_pkg("automake", build_config.automake_version)
    make_dir = ctx.devpacks_pkg("make", build_config.make_version)
    gcc_dir = ctx.devpacks_pkg("gcc", build_config.gcc_version)
    ghc_dir = ctx.devpacks_pkg("ghc", build_config.ghc_version)
    happy_dir = ctx.devpacks_pkg("haskell_happy", build_config.happy_version)
    alex_dir = ctx.devpacks_pkg("haskell_alex", build_config.alex_version)

    python_prog = ctx.devpacks_pkg_prog("python", build_config.python_version, "python")

    loose_version = distutils.version.LooseVersion(pkg_version).version
    if loose_version < [9, 0, 1]:  # type: ignore
        git_branch, git_hash = git_hashes[pkg_version]

    with ctx.shell.cd(ctx.tmp_dir):
        # The "./boot" script looks specifically for a program called
        # "python3" (instead of "python") so we need to create a shortcut:
        ctx.shell.mkdir("bin")
        ctx.shell.create_exe_shortcut(python_prog, Path("bin") / "python3")

        ctx.shell.env["PATH"] = os.path.pathsep.join(
            [
                str(ctx.tmp_dir / "bin"),
                str(Path(autoreconf_dir) / "bin"),
                str(Path(automake_dir) / "bin"),
                str(Path(make_dir) / "bin"),
                str(Path(gcc_dir) / "bin"),
                str(Path(ghc_dir) / "bin"),
                str(Path(happy_dir) / "bin"),
                str(Path(alex_dir) / "bin"),
                str(Path(cabal_dir) / "bin"),
                ctx.shell.env["PATH"],
            ]
        )

        ctx.shell.env["C_INCLUDE_PATH"] = f"{gmp_dir}/include"
        ctx.shell.env["LIBRARY_PATH"] = os.path.pathsep.join(
            [f"{gmp_dir}/lib", f"{ncurses_dir}/lib"]
        )

        ctx.shell.env["CABAL_DIR"] = f"{ctx.tmp_dir}/cabal"

        # For compatibility, we use an ancient version of glibc. Back then,
        # some functions (like "clock_gettime" and "clock_getres") are in
        # librt instead of libc
        # ctx.shell.env["LIBS"] = "-lrt -pthread"
        #
        # TODO GOOD!!!! (or not? doesn't look like we need it afterall)
        # ctx.shell.env["LDFLAGS"] = "-pthread"

        #####
        # shutil.copytree("/tmp/snapshot/ghc", ctx.tmp_dir / "ghc")

        if loose_version < [9, 0, 1]:  # type: ignore
            ctx.shell.run(
                "git",
                [
                    "clone",
                    "--branch",
                    git_branch,
                    "--depth",
                    "1",
                    "https://gitlab.haskell.org/ghc/ghc.git",
                ],
            )
        else:
            url = f"https://downloads.haskell.org/~ghc/{pkg_version}/ghc-{pkg_version}-src.tar.xz"

            ctx.shell.mkdir("ghc")
            ctx.download_extract(url, ctx.shell.cwd / "ghc")

        with ctx.shell.cd("ghc"):
            if loose_version < [9, 0, 1]:  # type: ignore
                ctx.shell.run("git", ["checkout", git_hash])
                ctx.shell.run("git", ["submodule", "update", "--init", "--recursive"])

            ###
            # print()
            # print("PAUSING")
            # input()
            ###

            if pkg_version == "9.0.1":
                print("Applying patch file patch-ghc-9.0.1-hp2ps-unlit.patch")
                with open(
                    os.path.join(
                        os.path.dirname(os.path.realpath(__file__)),
                        "patch-ghc-9.0.1-hp2ps-unlit.patch",
                    ),
                    "r",
                ) as f:
                    subprocess.run(["patch", "-p1"], stdin=f, cwd=ctx.shell.cwd, check=True)

            ctx.shell.run("./boot", [])
            ctx.shell.run("./configure", [])

            if loose_version < [9, 0, 1]:  # type: ignore
                # See: <https://gitlab.haskell.org/ghc/ghc/-/commit/982aaa837aed564ae9b418cda8e97d4facff8fb8>
                cabal_project = ctx.shell.cmd_stdout(
                    "sed",
                    [
                        "-E",
                        "s/index-state:.*$/index-state: 2020-03-28T07:24:23Z/",
                        "hadrian/cabal.project",
                    ],
                )
                ctx.shell.write_file("hadrian/cabal.project", cabal_project)

            ctx.shell.run("cabal", ["new-update"])

            if loose_version < [9, 0, 1]:  # type: ignore
                ctx.shell.run("./hadrian/build.sh", ["-j"])
            else:
                ctx.shell.run("./hadrian/build", ["-j"])

    ctx.shell.rename(ctx.tmp_dir / "ghc" / "_build" / "stage1" / "bin", ctx.pkg_dir / "bin")
    ctx.shell.rename(ctx.tmp_dir / "ghc" / "_build" / "stage1" / "lib", ctx.pkg_dir / "lib")
    ctx.shell.rename(ctx.tmp_dir / "ghc" / "_build" / "stage1" / "share", ctx.pkg_dir / "share")

    ctx.shell.copytree(gcc_dir, ctx.pkg_dir / f"gcc-{build_config.gcc_version}", symlinks=True)

    # Install libgmp.a into the bundled GCC sysroot, so that it can be found
    # by default without any special flags
    ctx.shell.copy(
        gmp_dir / "lib" / "libgmp.a",
        ctx.pkg_dir / f"gcc-{build_config.gcc_version}" / "sysroot" / "usr" / "lib" / "libgmp.a",
    )

    def write_lib_stub(name: str, forward_to: str) -> None:
        ctx.shell.write_file(
            ctx.pkg_dir / "lib" / f"x86_64-linux-ghc-{pkg_version}" / name,
            textwrap.dedent(
                f"""\
                /* This wrapper is a hack to get ghci working.

                   ghci tries to look for dynamic libraries(like "rt") using a very wacky
                   scheme, ultimately invoking "gcc --print-file-name -lrt"

                   The result is that it finds the libraries inside the bundled GCC sysroot,
                   which is not what we want. We want to load the system libraries.

                   By placing this file at this location, ghci will find it first and use it.
                   As you can see below, all we do is forward to the real system library.

                   Run "ghci -v" to see some clues about how ghci is searching, and see GHC
                   Linker.hs source code file for the full gory details. */
                GROUP ( {forward_to} )
                """
            ),
        )

    # These are the libs that ghci tries to look for. The so version numbers
    # listed here are expected to be identical on virtually any Linux
    # distribution that you will find
    write_lib_stub("librt.so", "librt.so.1")
    write_lib_stub("libutil.so", "libutil.so.1")
    write_lib_stub("libdl.so", "libdl.so.2")
    write_lib_stub("libpthread.so", "libpthread.so.0")

    # ghci also looks for gmp, so we need this as well
    #
    # TODO I don't think we actually need this file. ghci is already linked
    # statically with libgmp, so it shouldn't have to load any libgmp symbols
    # (it already has all of them immediately at startup). But if we don't
    # have this file then ghci will keep looking for "gmp" and eventually will
    # invoke "gcc --print-file-name" and will find and load the libgmp.a file
    # we copied above (which is bad, because Linker.hs says that loading .a
    # files is slow). Run "ghci -v" to see some clues about which files ghci
    # is looking for. The correct thing to do might be to just put a
    # "libgmp.so" file here that is an "empty" library (no symbols)
    ctx.shell.copy(
        gmp_dir / "lib-dynamic" / "libgmp.so",
        ctx.pkg_dir / "lib" / f"x86_64-linux-ghc-{pkg_version}" / "libgmp.so",
    )

    ctx.shell.rename(ctx.pkg_dir / "lib" / "settings", ctx.pkg_dir / "lib" / "settings.orig")
    ctx.shell.write_file(
        ctx.pkg_dir / "lib" / "settings", get_ghc_settings_file(pkg_version, build_config),
    )

    ctx.shell.write_file(
        "bin/ghci",
        textwrap.dedent(
            f"""\
            #!/bin/sh
            DIR=`dirname "$0"`
            exec "$DIR"/ghc --interactive "$@"
            """
        ),
    )
    os.chmod(ctx.pkg_dir / "bin" / "ghci", 0o775)


def get_ghc_settings_file(pkg_version: str, build_config: LinuxBuildConfig) -> str:
    if pkg_version == "8.10.2":
        return textwrap.dedent(
            f"""\
            [("GCC extra via C opts", "")
            ,("C compiler command", "$topdir/../gcc-{build_config.gcc_version}/bin/gcc")
            ,("C compiler flags", "")
            ,("C++ compiler flags", "")
            ,("C compiler link flags", "-fuse-ld=gold")
            ,("C compiler supports -no-pie", "YES")
            ,("Haskell CPP command", "$topdir/../gcc-{build_config.gcc_version}/bin/gcc")
            ,("Haskell CPP flags", "-E -undef -traditional")
            ,("ld command", "$topdir/../gcc-{build_config.gcc_version}/bin/ld.gold")
            ,("ld flags", "")
            ,("ld supports compact unwind", "YES")
            ,("ld supports build-id", "YES")
            ,("ld supports filelist", "NO")
            ,("ld is GNU ld", "YES")
            ,("ar command", "$topdir/../gcc-{build_config.gcc_version}/bin/ar")
            ,("ar flags", "q")
            ,("ar supports at file", "YES")
            ,("ranlib command", "$topdir/../gcc-{build_config.gcc_version}/bin/ranlib")
            ,("touch command", "touch")
            ,("dllwrap command", "/bin/false")
            ,("windres command", "/bin/false")
            ,("libtool command", "libtool")
            ,("unlit command", "$topdir/bin/unlit")
            ,("cross compiling", "NO")
            ,("target platform string", "x86_64-unknown-linux")
            ,("target os", "OSLinux")
            ,("target arch", "ArchX86_64")
            ,("target word size", "8")
            ,("target has GNU nonexec stack", "YES")
            ,("target has .ident directive", "YES")
            ,("target has subsections via symbols", "NO")
            ,("target has RTS linker", "YES")
            ,("Unregisterised", "NO")
            ,("LLVM target", "x86_64-unknown-linux")
            ,("LLVM llc command", "llc")
            ,("LLVM opt command", "opt")
            ,("LLVM clang command", "clang")
            ,("integer library", "integer-gmp")
            ,("Use interpreter", "YES")
            ,("Use native code generator", "YES")
            ,("Support SMP", "YES")
            ,("RTS ways", "v thr p thr_p debug_p thr_debug_p l thr_l debug thr_debug dyn thr_dyn debug_dyn l_dyn thr_debug_dyn thr_l_dyn")
            ,("Tables next to code", "YES")
            ,("Leading underscore", "NO")
            ,("Use LibFFI", "NO")
            ,("Use Threads", "YES")
            ,("Use Debugging", "NO")
            ,("RTS expects libdw", "NO")
            ]
            """
        )
    elif pkg_version == "8.10.3" or pkg_version == "8.10.4":
        return textwrap.dedent(
            f"""\
            [("GCC extra via C opts", "")
            ,("C compiler command", "$topdir/../gcc-{build_config.gcc_version}/bin/gcc")
            ,("C compiler flags", "")
            ,("C++ compiler flags", "")
            ,("C compiler link flags", "-fuse-ld=gold")
            ,("C compiler supports -no-pie", "YES")
            ,("Haskell CPP command", "$topdir/../gcc-{build_config.gcc_version}/bin/gcc")
            ,("Haskell CPP flags", "-E -undef -traditional")
            ,("ld command", "$topdir/../gcc-{build_config.gcc_version}/bin/ld.gold")
            ,("ld flags", "")
            ,("ld supports compact unwind", "YES")
            ,("ld supports build-id", "YES")
            ,("ld supports filelist", "NO")
            ,("ld is GNU ld", "YES")
            ,("Merge objects command", "$topdir/../gcc-{build_config.gcc_version}/bin/ld.gold")
            ,("Merge objects flags", "-r")
            ,("ar command", "$topdir/../gcc-{build_config.gcc_version}/bin/ar")
            ,("ar flags", "q")
            ,("ar supports at file", "YES")
            ,("ranlib command", "$topdir/../gcc-{build_config.gcc_version}/bin/ranlib")
            ,("otool command", "otool")
            ,("install_name_tool command", "install_name_tool")
            ,("touch command", "touch")
            ,("dllwrap command", "/bin/false")
            ,("windres command", "/bin/false")
            ,("libtool command", "libtool")
            ,("unlit command", "$topdir/bin/unlit")
            ,("cross compiling", "NO")
            ,("target platform string", "x86_64-unknown-linux")
            ,("target os", "OSLinux")
            ,("target arch", "ArchX86_64")
            ,("target word size", "8")
            ,("target has GNU nonexec stack", "YES")
            ,("target has .ident directive", "YES")
            ,("target has subsections via symbols", "NO")
            ,("target has RTS linker", "YES")
            ,("Unregisterised", "NO")
            ,("LLVM target", "x86_64-unknown-linux")
            ,("LLVM llc command", "llc")
            ,("LLVM opt command", "opt")
            ,("LLVM clang command", "clang")
            ,("integer library", "integer-gmp")
            ,("Use interpreter", "YES")
            ,("Use native code generator", "YES")
            ,("Support SMP", "YES")
            ,("RTS ways", "v thr p thr_p debug_p thr_debug_p l thr_l debug thr_debug dyn thr_dyn debug_dyn l_dyn thr_debug_dyn thr_l_dyn")
            ,("Tables next to code", "YES")
            ,("Leading underscore", "NO")
            ,("Use LibFFI", "NO")
            ,("Use Threads", "YES")
            ,("Use Debugging", "NO")
            ,("RTS expects libdw", "NO")
            ]
            """
        )
    elif pkg_version == "9.0.1":
        return textwrap.dedent(
            f"""\
            [("GCC extra via C opts", "")
            ,("C compiler command", "$topdir/../gcc-{build_config.gcc_version}/bin/gcc")
            ,("C compiler flags", "")
            ,("C++ compiler flags", "")
            ,("C compiler link flags", "-fuse-ld=gold")
            ,("C compiler supports -no-pie", "YES")
            ,("Haskell CPP command", "$topdir/../gcc-{build_config.gcc_version}/bin/gcc")
            ,("Haskell CPP flags", "-E -undef -traditional")
            ,("ld command", "$topdir/../gcc-{build_config.gcc_version}/bin/ld.gold")
            ,("ld flags", "")
            ,("ld supports compact unwind", "YES")
            ,("ld supports build-id", "YES")
            ,("ld supports filelist", "NO")
            ,("ld is GNU ld", "YES")
            ,("Merge objects command", "$topdir/../gcc-{build_config.gcc_version}/bin/ld.gold")
            ,("Merge objects flags", "-r")
            ,("ar command", "$topdir/../gcc-{build_config.gcc_version}/bin/ar")
            ,("ar flags", "q")
            ,("ar supports at file", "YES")
            ,("ranlib command", "$topdir/../gcc-{build_config.gcc_version}/bin/ranlib")
            ,("otool command", "otool")
            ,("install_name_tool command", "install_name_tool")
            ,("touch command", "touch")
            ,("dllwrap command", "/bin/false")
            ,("windres command", "/bin/false")
            ,("libtool command", "libtool")
            ,("unlit command", "$topdir/bin/unlit")
            ,("cross compiling", "NO")
            ,("target platform string", "x86_64-unknown-linux")
            ,("target os", "OSLinux")
            ,("target arch", "ArchX86_64")
            ,("target word size", "8")
            ,("target word big endian", "NO")
            ,("target has GNU nonexec stack", "YES")
            ,("target has .ident directive", "YES")
            ,("target has subsections via symbols", "NO")
            ,("target has RTS linker", "YES")
            ,("Unregisterised", "NO")
            ,("LLVM target", "x86_64-unknown-linux")
            ,("LLVM llc command", "llc")
            ,("LLVM opt command", "opt")
            ,("LLVM clang command", "clang")
            ,("BigNum backend", "gmp")
            ,("Use interpreter", "YES")
            ,("Support SMP", "YES")
            ,("RTS ways", "v thr p thr_p debug_p thr_debug_p l thr_l debug thr_debug dyn thr_dyn debug_dyn l_dyn thr_debug_dyn thr_l_dyn")
            ,("Tables next to code", "YES")
            ,("Leading underscore", "NO")
            ,("Use LibFFI", "NO")
            ,("Use Threads", "YES")
            ,("Use Debugging", "NO")
            ,("RTS expects libdw", "NO")
            ]
            """
        )
    else:
        raise NotImplementedError(f"Unsupported pkg_version: {pkg_version}")
