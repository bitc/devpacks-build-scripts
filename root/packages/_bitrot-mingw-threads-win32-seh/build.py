import os
import os.path
import subprocess

import pkglib


class Package(pkglib.PackageBuilder):
    name = "mingw-threads-win32-seh"
    version_number = "7.2.0"
    version_rev = "rev1"
    version = f"{version_number}-{version_rev}"

    _7_zip_version = "18.01"

    sha256s = {
        "https://netix.dl.sourceforge.net/project/mingw-w64/Toolchains%20targetting%20Win64/Personal%20Builds/mingw-builds/7.2.0/threads-win32/seh/x86_64-7.2.0-release-win32-seh-rt_v5-rev1.7z": "504f66ce6ba70ec70f9232e3872c6e38e3224fd6b7c2df9c6f479a9aa834cf68"
    }

    def make_pkg(self):
        url = {
            (
                "windows",
                "x86_64",
            ): f"https://netix.dl.sourceforge.net/project/mingw-w64/Toolchains%20targetting%20Win64/Personal%20Builds/mingw-builds/{Package.version_number}/threads-win32/seh/x86_64-{Package.version_number}-release-win32-seh-rt_v5-{Package.version_rev}.7z"
        }[self.os, self.arch]

        if self.os == "windows":
            _7_zip_prog = subprocess.run(
                ["devpacks", "quick-pkg-path", "7-zip", "-t", Package._7_zip_version, "-p", "7z",],
                encoding="utf-8",
                check=True,
                stdout=subprocess.PIPE,
            ).stdout

            self.download_file(url, os.path.join(self.pkg_dir, "mingw-download.7z"))
            subprocess.run(
                [
                    _7_zip_prog,
                    "x",
                    "-o" + self.pkg_dir,
                    os.path.join(self.pkg_dir, "mingw-download.7z"),
                ],
                check=True,
            )
            os.remove(os.path.join(self.pkg_dir, "mingw-download.7z"))

            files = os.listdir(self.pkg_dir)
            if len(files) != 1:
                raise RuntimeError("Downloaded archive does not contain a single directory")
            subdir = files[0]
            if not os.path.isdir(os.path.join(self.pkg_dir, subdir)):
                raise RuntimeError("Extracted archive is not a directory")
            for f in os.listdir(os.path.join(self.pkg_dir, subdir)):
                os.rename(os.path.join(self.pkg_dir, subdir, f), os.path.join(self.pkg_dir, f))
            os.rmdir(os.path.join(self.pkg_dir, subdir))
        else:
            raise RuntimeError(f"Platform {self.os} not supported")

    def create_tests(self, pkg_dir):
        class PackageTestCase(pkglib.PackageTestCase):
            def test_gcc(self):
                out = self.run_program(pkg_dir, "gcc", ["--version"])
                out_version = " ".join(out.split("\n")[0].split()[6:7])
                self.assertEqual(out_version, f"{Package.version_number}")

            def test_gxx(self):
                out = self.run_program(pkg_dir, "g++", ["--version"])
                out_version = " ".join(out.split("\n")[0].split()[6:7])
                self.assertEqual(out_version, f"{Package.version_number}")

        return PackageTestCase


if __name__ == "__main__":
    Package().run()
