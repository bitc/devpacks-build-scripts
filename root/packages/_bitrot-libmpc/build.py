import os.path
from collections import OrderedDict
from pathlib import Path
from typing import Dict, NamedTuple, Type

from ...lib.main_driver import main_driver
from ...lib.test import BuildContext, PackageDescr, PackageTestCase


class MPCOptions(NamedTuple):
    shared: bool


class BuildConfig(NamedTuple):
    make_version: str
    gmp_version: str
    mpfr_version: str


build_configs: Dict[str, BuildConfig] = OrderedDict(
    [
        ("1.1.0", BuildConfig(make_version="4.1", gmp_version="6.1.2", mpfr_version="4.0.2")),
        ("1.2.0", BuildConfig(make_version="4.1", gmp_version="6.2.0", mpfr_version="4.1.0")),
    ]
)


def make_mpc(ctx: BuildContext, pkg_version: str, options: MPCOptions) -> None:
    build_config = build_configs[pkg_version]

    make_dir = ctx.devpacks_pkg("make", build_config.make_version)
    gmp_dir = ctx.devpacks_pkg("libgmp", build_config.gmp_version)
    mpfr_dir = ctx.devpacks_pkg("libmpfr", build_config.mpfr_version)

    url = f"https://ftp.gnu.org/gnu/mpc/mpc-{pkg_version}.tar.gz"

    ctx.download_root(url)

    configure_flags = [
        "--prefix=" + str(ctx.pkg_dir / "local"),
        "--build=x86_64-linux-gnu",
        f"--with-gmp={gmp_dir}",
        f"--with-mpfr={mpfr_dir}",
        "CFLAGS=-fPIC -O3",
    ]

    if not options.shared:
        configure_flags = configure_flags + ["--disable-shared"]

    ctx.shell.env["PATH"] = str(make_dir / "bin") + os.pathsep + ctx.shell.env["PATH"]

    ctx.shell.run("./configure", configure_flags)

    ctx.shell.run("make", [])
    ctx.shell.run("make", ["install"])
    ctx.shell.run("make", ["check"])

    ctx.shell.rename("local/include", "include")
    ctx.shell.rename("local/lib", "lib")

    ctx.shell.remove("lib/libmpc.la")


def make_pkg(ctx: BuildContext, pkg_version: str) -> None:
    make_mpc(ctx, pkg_version, MPCOptions(shared=False))


def create_tests(pkg_dir: Path, pkg_version: str) -> Type[PackageTestCase]:
    class TestCase(PackageTestCase):
        def test_mpc_h(self) -> None:
            exists = os.path.isfile(os.path.join(pkg_dir, "include", "mpc.h"))
            self.assertTrue(exists, "mpc.h header file exists")

        def test_libmpc_a(self) -> None:
            exists = os.path.isfile(os.path.join(pkg_dir, "lib", "libmpc.a"))
            self.assertTrue(exists, "libmpc.a lib file exists")

    return TestCase


pkg = PackageDescr(
    name="libmpc",
    versions=list(build_configs.keys()),
    make_pkg=make_pkg,
    create_tests=create_tests,
    sha256s={
        "https://ftp.gnu.org/gnu/mpc/mpc-1.1.0.tar.gz": "6985c538143c1208dcb1ac42cedad6ff52e267b47e5f970183a3e75125b43c2e",
        "https://ftp.gnu.org/gnu/mpc/mpc-1.2.0.tar.gz": "e90f2d99553a9c19911abdb4305bf8217106a957e3994436428572c8dfe8fda6",
    },
)

if __name__ == "__main__":
    main_driver(pkg)
