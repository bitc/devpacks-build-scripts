import distutils.version
import glob
import os
import os.path
import subprocess
from collections import OrderedDict
from pathlib import Path
from typing import Dict, NamedTuple, Type

from ...lib.main_driver import main_driver
from ...lib.test import BuildContext, PackageDescr, PackageTestCase


class BuildConfig(NamedTuple):
    gcc_version: str
    make_version: str
    zlib_version: str
    patchelf_version: str


build_configs: Dict[str, BuildConfig] = OrderedDict(
    [
        (
            "1.0.2p",
            BuildConfig(
                gcc_version="7.5.0",
                make_version="4.1",
                zlib_version="1.2.11",
                patchelf_version="0.12",
            ),
        ),
        (
            "1.0.2u",
            BuildConfig(
                gcc_version="7.5.0",
                make_version="4.1",
                zlib_version="1.2.11",
                patchelf_version="0.12",
            ),
        ),
        (
            "1.1.0g",
            BuildConfig(
                gcc_version="7.5.0",
                make_version="4.1",
                zlib_version="1.2.11",
                patchelf_version="0.12",
            ),
        ),
        (
            "1.1.0l",
            BuildConfig(
                gcc_version="7.5.0",
                make_version="4.1",
                zlib_version="1.2.11",
                patchelf_version="0.12",
            ),
        ),
        (
            "1.1.1g",
            BuildConfig(
                gcc_version="7.5.0",
                make_version="4.1",
                zlib_version="1.2.11",
                patchelf_version="0.12",
            ),
        ),
        (
            "1.1.1j",
            BuildConfig(
                gcc_version="7.5.0",
                make_version="4.1",
                zlib_version="1.2.11",
                patchelf_version="0.12",
            ),
        ),
    ]
)


def make_pkg(ctx: BuildContext, pkg_version: str) -> None:
    build_config = build_configs[pkg_version]

    gcc_dir = ctx.devpacks_pkg("gcc", build_config.gcc_version)
    make_dir = ctx.devpacks_pkg("make", build_config.make_version)
    zlib_dir = ctx.devpacks_pkg("zlib", build_config.zlib_version)
    patchelf_prog = ctx.devpacks_pkg_prog("patchelf", build_config.patchelf_version, "patchelf")

    url = f"https://www.openssl.org/source/openssl-{pkg_version}.tar.gz"

    ctx.shell.mkdir(ctx.tmp_dir / "static")
    ctx.shell.mkdir(ctx.tmp_dir / "dynamic")
    ctx.download_extract(url, ctx.tmp_dir / "static")
    ctx.download_extract(url, ctx.tmp_dir / "dynamic")

    with ctx.shell.cd(ctx.tmp_dir / "static"):
        ctx.shell.env["PATH"] = os.path.pathsep.join(
            [str(Path(gcc_dir) / "bin"), str(Path(make_dir) / "bin"), ctx.shell.env["PATH"],]
        )

        if distutils.version.LooseVersion(pkg_version).version < [1, 1, 1]:  # type: ignore
            ctx.shell.run(
                "./config",
                [
                    "--prefix=" + str(ctx.tmp_dir / "static" / "local"),
                    "no-shared",
                    "threads",
                    "zlib",
                    "no-zlib-dynamic",
                    "-fPIC",
                    f'-I{zlib_dir / "include"}',
                    f'-L{zlib_dir / "lib"}',
                ],
            )
        else:
            ctx.shell.run(
                "./config",
                [
                    "--prefix=" + str(ctx.tmp_dir / "static" / "local"),
                    "no-shared",
                    "threads",
                    "zlib",
                    "no-zlib-dynamic",
                    f'CPPFLAGS=-I{zlib_dir / "include"}',
                    "CFLAGS=-fPIC",
                    f'LDFLAGS=-L{zlib_dir / "lib"}',
                ],
            )

        ctx.shell.run("make", [])
        ctx.shell.run("make", ["install"])

        ctx.shell.rename("local/bin", ctx.pkg_dir / "bin")
        ctx.shell.rename("local/include", ctx.pkg_dir / "include")
        ctx.shell.rename("local/lib", ctx.pkg_dir / "lib")
        ctx.shell.rename("local/ssl", ctx.pkg_dir / "ssl")
        if distutils.version.LooseVersion(pkg_version).version >= [1, 1, 0]:  # type: ignore
            ctx.shell.rename("local/share", ctx.pkg_dir / "share")

    with ctx.shell.cd(ctx.tmp_dir / "dynamic"):
        ctx.shell.env["PATH"] = os.path.pathsep.join(
            [str(Path(gcc_dir) / "bin"), str(Path(make_dir) / "bin"), ctx.shell.env["PATH"],]
        )

        if distutils.version.LooseVersion(pkg_version).version < [1, 1, 1]:  # type: ignore
            ctx.shell.run(
                "./config",
                [
                    "--prefix=" + str(ctx.tmp_dir / "dynamic" / "local"),
                    "shared",
                    "threads",
                    "zlib",
                    "no-zlib-dynamic",
                    "-fPIC",
                    f'-I{zlib_dir / "include"}',
                    f'-L{zlib_dir / "lib-dynamic"}',
                ],
            )
        else:
            ctx.shell.run(
                "./config",
                [
                    "--prefix=" + str(ctx.tmp_dir / "dynamic" / "local"),
                    "shared",
                    "threads",
                    "zlib",
                    "no-zlib-dynamic",
                    f'CPPFLAGS=-I{zlib_dir / "include"}',
                    "CFLAGS=-fPIC",
                    f'LDFLAGS=-L{zlib_dir / "lib-dynamic"}',
                ],
            )

        ctx.shell.run("make", [])
        ctx.shell.run("make", ["install"])

        ctx.shell.mkdir(ctx.pkg_dir / "lib-dynamic")
        for so_file in glob.glob(str(ctx.tmp_dir / "dynamic" / "local" / "lib" / "*.so*")):
            if not os.path.islink(so_file):
                ctx.shell.chmod(so_file, 0o755)
                ctx.shell.run(patchelf_prog, ["--set-rpath", "$ORIGIN", so_file])
            ctx.shell.rename(so_file, ctx.pkg_dir / "lib-dynamic" / os.path.basename(so_file))

        ctx.shell.copy(
            zlib_dir / "lib-dynamic" / "libz.so.1", ctx.pkg_dir / "lib-dynamic" / "libz.so.1"
        )

        if distutils.version.LooseVersion(pkg_version).version < [1, 1, 0]:  # type: ignore
            engines_dir = "engines"
        else:
            engines_dir = "engines-1.1"
        ctx.shell.rename(Path("local/lib") / engines_dir, ctx.pkg_dir / "lib-dynamic" / engines_dir)
        for so_file in glob.glob(str(ctx.pkg_dir / "lib-dynamic" / engines_dir / "*.so*")):
            ctx.shell.chmod(so_file, 0o755)
            ctx.shell.run(patchelf_prog, ["--set-rpath", "$ORIGIN/..", so_file])


def create_tests(pkg_dir: Path, pkg_version: str) -> Type[PackageTestCase]:
    class TestCase(PackageTestCase):
        def test_ssl_h(self) -> None:
            self.assertTrue(os.path.isfile(pkg_dir / "include" / "openssl" / "ssl.h"))

        def test_lib(self) -> None:
            self.assertTrue(os.path.isfile(pkg_dir / "lib" / "libcrypto.a"))
            self.assertTrue(os.path.isfile(pkg_dir / "lib" / "libssl.a"))

        def test_lib_dynamic(self) -> None:
            self.assertTrue(os.path.isfile(pkg_dir / "lib-dynamic" / "libcrypto.so"))
            self.assertTrue(os.path.isfile(pkg_dir / "lib-dynamic" / "libssl.so"))
            self.assertTrue(os.path.isfile(pkg_dir / "lib-dynamic" / "libz.so.1"))

        def test_engines(self) -> None:
            if distutils.version.LooseVersion(pkg_version).version < [1, 1, 0]:  # type: ignore
                self.assertTrue(
                    os.path.isfile(pkg_dir / "lib-dynamic" / "engines" / "lib4758cca.so")
                )
                self.assertTrue(os.path.isfile(pkg_dir / "lib-dynamic" / "engines" / "libaep.so"))
            else:
                self.assertTrue(os.path.isfile(pkg_dir / "lib-dynamic" / "engines-1.1" / "capi.so"))
                self.assertTrue(
                    os.path.isfile(pkg_dir / "lib-dynamic" / "engines-1.1" / "padlock.so")
                )

    return TestCase


pkg = PackageDescr(
    name="openssl",
    versions=list(build_configs.keys()),
    make_pkg=make_pkg,
    create_tests=create_tests,
    sha256s={
        "https://www.openssl.org/source/openssl-1.0.2p.tar.gz": "50a98e07b1a89eb8f6a99477f262df71c6fa7bef77df4dc83025a2845c827d00",
        "https://www.openssl.org/source/openssl-1.0.2u.tar.gz": "ecd0c6ffb493dd06707d38b14bb4d8c2288bb7033735606569d8f90f89669d16",
        "https://www.openssl.org/source/openssl-1.1.0g.tar.gz": "de4d501267da39310905cb6dc8c6121f7a2cad45a7707f76df828fe1b85073af",
        "https://www.openssl.org/source/openssl-1.1.0l.tar.gz": "74a2f756c64fd7386a29184dc0344f4831192d61dc2481a93a4c5dd727f41148",
        "https://www.openssl.org/source/openssl-1.1.1g.tar.gz": "ddb04774f1e32f0c49751e21b67216ac87852ceb056b75209af2443400636d46",
        "https://www.openssl.org/source/openssl-1.1.1j.tar.gz": "aaf2fcb575cdf6491b98ab4829abf78a3dec8402b8b81efc8f23c00d443981bf",
    },
)

if __name__ == "__main__":
    main_driver(pkg)
