import os
from collections import OrderedDict
from pathlib import Path
from typing import Dict, NamedTuple, Optional, Type

from ...lib.main_driver import main_driver
from ...lib.test import BuildContext, PackageDescr, PackageTestCase


class BuildConfig(NamedTuple):
    make_version: str
    gcc_version: str
    bison_version: Optional[str]


build_configs: Dict[str, BuildConfig] = OrderedDict(
    [
        ("2.27", BuildConfig(make_version="4.1", gcc_version="4.9.4", bison_version="3.0.4")),
        ("2.29.1", BuildConfig(make_version="4.1", gcc_version="5.5.0", bison_version="3.0.4")),
        ("2.31.1", BuildConfig(make_version="4.1", gcc_version="5.5.0", bison_version=None)),
        ("2.33.1", BuildConfig(make_version="4.1", gcc_version="5.5.0", bison_version=None)),
        ("2.34", BuildConfig(make_version="4.1", gcc_version="8.4.0", bison_version=None),),
    ],
)


def make_pkg(ctx: BuildContext, pkg_version: str) -> None:
    build_config = build_configs[pkg_version]

    make_dir = ctx.devpacks_pkg("make", build_config.make_version)
    if build_config.bison_version:
        bison_dir = ctx.devpacks_pkg("bison", build_config.bison_version)
    if not ctx.use_system_gcc:
        gcc_dir = ctx.devpacks_pkg("gcc", build_config.gcc_version)

    url = f"https://ftp.gnu.org/gnu/binutils/binutils-{pkg_version}.tar.gz"

    src_dir = ctx.tmp_dir / "src"
    ctx.shell.mkdir(src_dir)

    ctx.download_extract(url, src_dir)

    ctx.shell.env["PATH"] = os.pathsep.join(
        ([] if ctx.use_system_gcc else [str(gcc_dir / "bin")])
        + ([] if not build_config.bison_version else [str(bison_dir / "bin")])
        + [str(make_dir / "bin"), ctx.shell.env["PATH"]]
    )

    ctx.shell.env["LDFLAGS"] = "-static-libstdc++ -static-libgcc"

    with ctx.shell.cd(src_dir):
        ctx.shell.run(
            "./configure",
            [
                "--prefix=" + str(ctx.pkg_dir),
                "--build=x86_64-linux-gnu",
                "--enable-gold",
                "--enable-ld",
            ],
        )
        ctx.shell.run("make", [])
        ctx.shell.run("make", ["install"])


def create_tests(pkg_dir: Path, pkg_version: str) -> Type[PackageTestCase]:
    class TestCase(PackageTestCase):
        def test_as(self) -> None:
            self.assert_run_program_first_line(
                pkg_dir, "as", ["--version"], f"GNU assembler (GNU Binutils) {pkg_version}"
            )

        def test_ld(self) -> None:
            self.assert_run_program_first_line(
                pkg_dir, "ld", ["--version"], f"GNU ld (GNU Binutils) {pkg_version}"
            )

        def test_ld_gold(self) -> None:
            if not (pkg_dir / "bin" / "ld.gold").exists():
                self.skipTest(f"ld.gold not found in this version of binutils ({pkg_version})")

            self.assert_run_program_stdout_substring(
                pkg_dir, "ld.gold", ["--version"], f"GNU gold (GNU Binutils {pkg_version})"
            )

        def test_dwp(self) -> None:
            if not (pkg_dir / "bin" / "dwp").exists():
                self.skipTest(f"dwp not found in this version of binutils ({pkg_version})")

            self.assert_run_program_first_line(
                pkg_dir, "dwp", ["--version"], f"GNU dwp (GNU Binutils) {pkg_version}"
            )

        def test_strip(self) -> None:
            self.assert_run_program_first_line(
                pkg_dir, "strip", ["--version"], f"GNU strip (GNU Binutils) {pkg_version}"
            )

    return TestCase


pkg = PackageDescr(
    name="binutils",
    versions=list(build_configs.keys()),
    make_pkg=make_pkg,
    create_tests=create_tests,
    sha256s={
        "https://ftp.gnu.org/gnu/binutils/binutils-2.25.1.tar.gz": "82a40a37b13a12facb36ac7e87846475a1d80f2e63467b1b8d63ec8b6a2b63fc",
        "https://ftp.gnu.org/gnu/binutils/binutils-2.27.tar.gz": "26253bf0f360ceeba1d9ab6965c57c6a48a01a8343382130d1ed47c468a3094f",
        "https://ftp.gnu.org/gnu/binutils/binutils-2.29.1.tar.gz": "0d9d2bbf71e17903f26a676e7fba7c200e581c84b8f2f43e72d875d0e638771c",
        "https://ftp.gnu.org/gnu/binutils/binutils-2.31.1.tar.gz": "e88f8d36bd0a75d3765a4ad088d819e35f8d7ac6288049780e2fefcad18dde88",
        "https://ftp.gnu.org/gnu/binutils/binutils-2.33.1.tar.gz": "98aba5f673280451a09df3a8d8eddb3aa0c505ac183f1e2f9d00c67aa04c6f7d",
        "https://ftp.gnu.org/gnu/binutils/binutils-2.34.tar.gz": "53537d334820be13eeb8acb326d01c7c81418772d626715c7ae927a7d401cab3",
    },
)

if __name__ == "__main__":
    main_driver(pkg)
