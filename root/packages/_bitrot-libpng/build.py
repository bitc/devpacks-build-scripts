import os
import os.path
import subprocess

import pkglib


class Package(pkglib.PackageBuilder):
    name = "libpng"
    version = "1.6.34"

    zlib_version = "1.2.11"

    sha256s = {
        "https://download.sourceforge.net/libpng/libpng-1.6.34.tar.xz": "2f1e960d92ce3b3abd03d06dfec9637dfbd22febf107a536b44f7a47c60659f6"
    }

    def make_pkg(self):
        zlib_dir = subprocess.run(
            ["devpacks", "quick-pkg-path", "zlib", "-t", Package.zlib_version],
            encoding="utf-8",
            check=True,
            stdout=subprocess.PIPE,
        ).stdout

        url = f"https://download.sourceforge.net/libpng/libpng-{self.version}.tar.xz"

        self.download_root(url)

        subprocess.run(
            [
                "./configure",
                "--prefix=" + os.path.join(self.pkg_dir, "local"),
                "--disable-shared",
                "CFLAGS=-fPIC",
                f'CPPFLAGS=-I{os.path.join(zlib_dir, "include")}',
                f'LDFLAGS=-L{os.path.join(zlib_dir, "lib")}',
            ],
            cwd=self.pkg_dir,
            check=True,
        )
        subprocess.run(["make"], cwd=self.pkg_dir, check=True)
        subprocess.run(["make", "install"], cwd=self.pkg_dir, check=True)

        os.rename(
            os.path.join(self.pkg_dir, "local", "bin"), os.path.join(self.pkg_dir, "bin"),
        )
        os.rename(
            os.path.join(self.pkg_dir, "local", "include"), os.path.join(self.pkg_dir, "include"),
        )
        os.rename(
            os.path.join(self.pkg_dir, "local", "lib"), os.path.join(self.pkg_dir, "lib"),
        )

    def create_tests(self, pkg_dir):
        class PackageTestCase(pkglib.PackageTestCase):
            def test_pngfix(self):
                subprocess.run(
                    [os.path.join(pkg_dir, "bin", "pngfix"), os.path.join(pkg_dir, "pngtest.png"),],
                    check=True,
                )

            def test_png_h(self):
                exists = os.path.isfile(os.path.join(pkg_dir, "include", "png.h"))
                self.assertTrue(exists, "png.h header file exists")

            def test_libpng_a(self):
                exists = os.path.isfile(os.path.join(pkg_dir, "lib", "libpng.a"))
                self.assertTrue(exists, "libpng.a lib file exists")

        return PackageTestCase


if __name__ == "__main__":
    Package().run()
