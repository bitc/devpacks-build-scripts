import distutils.version
import os.path
import shutil
import textwrap
from collections import OrderedDict
from pathlib import Path
from typing import Dict, NamedTuple, Type

from ...lib.main_driver import main_driver
from ...lib.test import BuildContext, PackageDescr, PackageTestCase


class BuildConfig(NamedTuple):
    autoconf_version: str
    make_version: str


build_configs: Dict[str, BuildConfig] = OrderedDict(
    [
        ("1.16.2", BuildConfig(autoconf_version="2.69", make_version="4.1")),
        ("1.16.3", BuildConfig(autoconf_version="2.69", make_version="4.1")),
    ]
)


def make_pkg(ctx: BuildContext, pkg_version: str) -> None:
    build_config = build_configs[pkg_version]

    autoconf_dir = ctx.devpacks_pkg("autoconf", build_config.autoconf_version)
    make_dir = ctx.devpacks_pkg("make", build_config.make_version)

    loose_version = distutils.version.LooseVersion(pkg_version)
    major_minor = str(loose_version.version[0]) + "." + str(loose_version.version[1])

    url = f"https://ftp.gnu.org/gnu/automake/automake-{pkg_version}.tar.gz"

    ctx.download_root(url)

    ctx.shell.env["PATH"] = os.path.pathsep.join(
        [str(Path(autoconf_dir) / "bin"), str(Path(make_dir) / "bin"), ctx.shell.env["PATH"],]
    )

    ctx.shell.run(
        "./configure", ["--prefix=" + str(ctx.pkg_dir / "local"),],
    )
    ctx.shell.run("make", [])
    ctx.shell.run("make", ["install"])

    shutil.rmtree(ctx.pkg_dir / "bin")

    ctx.shell.mkdir("bin")
    ctx.shell.rename("local/share", "share")

    def install_perl_exe(name: str) -> None:
        ctx.shell.write_file(
            ctx.pkg_dir / "bin" / name,
            textwrap.dedent(
                f"""\
                #!/bin/sh
                DIR=`dirname "$0"`
                export PERL5LIB="$DIR/../share/automake-{major_minor}"
                export ACLOCAL_PATH="$DIR/../share/aclocal-{major_minor}"
                export ACLOCAL_AUTOMAKE_DIR="$DIR/../share/aclocal-{major_minor}"
                export AUTOMAKE_UNINSTALLED=1
                exec "$DIR/../local/bin/{name}" "$@"
                """
            ),
        )
        os.chmod(ctx.pkg_dir / "bin" / name, 0o775)

    install_perl_exe("aclocal")
    install_perl_exe(f"aclocal-{major_minor}")
    install_perl_exe("automake")
    install_perl_exe(f"automake-{major_minor}")


def create_tests(pkg_dir: Path, pkg_version: str) -> Type[PackageTestCase]:
    class TestCase(PackageTestCase):
        def test_aclocal(self) -> None:
            self.assert_run_program_first_line(
                pkg_dir, "aclocal", ["--version"], f"aclocal (GNU automake) {pkg_version}",
            )

        def test_automake(self) -> None:
            self.assert_run_program_first_line(
                pkg_dir, "automake", ["--version"], f"automake (GNU automake) {pkg_version}",
            )

    return TestCase


pkg = PackageDescr(
    name="automake",
    versions=list(build_configs.keys()),
    make_pkg=make_pkg,
    create_tests=create_tests,
    sha256s={
        "https://ftp.gnu.org/gnu/automake/automake-1.16.2.tar.gz": "b2f361094b410b4acbf4efba7337bdb786335ca09eb2518635a09fb7319ca5c1",
        "https://ftp.gnu.org/gnu/automake/automake-1.16.3.tar.gz": "ce010788b51f64511a1e9bb2a1ec626037c6d0e7ede32c1c103611b9d3cba65f",
    },
)

if __name__ == "__main__":
    main_driver(pkg)
