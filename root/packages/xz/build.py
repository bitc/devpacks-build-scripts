import glob
import os
import os.path
import subprocess
from collections import OrderedDict
from pathlib import Path
from typing import Dict, NamedTuple, Type

from ...lib.main_driver import main_driver
from ...lib.test import BuildContext, PackageDescr, PackageTestCase


class BuildConfig(NamedTuple):
    gcc_version: str
    make_version: str


build_configs: Dict[str, BuildConfig] = OrderedDict(
    [("5.2.3", BuildConfig(gcc_version="7.5.0", make_version="4.1")),]
)


def make_pkg(ctx: BuildContext, pkg_version: str) -> None:
    build_config = build_configs[pkg_version]

    gcc_dir = ctx.devpacks_pkg("gcc", build_config.gcc_version)
    make_dir = ctx.devpacks_pkg("make", build_config.make_version)

    url = f"https://tukaani.org/xz/xz-{pkg_version}.tar.xz"

    ctx.shell.mkdir(ctx.tmp_dir / "static")
    ctx.shell.mkdir(ctx.tmp_dir / "dynamic")
    ctx.download_extract(url, ctx.tmp_dir / "static")
    ctx.download_extract(url, ctx.tmp_dir / "dynamic")

    common_configure_flags = [
        "CFLAGS=-fPIC",
    ]

    with ctx.shell.cd(ctx.tmp_dir / "static"):
        ctx.shell.env["PATH"] = os.path.pathsep.join(
            [str(Path(gcc_dir) / "bin"), str(Path(make_dir) / "bin"), ctx.shell.env["PATH"],]
        )

        ctx.shell.run(
            "./configure",
            ["--prefix=" + str(ctx.tmp_dir / "static" / "local"), "--disable-shared",]
            + common_configure_flags,
        )

        ctx.shell.run("make", [])
        ctx.shell.run("make", ["install"])

        ctx.shell.rename("local/bin", ctx.pkg_dir / "bin")
        ctx.shell.rename("local/include", ctx.pkg_dir / "include")
        ctx.shell.rename("local/lib", ctx.pkg_dir / "lib")
        ctx.shell.rename("local/share", ctx.pkg_dir / "share")

    with ctx.shell.cd(ctx.tmp_dir / "dynamic"):
        ctx.shell.env["PATH"] = os.path.pathsep.join(
            [str(Path(gcc_dir) / "bin"), str(Path(make_dir) / "bin"), ctx.shell.env["PATH"],]
        )

        ctx.shell.run(
            "./configure",
            ["--prefix=" + str(ctx.tmp_dir / "dynamic" / "local"), "--enable-shared",]
            + common_configure_flags,
        )

        ctx.shell.run("make", [])
        ctx.shell.run("make", ["install"])

        ctx.shell.mkdir(ctx.pkg_dir / "lib-dynamic")
        for so_file in glob.glob(str(ctx.tmp_dir / "dynamic" / "local" / "lib" / "*.so*")):
            ctx.shell.rename(so_file, ctx.pkg_dir / "lib-dynamic" / os.path.basename(so_file))


def create_tests(pkg_dir: Path, pkg_version: str) -> Type[PackageTestCase]:
    class TestCase(PackageTestCase):
        def test_xz(self) -> None:
            self.assert_run_program(
                pkg_dir, "xz", ["--version"], f"xz (XZ Utils) {pkg_version}\nliblzma {pkg_version}"
            )

        def test_lzma_h(self) -> None:
            self.assertTrue(os.path.isfile(pkg_dir / "include" / "lzma.h"))

        def test_liblzma(self) -> None:
            self.assertTrue(os.path.isfile(pkg_dir / "lib" / "liblzma.a"))

        def test_liblzma_dynamic(self) -> None:
            self.assertTrue(os.path.isfile(pkg_dir / "lib-dynamic" / "liblzma.so"))

    return TestCase

    def create_tests(self, pkg_dir):
        class PackageTestCase(pkglib.PackageTestCase):
            def test_xz(self):
                self.assert_run_program(
                    pkg_dir,
                    "xz",
                    ["--version"],
                    f"xz (XZ Utils) {Package.version}\nliblzma {Package.version}",
                )

            def test_lzma_h(self):
                exists = os.path.isfile(os.path.join(pkg_dir, "include", "lzma.h"))
                self.assertTrue(exists, "lzma.h header file exists")

            def test_lzma_a(self):
                exists = os.path.isfile(os.path.join(pkg_dir, "lib", "liblzma.a"))
                self.assertTrue(exists, "liblzma.a lib file exists")

        return PackageTestCase


pkg = PackageDescr(
    name="xz",
    versions=list(build_configs.keys()),
    make_pkg=make_pkg,
    create_tests=create_tests,
    sha256s={
        f"https://tukaani.org/xz/xz-5.2.3.tar.xz": "7876096b053ad598c31f6df35f7de5cd9ff2ba3162e5a5554e4fc198447e0347"
    },
)

if __name__ == "__main__":
    main_driver(pkg)
