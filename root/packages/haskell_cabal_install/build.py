import os.path
import subprocess
import textwrap
from collections import OrderedDict
from pathlib import Path
from typing import Dict, NamedTuple, Tuple, Type

from ...lib.ecosystems.haskell.hackage import HackageBuildConfig
from ...lib.main_driver import main_driver
from ...lib.pkglib import strip_win_extended_path
from ...lib.test import BuildContext, PackageDescr, PackageTestCase
from .build_windows import compile_cabal_wrapper


class BuildConfig(NamedTuple):
    lukko_version: str
    mingw_threads_win32_seh_version: str
    msys2_version: str
    zlib_version: str


build_configs: Dict[str, Tuple[HackageBuildConfig, BuildConfig]] = OrderedDict(
    [
        (
            "3.4.0.0",
            (
                HackageBuildConfig(ghc_version="8.10.3", cabal_install_version="3.4.0.0"),
                BuildConfig(
                    lukko_version="0.1.1.3",
                    mingw_threads_win32_seh_version="7.2.0-rev1",
                    msys2_version="20201109",
                    zlib_version="1.2.11",
                ),
            ),
        ),
    ]
)


def make_pkg(ctx: BuildContext, pkg_version: str) -> None:
    hackage_build_config, build_config = build_configs[pkg_version]

    if ctx.os != "windows":
        zlib_dir = ctx.devpacks_pkg("zlib", build_config.zlib_version)

        ctx.shell.env["C_INCLUDE_PATH"] = f"{zlib_dir}/include"
        ctx.shell.env["LIBRARY_PATH"] = f"{zlib_dir}/lib"

    ghc_prog = ctx.devpacks_pkg_prog("ghc", hackage_build_config.ghc_version, "ghc")
    cabal_prog = ctx.devpacks_pkg_prog(
        "haskell_cabal_install", hackage_build_config.cabal_install_version, "cabal"
    )

    pkg_url = f"https://hackage.haskell.org/package/cabal-install-{pkg_version}/cabal-install-{pkg_version}.tar.gz"
    lukko_pkg_url = f"https://hackage.haskell.org/package/lukko-{build_config.lukko_version}/lukko-{build_config.lukko_version}.tar.gz"

    os.mkdir(ctx.tmp_dir / "src")
    os.mkdir(ctx.tmp_dir / "src" / "cabal-install")
    os.mkdir(ctx.tmp_dir / "src" / "lukko")
    ctx.download_extract(pkg_url, ctx.tmp_dir / "src" / "cabal-install")
    ctx.download_extract(lukko_pkg_url, ctx.tmp_dir / "src" / "lukko")

    ctx.shell.env["CABAL_DIR"] = f"{strip_win_extended_path(ctx.tmp_dir / 'cabal')}"

    ctx.shell.write_file(
        ctx.tmp_dir / "src" / "cabal.project",
        textwrap.dedent(
            f"""\
            packages: cabal-install/
                      lukko/
            """
        ),
    )

    if ctx.os == "linux":
        print("Applying patch file patch-lukko-linux-fcntl.patch")
        with open(
            Path(os.path.dirname(os.path.realpath(__file__))) / "patch-lukko-linux-fcntl.patch",
            "r",
        ) as f:
            subprocess.run(["patch", "-p1"], stdin=f, cwd=ctx.tmp_dir / "src" / "lukko", check=True)

    if ctx.os == "windows":
        # We bundle MSYS2 so that cabal packages that use "configure"
        # scripts will work
        msys2_base_url = f"https://repo.msys2.org/distrib/x86_64/msys2-base-x86_64-{build_config.msys2_version}.tar.xz"
        ctx.shell.mkdir(ctx.pkg_dir / "msys2")
        ctx.download_extract(msys2_base_url, ctx.pkg_dir / "msys2")

    with ctx.shell.cd(strip_win_extended_path(ctx.tmp_dir / "src")):
        ctx.shell.run(cabal_prog, ["v2-update"])
        constraints = []
        if ctx.os == "windows":
            # On windows we must use "process" package < 1.6.9 otherwise the
            # resulting build is buggy on Windows 7.
            #
            # See: <https://github.com/haskell/cabal/issues/7309>
            constraints = ["--constraint=process==1.6.8.2"]
        ctx.shell.run(
            cabal_prog, ["v2-build", f"--with-ghc={ghc_prog}"] + constraints + ["cabal-install"]
        )

        if ctx.os == "windows":
            which_cmd = "where"
        else:
            which_cmd = "which"

        output_file = (
            ctx.shell.cmd_stdout(
                cabal_prog,
                ["v2-exec", f"--with-ghc={ghc_prog}"] + constraints + ["--", which_cmd, "cabal"],
            )
            .strip()
            .split("\n")[-1]
        )

        ctx.shell.mkdir(ctx.pkg_dir / "bin")

        if ctx.os == "windows":
            ctx.shell.rename(output_file, ctx.pkg_dir / os.path.basename(output_file))
            compile_cabal_wrapper(ctx, build_config.mingw_threads_win32_seh_version)
        else:
            ctx.shell.rename(output_file, ctx.pkg_dir / "bin" / os.path.basename(output_file))


def create_tests(pkg_dir: Path, pkg_version: str) -> Type[PackageTestCase]:
    class TestCase(PackageTestCase):
        def test_cabal(self):
            self.assert_run_program_first_line(
                pkg_dir, "cabal", ["--version"], f"cabal-install version {pkg_version}"
            )

    return TestCase


pkg = PackageDescr(
    name="haskell_cabal_install",
    versions=list(build_configs.keys()),
    make_pkg=make_pkg,
    create_tests=create_tests,
    sha256s={
        "https://hackage.haskell.org/package/cabal-install-3.2.0.0/cabal-install-3.2.0.0.tar.gz": "a0555e895aaf17ca08453fde8b19af96725da8398e027aa43a49c1658a600cb0",
        "https://hackage.haskell.org/package/cabal-install-3.4.0.0/cabal-install-3.4.0.0.tar.gz": "1980ef3fb30001ca8cf830c4cae1356f6065f4fea787c7786c7200754ba73e97",
        "https://hackage.haskell.org/package/lukko-0.1.1.3/lukko-0.1.1.3.tar.gz": "a80efb60cfa3dae18682c01980d76d5f7e413e191cd186992e1bf7388d48ab1f",
        "https://repo.msys2.org/distrib/x86_64/msys2-base-x86_64-20201109.tar.xz": "ca10a72aa3df219fabeff117aa4b00c1ec700ea93c4febf4cfc03083f4b2cacb",
    },
)

if __name__ == "__main__":
    main_driver(pkg)
