import tempfile
from pathlib import Path

from ...lib.test import BuildContext


def compile_cabal_wrapper(ctx: BuildContext, mingw_threads_win32_seh_version: str) -> None:
    gcc_prog = ctx.devpacks_pkg_prog(
        "mingw-threads-win32-seh", mingw_threads_win32_seh_version, "gcc"
    )

    # Use tempfile instead of ctx.tmp_dir because gcc doesn't support Windows
    # UNC paths
    with tempfile.TemporaryDirectory() as tmpdirname:
        with open(Path(tmpdirname) / "cabal.c", "w", encoding="utf-8") as f:
            f.write(get_cabal_wrapper_code())

        ctx.shell.run(
            gcc_prog,
            [
                str(Path(tmpdirname) / "cabal.c"),
                "-o",
                str(Path(tmpdirname) / "cabal.exe"),
                "-lshlwapi",
                "-municode",
            ],
        )

        ctx.shell.copy(Path(tmpdirname) / "cabal.exe", ctx.pkg_dir / "bin" / "cabal.exe")


# We need to use an actual exe file as the launcher instead of a .cmd script,
# because the Haskell tooling looks specifically for a "cabal.exe" file:
#
# haskell-language-server uses `findExecutable` to find cabal.
# <https://hackage.haskell.org/package/directory-1.3.6.1/docs/System-Directory.html#v:findExecutable>
def get_cabal_wrapper_code() -> str:
    return """
#include <stdio.h>
#include <windows.h>
#include <Shlwapi.h>

#define MAX_ENV_VAR 32767

int WINAPI wWinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int iCmdShow) 
{
    WCHAR exePath[MAX_PATH];
    if(!GetModuleFileNameW(NULL, exePath, MAX_PATH)) {
        fprintf(stderr, "GetModuleFileNameW failed (%d)\\n", GetLastError());
        return 1;
    }

    PathRemoveFileSpecW(exePath); // Remove filename
    PathRemoveFileSpecW(exePath); // Go up one directory

    WCHAR envPath[MAX_ENV_VAR];
    if (!GetEnvironmentVariableW(L"PATH", envPath, MAX_ENV_VAR)) {
        fprintf(stderr, "GetEnvironmentVariableW failed (%d)\\n", GetLastError());
        return 1;
    }

    WCHAR newEnvPath[MAX_ENV_VAR];
    wcscpy(newEnvPath, exePath);
    lstrcatW(newEnvPath, L"\\\\msys2\\\\usr\\\\bin;");
    lstrcatW(newEnvPath, envPath);
    if (!SetEnvironmentVariableW(L"PATH", newEnvPath)) {
        fprintf(stderr, "SetEnvironmentVariableW failed (%d)\\n", GetLastError());
        return 1;
    }

    lstrcatW(exePath, L"\\\\cabal.exe");

    STARTUPINFOW info;
    PROCESS_INFORMATION processInfo;

    ZeroMemory(&info, sizeof(info));
    info.cb = sizeof(info);
    ZeroMemory(&processInfo, sizeof(processInfo));

    if (!CreateProcessW(exePath, GetCommandLineW(), NULL, NULL, TRUE, 0, NULL, NULL, &info, &processInfo)) {
        fprintf(stderr, "CreateProcessW failed (%d)\\n", GetLastError());
        return 1;
    }

    WaitForSingleObject(processInfo.hProcess, INFINITE);

    DWORD exit_code;
    if (!GetExitCodeProcess(processInfo.hProcess, &exit_code)) {
        fprintf(stderr, "GetExitCodeProcess failed (%d)\\n", GetLastError());
        return 1;
    }

    CloseHandle(processInfo.hProcess);
    CloseHandle(processInfo.hThread);

    return exit_code;
}
        """
