import glob
import os.path
from collections import OrderedDict
from pathlib import Path
from typing import Dict, NamedTuple, Type

from ...lib.main_driver import main_driver
from ...lib.test import BuildContext, PackageDescr, PackageTestCase


class BuildConfig(NamedTuple):
    gcc_version: str
    make_version: str
    gmp_version: str
    patchelf_version: str


build_configs: Dict[str, BuildConfig] = OrderedDict(
    [
        (
            "4.0.2",
            BuildConfig(
                gcc_version="4.9.4",
                make_version="4.1",
                gmp_version="6.1.2",
                patchelf_version="0.12",
            ),
        ),
        (
            "4.1.0",
            BuildConfig(
                gcc_version="4.9.4",
                make_version="4.1",
                gmp_version="6.1.2",
                patchelf_version="0.12",
            ),
        ),
    ]
)


def make_pkg(ctx: BuildContext, pkg_version: str) -> None:
    build_config = build_configs[pkg_version]

    gcc_dir = ctx.devpacks_pkg("gcc", build_config.gcc_version)
    make_dir = ctx.devpacks_pkg("make", build_config.make_version)
    gmp_dir = ctx.devpacks_pkg("gmp", build_config.gmp_version)
    patchelf_prog = ctx.devpacks_pkg_prog("patchelf", build_config.patchelf_version, "patchelf")

    url = f"https://www.mpfr.org/mpfr-{pkg_version}/mpfr-{pkg_version}.tar.gz"

    ctx.shell.mkdir(ctx.tmp_dir / "static")
    ctx.shell.mkdir(ctx.tmp_dir / "dynamic")
    ctx.download_extract(url, ctx.tmp_dir / "static")
    ctx.download_extract(url, ctx.tmp_dir / "dynamic")

    common_configure_flags = [
        "--build=x86_64-linux-gnu",
        "--enable-decimal-float",
        "--enable-float128",
    ]

    with ctx.shell.cd(ctx.tmp_dir / "static"):
        ctx.shell.env["PATH"] = os.path.pathsep.join(
            [str(Path(gcc_dir) / "bin"), str(Path(make_dir) / "bin"), ctx.shell.env["PATH"],]
        )

        ctx.shell.env["CFLAGS"] = "-fPIC -O3"
        ctx.shell.env["CPPFLAGS"] = f'-I{gmp_dir / "include"}'
        ctx.shell.env["LDFLAGS"] = f'-L{gmp_dir / "lib"}'

        ctx.shell.run(
            "./configure",
            [
                "--prefix=" + str(ctx.tmp_dir / "static" / "local"),
                "--enable-static",
                "--disable-shared",
            ]
            + common_configure_flags,
        )

        ctx.shell.run("make", [])
        ctx.shell.run("make", ["install"])
        ctx.shell.run("make", ["check"])

        ctx.shell.rename("local/include", ctx.pkg_dir / "include")
        ctx.shell.rename("local/lib", ctx.pkg_dir / "lib")
        ctx.shell.rename("local/share", ctx.pkg_dir / "share")

    with ctx.shell.cd(ctx.tmp_dir / "dynamic"):
        ctx.shell.env["PATH"] = os.path.pathsep.join(
            [str(Path(gcc_dir) / "bin"), str(Path(make_dir) / "bin"), ctx.shell.env["PATH"],]
        )

        ctx.shell.env["CFLAGS"] = "-fPIC -O3"
        ctx.shell.env["CPPFLAGS"] = f'-I{gmp_dir / "include"}'
        ctx.shell.env["LDFLAGS"] = f'-L{gmp_dir / "lib-dynamic"}'

        ctx.shell.run(
            "./configure",
            [
                "--prefix=" + str(ctx.tmp_dir / "dynamic" / "local"),
                "--enable-shared",
                "--disable-static",
            ]
            + common_configure_flags,
        )

        ctx.shell.run("make", [])
        ctx.shell.run("make", ["install"])

        ctx.shell.mkdir(ctx.pkg_dir / "lib-dynamic")
        for so_file in glob.glob(str(ctx.tmp_dir / "dynamic" / "local" / "lib" / "*.so*")):
            if not os.path.islink(so_file):
                ctx.shell.chmod(so_file, 0o755)
                ctx.shell.run(patchelf_prog, ["--set-rpath", "$ORIGIN", so_file])
            ctx.shell.rename(so_file, ctx.pkg_dir / "lib-dynamic" / os.path.basename(so_file))

        ctx.shell.copy(
            gmp_dir / "lib-dynamic" / "libgmp.so.10", ctx.pkg_dir / "lib-dynamic" / "libgmp.so.10"
        )


def create_tests(pkg_dir: Path, pkg_version: str) -> Type[PackageTestCase]:
    class TestCase(PackageTestCase):
        def test_mpfr_h(self) -> None:
            self.assertTrue(os.path.isfile(pkg_dir / "include" / "mpfr.h"))

        def test_libmpfr(self) -> None:
            self.assertTrue(os.path.isfile(pkg_dir / "lib" / "libmpfr.a"))

        def test_libz_dynamic(self) -> None:
            self.assertTrue(os.path.isfile(pkg_dir / "lib-dynamic" / "libmpfr.so"))

    return TestCase


pkg = PackageDescr(
    name="mpfr",
    versions=list(build_configs.keys()),
    make_pkg=make_pkg,
    create_tests=create_tests,
    sha256s={
        "https://www.mpfr.org/mpfr-4.0.2/mpfr-4.0.2.tar.gz": "ae26cace63a498f07047a784cd3b0e4d010b44d2b193bab82af693de57a19a78",
        "https://www.mpfr.org/mpfr-4.1.0/mpfr-4.1.0.tar.gz": "3127fe813218f3a1f0adf4e8899de23df33b4cf4b4b3831a5314f78e65ffa2d6",
    },
)

if __name__ == "__main__":
    main_driver(pkg)
