# NOTE: To build this package on Ubuntu, you will need the following packages:
#
#     $ sudo apt-get install build-essential mercurial make cmake autoconf automake libtool libasound2-dev libpulse-dev libaudio-dev libx11-dev libxext-dev libxrandr-dev libxcursor-dev libxi-dev libxinerama-dev libxxf86vm-dev libxss-dev libgl1-mesa-dev libesd0-dev libdbus-1-dev libudev-dev libgles1-mesa-dev libgles2-mesa-dev libegl1-mesa-dev libibus-1.0-dev fcitx-libs-dev libsamplerate0-dev libsndio-dev
#
# Note: Ubuntu 17.10 doesn't seem to have `libgles1-mesa-dev`, so you might have to omit it. <https://askubuntu.com/questions/924903/unable-to-locate-package-libgles1-mesa-dev>
#
# Ubuntu 16.04 or later should add:
#
#     $ sudo apt-get install libwayland-dev libxkbcommon-dev wayland-protocols
#
# Ubuntu 16.10 or later should also add:
#
#     $ sudo apt-get install libmirclient-dev libxkbcommon-dev
#
# See: <https://hg.libsdl.org/SDL/file/2088cd828335/docs/README-linux.md>

import os
import os.path
import subprocess

import pkglib


class Package(pkglib.PackageBuilder):
    name = "libsdl2"
    version = "2.0.7"

    sha256s = {
        "https://www.libsdl.org/release/SDL2-2.0.7.tar.gz": "ee35c74c4313e2eda104b14b1b86f7db84a04eeab9430d56e001cea268bf4d5e"
    }

    def make_pkg(self):
        url = f"https://www.libsdl.org/release/SDL2-{self.version}.tar.gz"

        self.download_root(url)

        subprocess.run(
            [
                "./configure",
                "--prefix=" + os.path.join(self.pkg_dir, "local"),
                "--disable-shared",
                "--enable-static",
                "CFLAGS=-fPIC",
            ],
            cwd=self.pkg_dir,
            check=True,
        )
        subprocess.run(["make"], cwd=self.pkg_dir, check=True)
        subprocess.run(["make", "install"], cwd=self.pkg_dir, check=True)

        # TODO Patch and install `local/bin/sdl2-config`

        os.rename(
            os.path.join(self.pkg_dir, "include"), os.path.join(self.pkg_dir, "_include"),
        )
        os.rename(
            os.path.join(self.pkg_dir, "local", "include"), os.path.join(self.pkg_dir, "include"),
        )
        os.rename(
            os.path.join(self.pkg_dir, "local", "lib"), os.path.join(self.pkg_dir, "lib"),
        )

    def create_tests(self, pkg_dir):
        class PackageTestCase(pkglib.PackageTestCase):
            def test_sdl_h(self):
                exists = os.path.isfile(os.path.join(pkg_dir, "include", "SDL2", "SDL.h"))
                self.assertTrue(exists, "SDL.h header file exists")

            def test_libsdl2_a(self):
                exists = os.path.isfile(os.path.join(pkg_dir, "lib", "libSDL2.a"))
                self.assertTrue(exists, "libSDL2.a lib file exists")

        return PackageTestCase


if __name__ == "__main__":
    Package().run()
