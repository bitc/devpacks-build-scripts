import distutils.version
import os

import pkglib


class Package(pkglib.PackageBuilder):
    name = "cmake"
    version = "3.13.2"

    cmake_major_minor = ".".join(
        [str(d) for d in distutils.version.LooseVersion(version).version[0:2]]
    )

    sha256s = {
        "https://cmake.org/files/v3.10/cmake-3.10.1-Darwin-x86_64.tar.gz": "8684f9e084fe4a5565dd2b9538ba843832acb207955e71047a686219f928fb3d",
        "https://cmake.org/files/v3.10/cmake-3.10.1-Linux-x86_64.tar.gz": "f0f84761a254324ed9076c23fca502eb135ec49c0b752212a6298f317d303438",
        "https://cmake.org/files/v3.10/cmake-3.10.1-win64-x64.zip": "8251f70c85b58f3ca1f24e4a3b0637e2d609b5e4a341d00b70e02e89244d5029",
        "https://cmake.org/files/v3.13/cmake-3.13.2-Linux-x86_64.tar.gz": "6370de82999baafc2dbbf0eda23007d93f78d0c3afda8434a646518915ca0846",
    }

    def make_pkg(self):

        url = {
            (
                "darwin",
                "x86_64",
            ): f"https://cmake.org/files/v{Package.cmake_major_minor}/cmake-{self.version}-Darwin-x86_64.tar.gz",
            (
                "linux",
                "x86_64",
            ): f"https://cmake.org/files/v{Package.cmake_major_minor}/cmake-{self.version}-Linux-x86_64.tar.gz",
            (
                "windows",
                "x86_64",
            ): f"https://cmake.org/files/v{Package.cmake_major_minor}/cmake-{self.version}-win64-x64.zip",
        }[self.os, self.arch]

        self.download_root(url)

        if self.os == "darwin":
            # Move everything from CMake.app/Contents dir into root
            for f in os.listdir(os.path.join(self.pkg_dir, "CMake.app", "Contents")):
                os.rename(
                    os.path.join(self.pkg_dir, "CMake.app", "Contents", f),
                    os.path.join(self.pkg_dir, f),
                )
            os.rmdir(os.path.join(self.pkg_dir, "CMake.app", "Contents"))
            os.rmdir(os.path.join(self.pkg_dir, "CMake.app"))

    def create_tests(self, pkg_dir):
        class PackageTestCase(pkglib.PackageTestCase):
            def test_cmake(self):
                self.assert_run_program_first_line(
                    pkg_dir, "cmake", ["--version"], f"cmake version {Package.version}"
                )

        return PackageTestCase


if __name__ == "__main__":
    Package().run()
