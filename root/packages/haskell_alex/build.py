from collections import OrderedDict
from pathlib import Path
from typing import Dict, Type

from ...lib.ecosystems.haskell.hackage import HackageBuildConfig, make_hackage_pkg
from ...lib.main_driver import main_driver
from ...lib.test import BuildContext, PackageDescr, PackageTestCase

build_configs: Dict[str, HackageBuildConfig] = OrderedDict(
    [("3.2.5", HackageBuildConfig(ghc_version="8.10.3", cabal_install_version="3.4.0.0"),)]
)


def make_pkg(ctx: BuildContext, pkg_version: str) -> None:
    build_config = build_configs[pkg_version]
    make_hackage_pkg(ctx, "alex", pkg_version, build_config)


def create_tests(pkg_dir: Path, pkg_version: str) -> Type[PackageTestCase]:
    class TestCase(PackageTestCase):
        def test_alex(self) -> None:
            out = self.run_program(pkg_dir, "alex", ["--version"])
            out_version = " ".join(out.split("\n")[0].split()[0:3])
            self.assertEqual(out_version, f"Alex version {pkg_version},")

    return TestCase


pkg = PackageDescr(
    name="haskell_alex",
    versions=list(build_configs.keys()),
    make_pkg=make_pkg,
    create_tests=create_tests,
    sha256s={
        "https://hackage.haskell.org/package/alex-3.2.5/alex-3.2.5.tar.gz": "b77c8a1270767c64e2adb21a6e91ee7cd904ba17edae17bc20fd03da5256e0e3",
    },
)

if __name__ == "__main__":
    main_driver(pkg)
