import os.path
from collections import OrderedDict
from pathlib import Path
from typing import Dict, NamedTuple, Type

from ...lib.main_driver import main_driver
from ...lib.test import BuildContext, PackageDescr, PackageTestCase


class BuildConfig(NamedTuple):
    make_version: str
    gcc_version: str


build_configs: Dict[str, BuildConfig] = OrderedDict(
    [("6.0", BuildConfig(make_version="4.1", gcc_version="7.5.0"),)]
)


def make_pkg(ctx: BuildContext, pkg_version: str) -> None:
    build_config = build_configs[pkg_version]

    make_dir = ctx.devpacks_pkg("make", build_config.make_version)
    gcc_dir = ctx.devpacks_pkg("gcc", build_config.gcc_version)

    if pkg_version == "6.0":
        url = "ftp://ftp.info-zip.org/pub/infozip/src/unzip60.tgz"
    else:
        raise RuntimeError("Unknown pkg_version", pkg_version)

    ctx.download_root(url)

    ctx.shell.env["PATH"] = os.path.pathsep.join(
        [str(Path(gcc_dir) / "bin"), str(Path(make_dir) / "bin"), ctx.shell.env["PATH"],]
    )

    ctx.shell.run("make", ["-f", "unix/Makefile", "generic_gcc"])
    ctx.shell.run(
        "make", ["prefix=" + str(ctx.pkg_dir / "local"), "-f", "unix/Makefile", "install",],
    )

    ctx.shell.rename("local/bin", "bin")


def create_tests(pkg_dir: Path, pkg_version: str) -> Type[PackageTestCase]:
    class TestCase(PackageTestCase):
        def test_unzip_exists(self) -> None:
            exists = os.path.isfile(pkg_dir / "bin" / "unzip")
            self.assertTrue(exists, "unzip header file exists")

        # TODO Broken because "unzip --version" writes to stderr (instead of
        # stdout) and returns exit code 10

        # def test_unzip(self) -> None:
        #     self.assert_run_program_stdout_substring(
        #         pkg_dir, "unzip", ["--version"], f"UnZip {pkg_version}"
        #     )

    return TestCase


pkg = PackageDescr(
    name="unzip",
    versions=list(build_configs.keys()),
    make_pkg=make_pkg,
    create_tests=create_tests,
    sha256s={
        "ftp://ftp.info-zip.org/pub/infozip/src/unzip60.tgz": "036d96991646d0449ed0aa952e4fbe21b476ce994abc276e49d30e686708bd37"
    },
)

if __name__ == "__main__":
    main_driver(pkg)
