import glob
import os
import os.path
import subprocess
from collections import OrderedDict
from pathlib import Path
from typing import Dict, NamedTuple, Type

from ...lib.main_driver import main_driver
from ...lib.test import BuildContext, PackageDescr, PackageTestCase


class BuildConfig(NamedTuple):
    gcc_version: str
    make_version: str


build_configs: Dict[str, BuildConfig] = OrderedDict(
    [("1.2.11", BuildConfig(gcc_version="7.5.0", make_version="4.1")),]
)


def make_pkg(ctx: BuildContext, pkg_version: str) -> None:
    build_config = build_configs[pkg_version]

    gcc_dir = ctx.devpacks_pkg("gcc", build_config.gcc_version)
    make_dir = ctx.devpacks_pkg("make", build_config.make_version)

    url = f"http://www.zlib.net/zlib-{pkg_version}.tar.xz"

    ctx.download_extract(url, ctx.tmp_dir)

    with ctx.shell.cd(ctx.tmp_dir):
        ctx.shell.env["PATH"] = os.path.pathsep.join(
            [str(Path(gcc_dir) / "bin"), str(Path(make_dir) / "bin"), ctx.shell.env["PATH"],]
        )

        ctx.shell.env["CFLAGS"] = "-fPIC " + ctx.shell.env.get("CFLAGS", "")

        ctx.shell.run(
            "./configure", ["--prefix=" + str(ctx.tmp_dir / "local"),],
        )

        ctx.shell.run("make", [])
        ctx.shell.run("make", ["install"])

        ctx.shell.rename("local/include", ctx.pkg_dir / "include")
        ctx.shell.rename("local/lib", ctx.pkg_dir / "lib")
        ctx.shell.rename("local/share", ctx.pkg_dir / "share")

    ctx.shell.mkdir(ctx.pkg_dir / "lib-dynamic")
    for so_file in glob.glob(str(ctx.pkg_dir / "lib" / "*.so*")):
        ctx.shell.rename(so_file, ctx.pkg_dir / "lib-dynamic" / os.path.basename(so_file))


def create_tests(pkg_dir: Path, pkg_version: str) -> Type[PackageTestCase]:
    class TestCase(PackageTestCase):
        def test_zlib_h(self) -> None:
            self.assertTrue(os.path.isfile(pkg_dir / "include" / "zlib.h"))

        def test_libz(self) -> None:
            self.assertTrue(os.path.isfile(pkg_dir / "lib" / "libz.a"))

        def test_libz_dynamic(self) -> None:
            self.assertTrue(os.path.isfile(pkg_dir / "lib-dynamic" / "libz.so"))

    return TestCase


pkg = PackageDescr(
    name="zlib",
    versions=list(build_configs.keys()),
    make_pkg=make_pkg,
    create_tests=create_tests,
    sha256s={
        "http://www.zlib.net/zlib-1.2.11.tar.xz": "4ff941449631ace0d4d203e3483be9dbc9da454084111f97ea0a2114e19bf066"
    },
)

if __name__ == "__main__":
    main_driver(pkg)
