import os
import os.path
import subprocess

import pkglib


class Package(pkglib.PackageBuilder):
    name = "libpcre"
    version = "8.42"

    sha256s = {
        "https://ftp.pcre.org/pub/pcre/pcre-8.42.tar.gz": "69acbc2fbdefb955d42a4c606dfde800c2885711d2979e356c0636efde9ec3b5"
    }

    def make_pkg(self):
        url = f"https://ftp.pcre.org/pub/pcre/pcre-{self.version}.tar.gz"

        self.download_root(url)

        subprocess.run(
            [
                "./configure",
                "--prefix=" + os.path.join(self.pkg_dir, "local"),
                "--host=x86_64-linux-gnu",
                "--build=x86_64-linux-gnu",
                "--disable-shared",
                "--enable-utf",
                "--enable-unicode-properties",
                "--enable-jit",
                "--enable-pcre16",
                "--enable-pcre32",
                "CFLAGS=-fPIC -O2",
                "CXXFLAGS=-fPIC -O2",
            ],
            cwd=self.pkg_dir,
            check=True,
        )
        subprocess.run(["make"], cwd=self.pkg_dir, check=True)
        subprocess.run(["make", "install"], cwd=self.pkg_dir, check=True)
        subprocess.run(["make", "check"], cwd=self.pkg_dir, check=True)

        os.rename(
            os.path.join(self.pkg_dir, "local", "include"), os.path.join(self.pkg_dir, "include"),
        )
        os.rename(
            os.path.join(self.pkg_dir, "local", "lib"), os.path.join(self.pkg_dir, "lib"),
        )

    def create_tests(self, pkg_dir):
        class PackageTestCase(pkglib.PackageTestCase):
            def test_pcre_h(self):
                exists = os.path.isfile(os.path.join(pkg_dir, "include", "pcre.h"))
                self.assertTrue(exists, "pcre.h header file exists")

            def test_libpcre_a(self):
                exists = os.path.isfile(os.path.join(pkg_dir, "lib", "libpcre.a"))
                self.assertTrue(exists, "libpcre.a lib file exists")

        return PackageTestCase


if __name__ == "__main__":
    Package().run()
