import distutils.version
import os.path
from collections import OrderedDict
from pathlib import Path
from typing import Dict, NamedTuple, Type

from ...lib.main_driver import main_driver
from ...lib.test import BuildContext, PackageDescr, PackageTestCase


class BuildConfig(NamedTuple):
    gcc_version: str
    make_version: str


build_configs: Dict[str, BuildConfig] = OrderedDict(
    [
        ("0.9", BuildConfig(gcc_version="7.5.0", make_version="4.1")),
        ("0.10", BuildConfig(gcc_version="7.5.0", make_version="4.1")),
        ("0.11", BuildConfig(gcc_version="7.5.0", make_version="4.1")),
        ("0.12", BuildConfig(gcc_version="7.5.0", make_version="4.1")),
    ]
)


def make_pkg(ctx: BuildContext, pkg_version: str) -> None:
    build_config = build_configs[pkg_version]

    gcc_dir = ctx.devpacks_pkg("gcc", build_config.gcc_version)
    make_dir = ctx.devpacks_pkg("make", build_config.make_version)

    if distutils.version.LooseVersion(pkg_version) < distutils.version.LooseVersion("0.12"):
        url = f"https://nixos.org/releases/patchelf/patchelf-{pkg_version}/patchelf-{pkg_version}.tar.gz"
    else:
        url = f"https://github.com/NixOS/patchelf/releases/download/{pkg_version}/patchelf-{pkg_version}.tar.bz2"

    ctx.download_root(url)

    ctx.shell.env["PATH"] = os.path.pathsep.join(
        [str(Path(gcc_dir) / "bin"), str(Path(make_dir) / "bin"), ctx.shell.env["PATH"],]
    )

    ctx.shell.env["LDFLAGS"] = "-static-libstdc++"
    ctx.shell.run("./configure", ["--prefix=" + str(ctx.pkg_dir / "local")])
    ctx.shell.run("make", [])
    ctx.shell.run("make", ["install"])
    ctx.shell.run("strip", ["local/bin/patchelf"])

    ctx.shell.rename("local/bin", "bin")
    ctx.shell.rename("local/share", "share")


def create_tests(pkg_dir: Path, pkg_version: str) -> Type[PackageTestCase]:
    class TestCase(PackageTestCase):
        def test_patchelf(self):
            self.assert_run_program_stdout_substring(
                pkg_dir, "patchelf", ["--version"], f"patchelf {pkg_version}"
            )

    return TestCase


pkg = PackageDescr(
    name="patchelf",
    versions=list(build_configs.keys()),
    make_pkg=make_pkg,
    create_tests=create_tests,
    sha256s={
        "https://nixos.org/releases/patchelf/patchelf-0.9/patchelf-0.9.tar.gz": "f2aa40a6148cb3b0ca807a1bf836b081793e55ec9e5540a5356d800132be7e0a",
        "https://nixos.org/releases/patchelf/patchelf-0.10/patchelf-0.10.tar.gz": "b2deabce05c34ce98558c0efb965f209de592197b2c88e930298d740ead09019",
        "https://nixos.org/releases/patchelf/patchelf-0.11/patchelf-0.11.tar.gz": "e52378cc2f9379c6e84a04ac100a3589145533a7b0cd26ef23c79dfd8a9038f9",
        "https://github.com/NixOS/patchelf/releases/download/0.12/patchelf-0.12.tar.bz2": "699a31cf52211cf5ad6e35a8801eb637bc7f3c43117140426400d67b7babd792",
    },
)

if __name__ == "__main__":
    main_driver(pkg)
