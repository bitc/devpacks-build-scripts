import os
import os.path
import shutil
import subprocess

import pkglib


class Package(pkglib.PackageBuilder):
    name = "sassc"
    version = "3.4.8"

    sassc_version = "3.4.8"

    sha256s = {}

    libsass_git_hashes = {"3.4.8": "a1f13edf19f90f0723e02a736a33e0fc03c6c584"}

    sassc_git_hashes = {"3.4.8": "aa6d5c635ea8faf44d542a23aaf85d27e5777d48"}

    def make_pkg(self):
        with pkglib.TmpDir() as tmp_dir:
            subprocess.run(
                [
                    "git",
                    "clone",
                    "--branch",
                    self.version,
                    "--depth",
                    "1",
                    "https://github.com/sass/libsass.git",
                ],
                cwd=tmp_dir,
                check=True,
            )
            subprocess.run(
                ["git", "checkout", Package.libsass_git_hashes[self.version]],
                cwd=os.path.join(tmp_dir, "libsass"),
                check=True,
            )

            subprocess.run(
                [
                    "git",
                    "clone",
                    "--branch",
                    self.sassc_version,
                    "--depth",
                    "1",
                    "https://github.com/sass/sassc.git",
                ],
                cwd=tmp_dir,
                check=True,
            )
            subprocess.run(
                ["git", "checkout", Package.sassc_git_hashes[self.sassc_version]],
                cwd=os.path.join(tmp_dir, "sassc"),
                check=True,
            )

            subprocess.run(["make", "-C", "libsass"], cwd=tmp_dir, check=True)

            env = dict(os.environ)
            env["SASS_LIBSASS_PATH"] = os.path.join(tmp_dir, "libsass")
            subprocess.run(["make", "-C", "sassc"], cwd=tmp_dir, env=env, check=True)

            subprocess.run(
                ["strip", os.path.join("sassc", "bin", "sassc")], cwd=tmp_dir, check=True,
            )

            os.mkdir(os.path.join(self.pkg_dir, "bin"))
            shutil.copy(
                os.path.join(tmp_dir, "sassc", "bin", "sassc"), os.path.join(self.pkg_dir, "bin"),
            )

    def create_tests(self, pkg_dir):
        class PackageTestCase(pkglib.PackageTestCase):
            def test_sassc(self):
                out = self.run_program(pkg_dir, "sassc", ["--version"])
                self.assertListEqual(
                    out.split("\n")[0:2],
                    [f"sassc: {Package.sassc_version}", f"libsass: {Package.version}"],
                )

        return PackageTestCase


if __name__ == "__main__":
    Package().run()
