# NOTE: To build this package on Ubuntu 16.04, you will need the following packages:
#
#     $ sudo apt-get install build-essential pkg-config zlib1g-dev libexpat1-dev libx11-xcb-dev libxcb-dri2-0-dev libxcb-xfixes0-dev libxext-dev x11proto-gl-dev

import os
import os.path
import shutil
import subprocess

import pkglib


class Package(pkglib.PackageBuilder):
    name = "mesa"
    version = "17.3.3"

    python_version = "2.7.14"

    sha256s = {
        "https://mesa.freedesktop.org/archive/mesa-17.3.3.tar.xz": "41bac5de0ef6adc1f41a1ec0f80c19e361298ce02fa81b5f9ba4fdca33a9379b"
    }

    def make_pkg(self):
        python_dir = subprocess.run(
            ["devpacks", "quick-pkg-path", "python", "-t", Package.python_version],
            encoding="utf-8",
            check=True,
            stdout=subprocess.PIPE,
        ).stdout

        url = f"https://mesa.freedesktop.org/archive/mesa-{self.version}.tar.xz"

        self.download_root(url)

        env = dict(os.environ)
        env["PATH"] = os.path.join(python_dir, "bin") + os.pathsep + env["PATH"]

        configure_flags = [
            "--enable-shared",
            "--disable-static",
            "--disable-dri",
            "--disable-dri3",
            "--enable-glx",
            "--disable-egl",
            "--disable-xvmc",
            "--disable-vdpau",
            "--disable-omx-bellagio",
            "--disable-va",
            "--disable-gbm",
            "--disable-driglx-direct",
            "--disable-glx-tls",
            "--disable-llvm",
            "--disable-valgrind",
            "--with-gallium-drivers=no",
            "--with-platforms=x11",
            "--with-dri-drivers=no",
        ]

        subprocess.run(
            ["./configure", "--prefix=" + os.path.join(self.pkg_dir, "local")] + configure_flags,
            cwd=self.pkg_dir,
            check=True,
            env=env,
        )
        subprocess.run(["make"], cwd=self.pkg_dir, check=True, env=env)
        subprocess.run(["make", "install"], cwd=self.pkg_dir, check=True, env=env)

        shutil.rmtree(os.path.join(self.pkg_dir, "include"))
        shutil.rmtree(os.path.join(self.pkg_dir, "lib"))
        shutil.rmtree(os.path.join(self.pkg_dir, "src"))  # This is huge! (several hundred MB)
        os.rename(
            os.path.join(self.pkg_dir, "local", "include"), os.path.join(self.pkg_dir, "include"),
        )
        os.rename(
            os.path.join(self.pkg_dir, "local", "lib"), os.path.join(self.pkg_dir, "lib"),
        )

    def create_tests(self, pkg_dir):
        class PackageTestCase(pkglib.PackageTestCase):
            def test_gl_h(self):
                exists = os.path.isfile(os.path.join(pkg_dir, "include", "GL", "gl.h"))
                self.assertTrue(exists, "gl.h header file exists")

            def test_libgl_la(self):
                exists = os.path.isfile(os.path.join(pkg_dir, "lib", "libGL.la"))
                self.assertTrue(exists, "libGL.la lib file exists")

            def test_libgl_so(self):
                exists = os.path.isfile(os.path.join(pkg_dir, "lib", "libGL.so"))
                self.assertTrue(exists, "libGL.so lib file exists")

        return PackageTestCase


if __name__ == "__main__":
    Package().run()
