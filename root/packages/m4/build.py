import os.path
import unittest
from collections import OrderedDict
from pathlib import Path
from typing import Dict, NamedTuple, Type

from ...lib.main_driver import main_driver
from ...lib.test import BuildContext, PackageDescr, PackageTestCase


class BuildConfig(NamedTuple):
    gcc_version: str
    make_version: str


build_configs: Dict[str, BuildConfig] = OrderedDict(
    [("1.4.18", BuildConfig(gcc_version="7.5.0", make_version="4.1")),]
)


def make_pkg(ctx: BuildContext, pkg_version: str) -> None:
    build_config = build_configs[pkg_version]

    gcc_dir = ctx.devpacks_pkg("gcc", build_config.gcc_version)
    make_dir = ctx.devpacks_pkg("make", build_config.make_version)

    url = f"http://ftp.gnu.org/gnu/m4/m4-{pkg_version}.tar.xz"

    ctx.download_extract(url, ctx.tmp_dir)

    with ctx.shell.cd(ctx.tmp_dir):
        ctx.shell.env["PATH"] = os.path.pathsep.join(
            [str(Path(gcc_dir) / "bin"), str(Path(make_dir) / "bin"), ctx.shell.env["PATH"],]
        )

        ctx.shell.run("./configure", ["--prefix=" + str(ctx.tmp_dir / "local")])

        ctx.shell.run("make", [])
        ctx.shell.run("make", ["install"])

        ctx.shell.rename("local/bin", ctx.pkg_dir / "bin")
        ctx.shell.rename("local/share", ctx.pkg_dir / "share")


def create_tests(pkg_dir: Path, pkg_version: str) -> Type[PackageTestCase]:
    class TestCase(PackageTestCase):
        def test_m4(self) -> None:
            self.assert_run_program_first_line(
                pkg_dir, "m4", ["--version"], f"m4 (GNU M4) {pkg_version}"
            )

    return TestCase


pkg = PackageDescr(
    name="m4",
    versions=list(build_configs.keys()),
    make_pkg=make_pkg,
    create_tests=create_tests,
    sha256s={
        "http://ftp.gnu.org/gnu/m4/m4-1.4.18.tar.xz": "f2c1e86ca0a404ff281631bdc8377638992744b175afb806e25871a24a934e07"
    },
)

if __name__ == "__main__":
    main_driver(pkg)
