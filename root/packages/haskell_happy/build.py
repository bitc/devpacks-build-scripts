from collections import OrderedDict
from pathlib import Path
from typing import Dict, Type

from ...lib.ecosystems.haskell.hackage import HackageBuildConfig, make_hackage_pkg
from ...lib.main_driver import main_driver
from ...lib.test import BuildContext, PackageDescr, PackageTestCase

build_configs: Dict[str, HackageBuildConfig] = OrderedDict(
    [("1.19.12", HackageBuildConfig(ghc_version="8.10.3", cabal_install_version="3.4.0.0"),)]
)


def make_pkg(ctx: BuildContext, pkg_version: str) -> None:
    build_config = build_configs[pkg_version]
    make_hackage_pkg(ctx, "happy", pkg_version, build_config)


def create_tests(pkg_dir: Path, pkg_version: str) -> Type[PackageTestCase]:
    class TestCase(PackageTestCase):
        def test_happy(self) -> None:
            out = self.run_program(pkg_dir, "happy", ["--version"])
            out_version = " ".join(out.split("\n")[0].split()[0:3])
            self.assertEqual(out_version, f"Happy Version {pkg_version}")

    return TestCase


pkg = PackageDescr(
    name="haskell_happy",
    versions=list(build_configs.keys()),
    make_pkg=make_pkg,
    create_tests=create_tests,
    sha256s={
        "https://hackage.haskell.org/package/happy-1.19.12/happy-1.19.12.tar.gz": "fb9a23e41401711a3b288f93cf0a66db9f97da1ce32ec4fffea4b78a0daeb40f",
    },
)

if __name__ == "__main__":
    main_driver(pkg)
