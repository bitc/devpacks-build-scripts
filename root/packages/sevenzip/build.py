import textwrap
from collections import OrderedDict
from pathlib import Path
from typing import Dict, NamedTuple, Type

from ...lib.main_driver import main_driver
from ...lib.test import BuildContext, PackageDescr, PackageTestCase


class BuildConfig(NamedTuple):
    sevenzip_version: str


build_configs: Dict[str, BuildConfig] = OrderedDict(
    [("18.01", BuildConfig(sevenzip_version="18.01")),],
)


def make_pkg(ctx: BuildContext, pkg_version: str) -> None:
    build_config = build_configs[pkg_version]

    version_condensed = pkg_version.replace(".", "")
    url = {("windows", "x86_64"): f"http://d.7-zip.org/a/7z{version_condensed}-x64.exe"}[
        ctx.os, ctx.arch
    ]

    sevenzip_prog = ctx.devpacks_pkg_prog("sevenzip", build_config.sevenzip_version, "7z")

    if ctx.os == "windows":
        ctx.download_file(url, ctx.pkg_dir / "7-zip-installer.exe")
        ctx.shell.run(
            sevenzip_prog, ["x", "-o" + str(ctx.pkg_dir), str(ctx.pkg_dir / "7-zip-installer.exe")]
        )
        ctx.shell.remove(ctx.pkg_dir / "7-zip-installer.exe")
        ctx.shell.mkdir(ctx.pkg_dir / "bin")

        # Create wrapper script for running programs
        with open(ctx.pkg_dir / "bin" / "7z.cmd", "w", encoding="utf-8") as f:
            f.write(
                textwrap.dedent(
                    f"""\
                    @ECHO OFF
                    SETLOCAL
                    SET "PROG_EXE=%~dp0..\\7z"
                    "%PROG_EXE%" %*
                    """
                )
            )
    else:
        raise RuntimeError(f'TODO: os "{ctx.os}"')


def create_tests(pkg_dir: Path, pkg_version: str) -> Type[PackageTestCase]:
    class TestCase(PackageTestCase):
        def test_7z(self):
            out = self.run_program(pkg_dir, "7z", [])
            out_version = " ".join(out.split("\n")[0].split()[0:2])
            self.assertEqual(out_version, f"7-Zip {pkg_version}")

    return TestCase


pkg = PackageDescr(
    name="sevenzip",
    versions=list(build_configs.keys()),
    make_pkg=make_pkg,
    create_tests=create_tests,
    sha256s={
        "http://d.7-zip.org/a/7z1801-x64.exe": "86670d63429281a4a65c36919ca0f3099e3f803e3096c3a9722d61b3d31e4a9f",
    },
)

if __name__ == "__main__":
    main_driver(pkg)
