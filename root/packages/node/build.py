import os
import platform
import textwrap
from collections import OrderedDict
from pathlib import Path
from typing import Dict, NamedTuple, Type

from ...lib.main_driver import main_driver
from ...lib.test import BuildContext, PackageDescr, PackageTestCase


class BuildConfig(NamedTuple):
    npm_version: str


build_configs: Dict[str, BuildConfig] = OrderedDict(
    [
        ("8.12.0", BuildConfig(npm_version="6.4.1")),
        ("8.17.0", BuildConfig(npm_version="6.13.4")),
        ("10.24.0", BuildConfig(npm_version="6.14.11")),
        ("12.19.0", BuildConfig(npm_version="6.14.8")),
        ("12.21.0", BuildConfig(npm_version="6.14.11")),
        ("14.16.0", BuildConfig(npm_version="6.14.11")),
    ]
)


def make_pkg(ctx: BuildContext, pkg_version: str) -> None:
    url = {
        (
            "darwin",
            "x86_64",
        ): f"https://nodejs.org/dist/v{pkg_version}/node-v{pkg_version}-darwin-x64.tar.xz",
        (
            "linux",
            "x86_64",
        ): f"https://nodejs.org/dist/v{pkg_version}/node-v{pkg_version}-linux-x64.tar.xz",
        (
            "windows",
            "x86_64",
        ): f"https://nodejs.org/dist/v{pkg_version}/node-v{pkg_version}-win-x64.zip",
    }[ctx.os, ctx.arch]

    ctx.download_root(url)

    if ctx.os == "windows":
        ctx.shell.mkdir(ctx.pkg_dir / "bin")

        # Create wrapper script for running the programs
        with open(ctx.pkg_dir / "bin" / "node.cmd", "w", encoding="utf-8") as f:
            f.write(
                textwrap.dedent(
                    f"""\
                    @ECHO OFF
                    SETLOCAL
                    SET "PROG_EXE=%~dp0..\\node"
                    "%PROG_EXE%" %*
                    """
                )
            )

        with open(ctx.pkg_dir / "bin" / "npm.cmd", "w", encoding="utf-8") as f:
            f.write(
                textwrap.dedent(
                    f"""\
                    @ECHO OFF
                    SETLOCAL
                    SET "PROG_EXE=%~dp0..\\npm"
                    SET "PATH=%~dp0..;%PATH%"
                    "%PROG_EXE%" %*
                    """
                )
            )

        with open(ctx.pkg_dir / "bin" / "npx.cmd", "w", encoding="utf-8") as f:
            f.write(
                textwrap.dedent(
                    f"""\
                    @ECHO OFF
                    SETLOCAL
                    SET "PROG_EXE=%~dp0..\\npx"
                    SET "PATH=%~dp0..;%PATH%"
                    "%PROG_EXE%" %*
                    """
                )
            )
    else:
        # We need to make wrapper scripts for npm and npx

        ctx.shell.rename(ctx.pkg_dir / "bin" / "npm", ctx.pkg_dir / "bin" / "npm.js")
        os.chmod(ctx.pkg_dir / "bin" / "npm.js", 0o644)

        with open(ctx.pkg_dir / "bin" / "npm", "w", encoding="utf-8") as f:
            f.write(
                textwrap.dedent(
                    """\
                    #!/bin/sh
                    NODE=`dirname "$0"`/node
                    NPM=`dirname "$0"`/npm.js
                    export PATH=`dirname "$0"`:$PATH
                    exec "$NODE" "$NPM" "$@"
                    """
                )
            )
        os.chmod(ctx.pkg_dir / "bin" / "npm", 0o775)

        ctx.shell.rename(ctx.pkg_dir / "bin" / "npx", ctx.pkg_dir / "bin" / "npx.js")
        os.chmod(ctx.pkg_dir / "bin" / "npx.js", 0o644)

        with open(ctx.pkg_dir / "bin" / "npx", "w", encoding="utf-8") as f:
            f.write(
                textwrap.dedent(
                    """\
                    #!/bin/sh
                    NODE=`dirname "$0"`/node
                    NPX=`dirname "$0"`/npx.js
                    export PATH=`dirname "$0"`:$PATH
                    exec "$NODE" "$NPX" "$@"
                    """
                )
            )
        os.chmod(ctx.pkg_dir / "bin" / "npx", 0o775)


def create_tests(pkg_dir: Path, pkg_version: str) -> Type[PackageTestCase]:
    build_config = build_configs[pkg_version]

    class TestCase(PackageTestCase):
        def test_node(self) -> None:
            if platform.system() == "Windows":
                os.environ["NODE_SKIP_PLATFORM_CHECK"] = "1"
            self.assert_run_program(pkg_dir, "node", ["--version"], f"v{pkg_version}")

        def test_npm(self) -> None:
            if platform.system() == "Windows":
                os.environ["NODE_SKIP_PLATFORM_CHECK"] = "1"
            self.assert_run_program(pkg_dir, "npm", ["--version"], f"{build_config.npm_version}")

        def test_npx(self) -> None:
            if platform.system() == "Windows":
                os.environ["NODE_SKIP_PLATFORM_CHECK"] = "1"
            self.assert_run_program(pkg_dir, "npx", ["--version"], f"{build_config.npm_version}")

    return TestCase


pkg = PackageDescr(
    name="node",
    versions=list(build_configs.keys()),
    make_pkg=make_pkg,
    create_tests=create_tests,
    sha256s={
        "https://nodejs.org/dist/v10.24.0/node-v10.24.0-linux-x64.tar.xz": "a937fb43225289ada54c6c3272a2ad18e1e33b8c7d6211c289d421b5051fdbd0",
        "https://nodejs.org/dist/v10.24.0/node-v10.24.0-win-x64.zip": "abf0aa48f642aa9ef6cc0021d2fe0275a60feece603664a76c31a812adc710bb",
        "https://nodejs.org/dist/v12.19.0/node-v12.19.0-linux-x64.tar.xz": "6e878d5e7aedaffb16de27ed65ee8d8351282c146caf8aa3ef726fded26226c5",
        "https://nodejs.org/dist/v12.19.0/node-v12.19.0-win-x64.zip": "8eead3c0d5a414c4985941ba78c581cb5c773b730957c128e5c764d10094cf68",
        "https://nodejs.org/dist/v12.21.0/node-v12.21.0-linux-x64.tar.xz": "eb89c02153cfa25e40170e5e9b0ab43ad55d456af8b72ad2a8c2a42b7a647432",
        "https://nodejs.org/dist/v12.21.0/node-v12.21.0-win-x64.zip": "d8ae037fb8be60e74fb96124e341fdf1251eae0d5d88d7d86f056d4b0c9440f3",
        "https://nodejs.org/dist/v14.16.0/node-v14.16.0-linux-x64.tar.xz": "2e079cf638766fedd720d30ec8ffef5d6ceada4e8b441fc2a093cb9a865f4087",
        "https://nodejs.org/dist/v14.16.0/node-v14.16.0-win-x64.zip": "716045c2f16ea10ca97bd04cf2e5ef865f9c4d6d677a9bc25e2ea522b594af4f",
        "https://nodejs.org/dist/v8.12.0/node-v8.12.0-linux-x64.tar.xz": "29a20479cd1e3a03396a4e74a1784ccdd1cf2f96928b56f6ffa4c8dae40c88f2",
        "https://nodejs.org/dist/v8.12.0/node-v8.12.0-win-x64.zip": "9b22c9b23148b61ea0052826b3ac0255b8a3a542c125272b8f014f15bf11b091",
        "https://nodejs.org/dist/v8.17.0/node-v8.17.0-linux-x64.tar.xz": "b7f6dd77fb173c8c7c30d61d0702eefc236bba74398538aa77bfa2bb47bddce6",
        "https://nodejs.org/dist/v8.17.0/node-v8.17.0-win-x64.zip": "e95a63e81b27e78872c0efb9dd5809403014dbf9896035cc17adf51a350f88fa",
    },
)

if __name__ == "__main__":
    main_driver(pkg)
