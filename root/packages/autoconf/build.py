import os.path
import textwrap
from collections import OrderedDict
from pathlib import Path
from typing import Dict, NamedTuple, Type

from ...lib.main_driver import main_driver
from ...lib.test import BuildContext, PackageDescr, PackageTestCase


class BuildConfig(NamedTuple):
    make_version: str


build_configs: Dict[str, BuildConfig] = OrderedDict(
    [
        ("2.69", BuildConfig(make_version="4.1")),
        ("2.70", BuildConfig(make_version="4.1")),
        ("2.71", BuildConfig(make_version="4.1")),
    ]
)


def make_pkg(ctx: BuildContext, pkg_version: str) -> None:
    build_config = build_configs[pkg_version]

    make_dir = ctx.devpacks_pkg("make", build_config.make_version)

    url = f"https://ftp.gnu.org/gnu/autoconf/autoconf-{pkg_version}.tar.gz"

    ctx.download_root(url)

    ctx.shell.env["PATH"] = os.path.pathsep.join(
        [str(Path(make_dir) / "bin"), ctx.shell.env["PATH"],]
    )

    ctx.shell.run(
        "./configure", ["--prefix=" + str(ctx.pkg_dir / "local"),],
    )
    ctx.shell.run("make", [])
    ctx.shell.run("make", ["install"])

    ctx.shell.rename("bin", "_bin")

    ctx.shell.mkdir("bin")
    ctx.shell.rename("local/share", "share")

    ctx.shell.write_file(
        ctx.pkg_dir / "bin" / "autoconf",
        textwrap.dedent(
            f"""\
            #!/bin/sh
            DIR=`dirname "$0"`
            export AUTOM4TE="$DIR/autom4te"
            exec "$DIR/../local/bin/autoconf" "$@"
            """
        ),
    )
    os.chmod(ctx.pkg_dir / "bin" / "autoconf", 0o775)

    def install_perl_exe(name: str) -> None:
        ctx.shell.write_file(
            ctx.pkg_dir / "bin" / name,
            textwrap.dedent(
                f"""\
                #!/bin/sh
                DIR=`dirname "$0"`
                export PERL5LIB="$DIR/../share/autoconf"
                export autom4te_perllibdir="$DIR/../share/autoconf"
                export AUTOCONF="$DIR/autoconf"
                export AUTOHEADER="$DIR/autoheader"
                export AUTOM4TE="$DIR/autom4te"
                export AC_MACRODIR="$DIR/../share/autoconf"
                autom4te_cfg=`mktemp`
                sed -E "s%'.*share/autoconf'%'$DIR/../share/autoconf'%" "$DIR/../share/autoconf/autom4te.cfg" > "$autom4te_cfg"
                export AUTOM4TE_CFG="$autom4te_cfg"
                exec "$DIR/../local/bin/{name}" "$@"
                """
            ),
        )
        os.chmod(ctx.pkg_dir / "bin" / name, 0o775)

    install_perl_exe("autoheader")
    install_perl_exe("autom4te")
    install_perl_exe("autoreconf")
    install_perl_exe("autoscan")
    install_perl_exe("autoupdate")
    install_perl_exe("ifnames")


def create_tests(pkg_dir: Path, pkg_version: str) -> Type[PackageTestCase]:
    class TestCase(PackageTestCase):
        def test_autoconf(self) -> None:
            self.assert_run_program_first_line(
                pkg_dir, "autoconf", ["--version"], f"autoconf (GNU Autoconf) {pkg_version}",
            )

        def test_autoreconf(self) -> None:
            self.assert_run_program_first_line(
                pkg_dir, "autoconf", ["--version"], f"autoconf (GNU Autoconf) {pkg_version}",
            )

        def test_autom4te(self) -> None:
            self.assert_run_program_first_line(
                pkg_dir, "autom4te", ["--version"], f"autom4te (GNU Autoconf) {pkg_version}",
            )

    return TestCase


pkg = PackageDescr(
    name="autoconf",
    versions=list(build_configs.keys()),
    make_pkg=make_pkg,
    create_tests=create_tests,
    sha256s={
        "https://ftp.gnu.org/gnu/autoconf/autoconf-2.69.tar.gz": "954bd69b391edc12d6a4a51a2dd1476543da5c6bbf05a95b59dc0dd6fd4c2969",
        "https://ftp.gnu.org/gnu/autoconf/autoconf-2.70.tar.gz": "f05f410fda74323ada4bdc4610db37f8dbd556602ba65bc843edb4d4d4a1b2b7",
        "https://ftp.gnu.org/gnu/autoconf/autoconf-2.71.tar.gz": "431075ad0bf529ef13cb41e9042c542381103e80015686222b8a9d4abef42a1c",
    },
)

if __name__ == "__main__":
    main_driver(pkg)
