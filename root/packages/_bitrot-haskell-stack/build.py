import os
import subprocess

import pkglib


class Package(pkglib.PackageBuilder):
    name = "haskell-stack"
    version = "2.1.3"

    # stack_version = '1.6.3'
    zlib_version = "1.2.11"
    libgmp_version = "6.1.2"

    sha256s = {
        "https://github.com/commercialhaskell/stack/releases/download/v1.6.3/stack-1.6.3-linux-x86_64-static.tar.gz": "c16b6b1fc31edb203dac9cd3745872c25ea088f12ca19dffb368de73e8b2b89b",
        "https://github.com/commercialhaskell/stack/releases/download/v1.6.3/stack-1.6.3-osx-x86_64.tar.gz": "4defbbaca2256a3d281175c1de4cfa1a7720ce90f5622d31aa8601f7ac0d7cad",
        "https://github.com/commercialhaskell/stack/releases/download/v1.6.3/stack-1.6.3-windows-x86_64.zip": "461b3653496340b09dacaee6bdbf696a3d676e73031ace8e53cdc2479468301b",
        "https://github.com/commercialhaskell/stack/releases/download/v1.7.1/stack-1.7.1-sdist-0.tar.gz": "44c085aa99228f1f0c67866da9453c2e50e5d894381ee049aaa91afd79066107",
        "https://github.com/commercialhaskell/stack/releases/download/v2.1.3/stack-2.1.3-linux-x86_64-static.tar.gz": "4e937a6ad7b5e352c5bd03aef29a753e9c4ca7e8ccc22deb5cd54019a8cf130c",
    }

    def make_pkg(self):

        url = {
            (
                "darwin",
                "x86_64",
            ): f"https://github.com/commercialhaskell/stack/releases/download/v{self.version}/stack-{self.version}-osx-x86_64.tar.gz",
            (
                "linux",
                "x86_64",
            ): f"https://github.com/commercialhaskell/stack/releases/download/v{self.version}/stack-{self.version}-linux-x86_64-static.tar.gz",
            (
                "windows",
                "x86_64",
            ): f"https://github.com/commercialhaskell/stack/releases/download/v{self.version}/stack-{self.version}-windows-x86_64.zip",
        }[self.os, self.arch]

        self.download_root(url)

        os.makedirs(os.path.join(self.pkg_dir, "bin"), exist_ok=True)
        if self.os == "windows":
            os.rename(
                os.path.join(self.pkg_dir, "stack.exe"),
                os.path.join(self.pkg_dir, "bin", "stack.exe"),
            )
        else:
            os.rename(
                os.path.join(self.pkg_dir, "stack"), os.path.join(self.pkg_dir, "bin", "stack"),
            )

    #        if self.os == 'linux':
    #            stack_prog = subprocess.run(['devpacks', 'quick-pkg-path', 'haskell-stack', '-t', Package.stack_version, '-p', 'stack'], encoding='utf-8', check=True, stdout=subprocess.PIPE).stdout
    #            zlib_dir = subprocess.run(['devpacks', 'quick-pkg-path', 'zlib', '-t', Package.zlib_version], encoding='utf-8', check=True, stdout=subprocess.PIPE).stdout
    #            libgmp_dir = subprocess.run(['devpacks', 'quick-pkg-path', 'libgmp', '-t', Package.libgmp_version], encoding='utf-8', check=True, stdout=subprocess.PIPE).stdout
    #
    #            with pkglib.TmpDir() as tmp_dir:
    #                self.download_extract(url, tmp_dir)
    #
    #                env = dict(os.environ)
    #                env['C_INCLUDE_PATH'] = os.path.join(zlib_dir, 'include')
    #                env['LIBRARY_PATH'] = os.path.join(zlib_dir, 'lib') + os.pathsep + os.path.join(libgmp_dir, 'lib')
    #
    #                subprocess.run([
    #                    stack_prog, 'build'
    #                    ], cwd=tmp_dir, env=env, check=True)
    #
    #                root_dir = subprocess.run([
    #                    stack_prog, 'path', '--local-install-root'
    #                    ], cwd=tmp_dir, env=env, encoding='utf-8', stdout=subprocess.PIPE, check=True).stdout.strip()
    #
    #                os.makedirs(os.path.join(self.pkg_dir, 'bin'), exist_ok=True)
    #                os.rename(os.path.join(root_dir, 'bin', 'stack'), os.path.join(self.pkg_dir, 'bin', 'stack'))
    #        else:
    #            self.download_root(url)
    #
    #            os.makedirs(os.path.join(self.pkg_dir, 'bin'), exist_ok=True)
    #            if self.os == 'windows':
    #                os.rename(os.path.join(self.pkg_dir, 'stack.exe'), os.path.join(self.pkg_dir, 'bin', 'stack.exe'))
    #            else:
    #                os.rename(os.path.join(self.pkg_dir, 'stack'), os.path.join(self.pkg_dir, 'bin', 'stack'))

    def create_tests(self, pkg_dir):
        class PackageTestCase(pkglib.PackageTestCase):
            def test_stack(self):
                out = self.run_program(pkg_dir, "stack", ["--version"])
                out_version = " ".join(out.split("\n")[0].split()[0:2])
                if out_version[-1] == ",":
                    out_version = out_version[:-1]
                self.assertEqual(out_version, f"Version {Package.version}")

        return PackageTestCase


if __name__ == "__main__":
    Package().run()
