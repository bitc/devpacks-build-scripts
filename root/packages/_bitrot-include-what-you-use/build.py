import os
import os.path
import shutil
import subprocess
import textwrap

import pkglib


class Package(pkglib.PackageBuilder):
    name = "include-what-you-use"
    version = "0.11"

    clang_version = "7.0.0"

    cmake_version = "3.10.1"
    python_version = "3.6.4"
    zlib_version = "1.2.11"

    sha256s = {
        "https://include-what-you-use.org/downloads/include-what-you-use-0.11.src.tar.gz": "2d2877726c4aed9518cbb37673ffbc2b7da9c239bf8fe29432da35c1c0ec367a",
        "http://releases.llvm.org/7.0.0/llvm-7.0.0.src.tar.xz": "8bc1f844e6cbde1b652c19c1edebc1864456fd9c78b8c1bea038e51b363fe222",
        "http://releases.llvm.org/7.0.0/cfe-7.0.0.src.tar.xz": "550212711c752697d2f82c648714a7221b1207fd9441543ff4aa9e3be45bba55",
        "http://releases.llvm.org/7.0.0/clang-tools-extra-7.0.0.src.tar.xz": "937c5a8c8c43bc185e4805144744799e524059cac877a44d9063926cd7a19dbe",
        "http://releases.llvm.org/7.0.0/compiler-rt-7.0.0.src.tar.xz": "bdec7fe3cf2c85f55656c07dfb0bd93ae46f2b3dd8f33ff3ad6e7586f4c670d6",
        "http://releases.llvm.org/7.0.0/libcxx-7.0.0.src.tar.xz": "9b342625ba2f4e65b52764ab2061e116c0337db2179c6bce7f9a0d70c52134f0",
        "http://releases.llvm.org/7.0.0/libcxxabi-7.0.0.src.tar.xz": "9b45c759ff397512eae4d938ff82827b1bd7ccba49920777e5b5e460baeb245f",
    }

    def make_pkg(self):
        cmake_bin = subprocess.run(
            ["devpacks", "quick-pkg-path", "cmake", "-t", Package.cmake_version, "-p", "cmake",],
            encoding="utf-8",
            check=True,
            stdout=subprocess.PIPE,
        ).stdout
        python_dir = subprocess.run(
            ["devpacks", "quick-pkg-path", "python", "-t", Package.python_version],
            encoding="utf-8",
            check=True,
            stdout=subprocess.PIPE,
        ).stdout
        zlib_dir = subprocess.run(
            ["devpacks", "quick-pkg-path", "zlib", "-t", Package.zlib_version],
            encoding="utf-8",
            check=True,
            stdout=subprocess.PIPE,
        ).stdout

        url = f"https://include-what-you-use.org/downloads/include-what-you-use-{self.version}.src.tar.gz"

        llvm_url = (
            f"http://releases.llvm.org/{self.clang_version}/llvm-{self.clang_version}.src.tar.xz"
        )
        clang_url = (
            f"http://releases.llvm.org/{self.clang_version}/cfe-{self.clang_version}.src.tar.xz"
        )
        clang_tools_extra_url = f"http://releases.llvm.org/{self.clang_version}/clang-tools-extra-{self.clang_version}.src.tar.xz"
        compiler_rt_url = f"http://releases.llvm.org/{self.clang_version}/compiler-rt-{self.clang_version}.src.tar.xz"
        libcxx_url = (
            f"http://releases.llvm.org/{self.clang_version}/libcxx-{self.clang_version}.src.tar.xz"
        )
        libcxxabi_url = f"http://releases.llvm.org/{self.clang_version}/libcxxabi-{self.clang_version}.src.tar.xz"

        # Step 1: Download and extract all the required packages into the right places

        # Reference: <https://clang.llvm.org/get_started.html>

        os.mkdir(os.path.join(self.pkg_dir, "include-what-you-use"))
        self.download_extract(url, os.path.join(self.pkg_dir, "include-what-you-use"))

        os.mkdir(os.path.join(self.pkg_dir, "llvm"))
        self.download_extract(llvm_url, os.path.join(self.pkg_dir, "llvm"))

        os.mkdir(os.path.join(self.pkg_dir, "llvm", "tools", "clang"))
        self.download_extract(clang_url, os.path.join(self.pkg_dir, "llvm", "tools", "clang"))

        # os.mkdir(os.path.join(self.pkg_dir, 'llvm', 'tools', 'clang', 'tools', 'extra'))
        # self.download_extract(clang_tools_extra_url, os.path.join(self.pkg_dir, 'llvm', 'tools', 'clang', 'tools', 'extra'))
        #
        # os.mkdir(os.path.join(self.pkg_dir, 'llvm', 'projects', 'compiler-rt'))
        # self.download_extract(compiler_rt_url, os.path.join(self.pkg_dir, 'llvm', 'projects', 'compiler-rt'))
        #
        # os.mkdir(os.path.join(self.pkg_dir, 'llvm', 'projects', 'libcxx'))
        # self.download_extract(libcxx_url, os.path.join(self.pkg_dir, 'llvm', 'projects', 'libcxx'))
        #
        # os.mkdir(os.path.join(self.pkg_dir, 'llvm', 'projects', 'libcxxabi'))
        # self.download_extract(libcxxabi_url, os.path.join(self.pkg_dir, 'llvm', 'projects', 'libcxxabi'))

        # Step 2: Build llvm

        env = dict(os.environ)
        env["CFLAGS"] = f'-I{os.path.join(zlib_dir, "include")} ' + env.get("CFLAGS", "")
        env["CXXFLAGS"] = f'-I{os.path.join(zlib_dir, "include")} ' + env.get("CFLAGS", "")
        env["LDFLAGS"] = f'-L{os.path.join(zlib_dir, "lib")} ' + env.get("LDFLAGS", "")

        os.mkdir(os.path.join(self.pkg_dir, "build_llvm"))

        subprocess.run(
            [
                cmake_bin,
                os.path.join("..", "llvm"),
                "-DLIBCXX_CXX_ABI=libstdc++",
                "-G",
                "Unix Makefiles",
            ],
            cwd=os.path.join(self.pkg_dir, "build_llvm"),
            env=env,
            check=True,
        )

        subprocess.run(["make"], cwd=os.path.join(self.pkg_dir, "build_llvm"), check=True)

        # Step 3: Build include-what-you-use

        os.mkdir(os.path.join(self.pkg_dir, "build_iwyu"))

        subprocess.run(
            [
                cmake_bin,
                os.path.join("..", "include-what-you-use"),
                "-DCMAKE_PREFIX_PATH=" + os.path.join("..", "build_llvm"),
                "-G",
                "Unix Makefiles",
            ],
            cwd=os.path.join(self.pkg_dir, "build_iwyu"),
            env=env,
            check=True,
        )

        subprocess.run(["make"], cwd=os.path.join(self.pkg_dir, "build_iwyu"), check=True)

        subprocess.run(
            ["strip", "bin/include-what-you-use"],
            cwd=os.path.join(self.pkg_dir, "build_iwyu"),
            check=True,
        )

        # Step 4: Move all the needed built artifacts into the correct places

        os.rename(
            os.path.join(self.pkg_dir, "build_iwyu", "bin"), os.path.join(self.pkg_dir, "bin"),
        )
        os.mkdir(os.path.join(self.pkg_dir, "lib"))
        os.rename(
            os.path.join(self.pkg_dir, "build_llvm", "lib", "clang"),
            os.path.join(self.pkg_dir, "lib", "clang"),
        )

        # Step 5: Install Python

        shutil.copytree(
            python_dir,
            os.path.join(self.pkg_dir, pkglib.package_slug("python", Package.python_version)),
            symlinks=True,
        )

        # Step 6: Install python scripts

        bins = [("fix_includes", "fix_includes.py"), ("iwyu_tool", "iwyu_tool.py")]

        for bin_name, target_file in bins:
            shutil.copy(
                os.path.join(self.pkg_dir, "include-what-you-use", target_file),
                os.path.join(self.pkg_dir, "bin", target_file),
            )
            os.chmod(os.path.join(self.pkg_dir, "bin", target_file), 0o664)
            with open(os.path.join(self.pkg_dir, "bin", bin_name), "w", encoding="utf-8") as f:
                f.write(
                    textwrap.dedent(
                        f"""\
                    #!/bin/sh
                    DIR=`dirname "$0"`
                    exec "$DIR"/../{pkglib.package_slug('python', Package.python_version)}/bin/python "$DIR"/{target_file} "$@"
                    """
                    )
                )
            os.chmod(os.path.join(self.pkg_dir, "bin", bin_name), 0o775)

        # Step 7: Cleanup

        shutil.rmtree(os.path.join(self.pkg_dir, "build_iwyu"))
        shutil.rmtree(os.path.join(self.pkg_dir, "build_llvm"))
        shutil.rmtree(os.path.join(self.pkg_dir, "include-what-you-use"))
        shutil.rmtree(os.path.join(self.pkg_dir, "llvm"))

    def create_tests(self, pkg_dir):
        class PackageTestCase(pkglib.PackageTestCase):
            def test_include_what_you_use(self):
                out = self.run_program(pkg_dir, "include-what-you-use", ["--version"])
                out_version = " ".join(out.split()[0:7])
                self.assertEqual(
                    out_version,
                    f"include-what-you-use {Package.version} based on clang version {Package.clang_version}",
                )

        return PackageTestCase


if __name__ == "__main__":
    Package().run()
