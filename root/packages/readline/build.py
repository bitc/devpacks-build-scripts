import glob
import os.path
from collections import OrderedDict
from pathlib import Path
from typing import Dict, NamedTuple, Type

from ...lib.main_driver import main_driver
from ...lib.test import BuildContext, PackageDescr, PackageTestCase


class BuildConfig(NamedTuple):
    gcc_version: str
    make_version: str
    ncurses_version: str


build_configs: Dict[str, BuildConfig] = OrderedDict(
    [("8.0", BuildConfig(gcc_version="7.5.0", make_version="4.1", ncurses_version="6.2"))]
)


def make_pkg(ctx: BuildContext, pkg_version: str) -> None:
    build_config = build_configs[pkg_version]

    gcc_dir = ctx.devpacks_pkg("gcc", build_config.gcc_version)
    make_dir = ctx.devpacks_pkg("make", build_config.make_version)
    ncurses_dir = ctx.devpacks_pkg("ncurses", build_config.ncurses_version)

    url = f"http://ftp.gnu.org/gnu/readline/readline-{pkg_version}.tar.gz"

    ctx.download_extract(url, ctx.tmp_dir)

    with ctx.shell.cd(ctx.tmp_dir):
        ctx.shell.env["PATH"] = os.path.pathsep.join(
            [str(Path(gcc_dir) / "bin"), str(Path(make_dir) / "bin"), ctx.shell.env["PATH"],]
        )

        ctx.shell.env["CPPFLAGS"] = f'-I{ncurses_dir / "include"} ' + ctx.shell.env.get(
            "CPPFLAGS", ""
        )
        ctx.shell.env["LDFLAGS"] = f'-L{ncurses_dir / "lib"} ' + ctx.shell.env.get("LDFLAGS", "")

        ctx.shell.env["CFLAGS"] = "-fPIC " + ctx.shell.env.get("CFLAGS", "")

        ctx.shell.run(
            "./configure",
            [
                "--prefix=" + str(ctx.tmp_dir / "local"),
                "--enable-shared",
                "--enable-static",
                "--disable-install-examples",
                "--with-curses",
                "--enable-multibyte",
            ],
        )

        ctx.shell.run("make", ["SHLIB_LIBS=-lncursesw"])
        ctx.shell.run("make", ["SHLIB_LIBS=-lncursesw", "install"])

        ctx.shell.rename("local/include", ctx.pkg_dir / "include")
        ctx.shell.rename("local/lib", ctx.pkg_dir / "lib")
        ctx.shell.rename("local/share", ctx.pkg_dir / "share")

    ctx.shell.mkdir(ctx.pkg_dir / "lib-dynamic")
    for so_file in glob.glob(str(ctx.pkg_dir / "lib" / "*.so*")):
        ctx.shell.rename(so_file, ctx.pkg_dir / "lib-dynamic" / os.path.basename(so_file))


def create_tests(pkg_dir: Path, pkg_version: str) -> Type[PackageTestCase]:
    class TestCase(PackageTestCase):
        def test_readline_h(self) -> None:
            self.assertTrue(os.path.isfile(pkg_dir / "include" / "readline" / "readline.h"))

        def test_libreadline(self) -> None:
            self.assertTrue(os.path.isfile(pkg_dir / "lib" / "libreadline.a"))
            self.assertTrue(os.path.isfile(pkg_dir / "lib" / "libhistory.a"))

        def test_libreadline_dynamic(self) -> None:
            self.assertTrue(os.path.isfile(pkg_dir / "lib-dynamic" / "libreadline.so"))
            self.assertTrue(os.path.isfile(pkg_dir / "lib-dynamic" / "libhistory.so"))

    return TestCase


pkg = PackageDescr(
    name="readline",
    versions=list(build_configs.keys()),
    make_pkg=make_pkg,
    create_tests=create_tests,
    sha256s={
        "http://ftp.gnu.org/gnu/readline/readline-8.0.tar.gz": "e339f51971478d369f8a053a330a190781acb9864cf4c541060f12078948e461"
    },
)

if __name__ == "__main__":
    main_driver(pkg)
