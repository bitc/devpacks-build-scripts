import os.path
from collections import OrderedDict
from pathlib import Path
from typing import Dict, NamedTuple, Type

from ...lib.main_driver import main_driver
from ...lib.test import BuildContext, PackageDescr, PackageTestCase


class BuildConfig(NamedTuple):
    make_version: str
    gcc_version: str


build_configs: Dict[str, BuildConfig] = OrderedDict(
    [("3.0", BuildConfig(make_version="4.1", gcc_version="7.5.0"),)]
)


def make_pkg(ctx: BuildContext, pkg_version: str) -> None:
    build_config = build_configs[pkg_version]

    make_dir = ctx.devpacks_pkg("make", build_config.make_version)
    gcc_dir = ctx.devpacks_pkg("gcc", build_config.gcc_version)

    if pkg_version == "3.0":
        url = "ftp://ftp.info-zip.org/pub/infozip/src/zip30.tgz"
    else:
        raise RuntimeError("Unknown pkg_version", pkg_version)

    ctx.download_root(url)

    ctx.shell.env["PATH"] = os.path.pathsep.join(
        [str(Path(gcc_dir) / "bin"), str(Path(make_dir) / "bin"), ctx.shell.env["PATH"],]
    )

    ctx.shell.run("make", ["-f", "unix/Makefile", "generic_gcc"])
    ctx.shell.run("make", ["BINDIR=bin", "MANDIR=man/man1", "-f", "unix/Makefile", "install"])


def create_tests(pkg_dir: Path, pkg_version: str) -> Type[PackageTestCase]:
    class TestCase(PackageTestCase):
        def test_zip(self) -> None:
            self.assert_run_program_stdout_substring(
                pkg_dir, "zip", ["--version"], f"Zip {pkg_version}"
            )

    return TestCase


pkg = PackageDescr(
    name="zip",
    versions=list(build_configs.keys()),
    make_pkg=make_pkg,
    create_tests=create_tests,
    sha256s={
        "ftp://ftp.info-zip.org/pub/infozip/src/zip30.tgz": "f0e8bb1f9b7eb0b01285495a2699df3a4b766784c1765a8f1aeedf63c0806369"
    },
)

if __name__ == "__main__":
    main_driver(pkg)
