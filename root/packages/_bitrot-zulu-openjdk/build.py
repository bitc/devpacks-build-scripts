import distutils.version

import pkglib


class Package(pkglib.PackageBuilder):
    name = "zulu-openjdk"
    version = "9.0.1.3"

    java_major_minor_patch = ".".join(
        [str(d) for d in distutils.version.LooseVersion(version).version[0:3]]
    )

    sha256s = {
        f"http://cdn.azul.com/zulu/bin/zulu{version}-jdk{java_major_minor_patch}-macosx_x64.tar.gz": "14c252a0e565e1eacbc4c4c0d17c17368a3f2f348e9aef1104b516856fd25641",
        f"http://cdn.azul.com/zulu/bin/zulu{version}-jdk{java_major_minor_patch}-linux_x64.tar.gz": "a68605e467a516309fe44bedfb07973d145e918fc25d410cf2c795d922d6f82e",
    }

    def make_pkg(self):

        url = {
            (
                "darwin",
                "x86_64",
            ): f"http://cdn.azul.com/zulu/bin/zulu{self.version}-jdk{Package.java_major_minor_patch}-macosx_x64.tar.gz",
            (
                "linux",
                "x86_64",
            ): f"http://cdn.azul.com/zulu/bin/zulu{self.version}-jdk{Package.java_major_minor_patch}-linux_x64.tar.gz",
        }[self.os, self.arch]

        self.download_root(url)

    def create_tests(self, pkg_dir):
        class PackageTestCase(pkglib.PackageTestCase):
            def test_java(self):
                self.assert_run_program_first_line(
                    pkg_dir, "java", ["--version"], f"openjdk {Package.version}"
                )

            def test_javac(self):
                self.assert_run_program_first_line(
                    pkg_dir, "javac", ["--version"], f"javac {Package.version}"
                )

            def test_jar(self):
                self.assert_run_program_first_line(
                    pkg_dir, "jar", ["--version"], f"jar {Package.version}"
                )

        return PackageTestCase


if __name__ == "__main__":
    Package().run()
