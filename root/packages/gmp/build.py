import glob
import os.path
from collections import OrderedDict
from pathlib import Path
from typing import Dict, NamedTuple, Type

from ...lib.main_driver import main_driver
from ...lib.test import BuildContext, PackageDescr, PackageTestCase


class BuildConfig(NamedTuple):
    make_version: str
    gcc_version: str
    m4_version: str


build_configs: Dict[str, BuildConfig] = OrderedDict(
    [
        ("6.1.2", BuildConfig(make_version="4.1", gcc_version="4.9.4", m4_version="1.4.18")),
        ("6.2.0", BuildConfig(make_version="4.1", gcc_version="4.9.4", m4_version="1.4.18")),
    ]
)


def make_pkg(ctx: BuildContext, pkg_version: str) -> None:
    build_config = build_configs[pkg_version]

    make_dir = ctx.devpacks_pkg("make", build_config.make_version)
    gcc_dir = ctx.devpacks_pkg("gcc", build_config.gcc_version)
    m4_bin = ctx.shell.cmd_stdout(
        "devpacks", ["quick-pkg-path", "m4", "-t", build_config.m4_version, "-p", "m4"]
    )

    url = f"https://gmplib.org/download/gmp/gmp-{pkg_version}.tar.xz"

    src_dir = ctx.tmp_dir / "src"
    ctx.shell.mkdir(src_dir)

    ctx.download_extract(url, src_dir)

    ctx.shell.env["PATH"] = os.pathsep.join(
        [str(gcc_dir / "bin"), str(make_dir / "bin"), ctx.shell.env["PATH"]]
    )

    with ctx.shell.cd(src_dir):
        ctx.shell.run(
            "./configure",
            [
                f"--prefix={ctx.pkg_dir}",
                "--build=x86_64-linux-gnu",
                "--disable-fat",  # TODO Debian uses "--enable-fat" for all arches other than: amd64 kfreebsd-amd64 lpia
                "CFLAGS=-fPIC -O3",  # TODO For ia64 Debian uses -O2
                f"M4={m4_bin}",
            ],
        )

        ctx.shell.run("make", [])
        ctx.shell.run("make", ["install"])
        ctx.shell.run("make", ["check"])

    # Delete all "lib64/*.la" files (they have broken absolute paths and should
    # not be used)
    for la_file in glob.glob(str(ctx.pkg_dir / "lib" / "*.la")):
        ctx.shell.remove(la_file)

    # Move all "lib/*.so*" files to a separate "lib-dynamic"
    #
    # This enables dynamic linking but only when explicitly requested (by
    # providing the flag -L<path_to>/lib-dynamic)
    ctx.shell.mkdir(ctx.pkg_dir / "lib-dynamic")
    for so_file in glob.glob(str(ctx.pkg_dir / "lib" / "*.so*")):
        ctx.shell.rename(so_file, ctx.pkg_dir / "lib-dynamic" / os.path.basename(so_file))


def create_tests(pkg_dir: Path, pkg_version: str) -> Type[PackageTestCase]:
    class TestCase(PackageTestCase):
        def test_gmp_h(self) -> None:
            exists = os.path.isfile(os.path.join(pkg_dir, "include", "gmp.h"))
            self.assertTrue(exists, "gmp.h header file exists")

        def test_libgmp_a(self) -> None:
            exists = os.path.isfile(os.path.join(pkg_dir, "lib", "libgmp.a"))
            self.assertTrue(exists, "libgmp.a lib file exists")

        def test_libgmp_so(self) -> None:
            exists = os.path.isfile(os.path.join(pkg_dir, "lib-dynamic", "libgmp.so"))
            self.assertTrue(exists, "libgmp.so lib file exists")

    return TestCase


pkg = PackageDescr(
    name="gmp",
    versions=list(build_configs.keys()),
    make_pkg=make_pkg,
    create_tests=create_tests,
    sha256s={
        "https://gmplib.org/download/gmp/gmp-6.1.2.tar.xz": "87b565e89a9a684fe4ebeeddb8399dce2599f9c9049854ca8c0dfbdea0e21912"
    },
)

if __name__ == "__main__":
    main_driver(pkg)
