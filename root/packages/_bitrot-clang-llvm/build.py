import os
import os.path
import subprocess

import pkglib


class Package(pkglib.PackageBuilder):
    name = "clang-llvm"
    version = "7.0.0"

    _7_zip_version = "18.01"

    sha256s = {
        "http://releases.llvm.org/5.0.1/clang+llvm-5.0.1-x86_64-apple-darwin.tar.xz": "c5b105c4960619feb32641ef051fa39ecb913cc0feb6bacebdfa71f8d3cae277",
        "http://releases.llvm.org/5.0.1/clang+llvm-5.0.1-x86_64-linux-gnu-ubuntu-16.04.tar.xz": "005f21861cd2953138df7cf511f1552ef7041bafb7cfc4b172264b7ff5fe09b4",
        "http://releases.llvm.org/5.0.1/LLVM-5.0.1-win64.exe": "981543611d719624acb29a2cffd6a479cff36e8ab5ee8a57d8eca4f9c4c6956f",
        "http://releases.llvm.org/6.0.1/clang+llvm-6.0.1-x86_64-linux-gnu-ubuntu-16.04.tar.xz": "7ea204ecd78c39154d72dfc0d4a79f7cce1b2264da2551bb2eef10e266d54d91",
        "http://releases.llvm.org/7.0.0/clang+llvm-7.0.0-x86_64-linux-gnu-ubuntu-16.04.tar.xz": "69b85c833cd28ea04ce34002464f10a6ad9656dd2bba0f7133536a9927c660d2",
    }

    def make_pkg(self):
        url = {
            (
                "darwin",
                "x86_64",
            ): f"http://releases.llvm.org/{self.version}/clang+llvm-{self.version}-x86_64-apple-darwin.tar.xz",
            (
                "linux",
                "x86_64",
            ): f"http://releases.llvm.org/{self.version}/clang+llvm-{self.version}-x86_64-linux-gnu-ubuntu-16.04.tar.xz",
            (
                "windows",
                "x86_64",
            ): f"http://releases.llvm.org/{self.version}/LLVM-{self.version}-win64.exe",
        }[self.os, self.arch]

        if self.os == "windows":
            _7_zip_prog = subprocess.run(
                ["devpacks", "quick-pkg-path", "7-zip", "-t", Package._7_zip_version, "-p", "7z",],
                encoding="utf-8",
                check=True,
                stdout=subprocess.PIPE,
            ).stdout

            self.download_file(url, os.path.join(self.pkg_dir, "llvm-installer.exe"))
            subprocess.run(
                [
                    _7_zip_prog,
                    "x",
                    "-o" + self.pkg_dir,
                    "-xr!$PLUGINSDIR",
                    os.path.join(self.pkg_dir, "llvm-installer.exe"),
                ],
                check=True,
            )
            os.remove(os.path.join(self.pkg_dir, "llvm-installer.exe"))
        else:
            self.download_root(url)

    def create_tests(self, pkg_dir):
        class PackageTestCase(pkglib.PackageTestCase):
            def test_clang(self):
                out = self.run_program(pkg_dir, "clang", ["--version"])
                out_version = " ".join(out.split("\n")[0].split()[0:3])
                self.assertEqual(out_version, f"clang version {Package.version}")

            def test_clang_cc(self):
                out = self.run_program(pkg_dir, "clang++", ["--version"])
                out_version = " ".join(out.split("\n")[0].split()[0:3])
                self.assertEqual(out_version, f"clang version {Package.version}")

            def test_llvm_ar(self):
                out = self.run_program(pkg_dir, "llvm-ar", ["--version"])
                out_version = out.split("\n")[1].strip()
                self.assertEqual(out_version, f"LLVM version {Package.version}")

        return PackageTestCase


if __name__ == "__main__":
    Package().run()
