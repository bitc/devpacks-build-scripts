import glob
import os.path
from collections import OrderedDict
from pathlib import Path
from typing import Dict, NamedTuple, Type

from ...lib.main_driver import main_driver
from ...lib.test import BuildContext, PackageDescr, PackageTestCase


class BuildConfig(NamedTuple):
    gcc_version: str
    make_version: str
    patchelf_version: str


build_configs: Dict[str, BuildConfig] = OrderedDict(
    [("6.2", BuildConfig(gcc_version="7.5.0", make_version="4.1", patchelf_version="0.12")),]
)


def make_pkg(ctx: BuildContext, pkg_version: str) -> None:
    build_config = build_configs[pkg_version]

    gcc_dir = ctx.devpacks_pkg("gcc", build_config.gcc_version)
    make_dir = ctx.devpacks_pkg("make", build_config.make_version)
    patchelf_prog = ctx.devpacks_pkg_prog("patchelf", build_config.patchelf_version, "patchelf")

    url = f"http://ftp.gnu.org/gnu/ncurses/ncurses-{pkg_version}.tar.gz"

    ctx.shell.mkdir(ctx.tmp_dir / "static")
    ctx.shell.mkdir(ctx.tmp_dir / "staticw")
    ctx.shell.mkdir(ctx.tmp_dir / "dynamic")
    ctx.shell.mkdir(ctx.tmp_dir / "dynamicw")
    ctx.download_extract(url, ctx.tmp_dir / "static")
    ctx.download_extract(url, ctx.tmp_dir / "staticw")
    ctx.download_extract(url, ctx.tmp_dir / "dynamic")
    ctx.download_extract(url, ctx.tmp_dir / "dynamicw")

    ctx.shell.env["PATH"] = os.path.pathsep.join(
        [str(Path(gcc_dir) / "bin"), str(Path(make_dir) / "bin"), ctx.shell.env["PATH"],]
    )

    common_configure_flags = [
        "--disable-pkg-config",
        "--disable-pc-files",
        "--without-ada",
        "--without-cxx-binding",
        "--without-debug",
        "--without-tests",
        "--enable-symlinks",
        "--with-default-terminfo-dir=/etc/terminfo",
        "--with-terminfo-dirs=/etc/terminfo:/lib/terminfo:/usr/share/terminfo",
        "--with-xterm-kbs=del",
        "CFLAGS=-fPIC",
        "CXXFLAGS=-fPIC",
    ]

    with ctx.shell.cd(ctx.tmp_dir / "static"):
        ctx.shell.run(
            "./configure",
            ["--prefix=" + str(ctx.tmp_dir / "static" / "local"), "--disable-shared",]
            + common_configure_flags,
        )
        ctx.shell.run("make", [])
        ctx.shell.run("make", ["install"])

        ctx.shell.rename("local/include", ctx.pkg_dir / "include")
        ctx.shell.rename("local/lib", ctx.pkg_dir / "lib")
        ctx.shell.rename("local/bin", ctx.pkg_dir / "bin")
        ctx.shell.rename("local/share", ctx.pkg_dir / "share")

    with ctx.shell.cd(ctx.tmp_dir / "staticw"):
        ctx.shell.run(
            "./configure",
            [
                "--prefix=" + str(ctx.tmp_dir / "staticw" / "local"),
                "--disable-shared",
                "--enable-widec",
            ]
            + common_configure_flags,
        )
        ctx.shell.run("make", [])
        ctx.shell.run("make", ["install"])

        ctx.shell.rename("local/include/ncursesw", ctx.pkg_dir / "include/ncursesw")
        for f in os.listdir(ctx.tmp_dir / "staticw" / "local" / "lib"):
            ctx.shell.rename(ctx.tmp_dir / "staticw" / "local" / "lib" / f, ctx.pkg_dir / "lib" / f)

    with ctx.shell.cd(ctx.tmp_dir / "dynamic"):
        ctx.shell.run(
            "./configure",
            ["--prefix=" + str(ctx.tmp_dir / "dynamic" / "local"), "--with-shared",]
            + common_configure_flags,
        )
        ctx.shell.run("make", [])
        ctx.shell.run("make", ["install"])

        ctx.shell.mkdir(ctx.pkg_dir / "lib-dynamic")
        for so_file in glob.glob(str(ctx.tmp_dir / "dynamic" / "local" / "lib" / "*.so*")):
            if not os.path.islink(so_file):
                ctx.shell.run(patchelf_prog, ["--set-rpath", "$ORIGIN", so_file])
            ctx.shell.rename(so_file, ctx.pkg_dir / "lib-dynamic" / os.path.basename(so_file))

    with ctx.shell.cd(ctx.tmp_dir / "dynamicw"):
        ctx.shell.run(
            "./configure",
            [
                "--prefix=" + str(ctx.tmp_dir / "dynamicw" / "local"),
                "--with-shared",
                "--enable-widec",
            ]
            + common_configure_flags,
        )
        ctx.shell.run("make", [])
        ctx.shell.run("make", ["install"])

        for so_file in glob.glob(str(ctx.tmp_dir / "dynamicw" / "local" / "lib" / "*.so*")):
            if not os.path.islink(so_file):
                ctx.shell.run(patchelf_prog, ["--set-rpath", "$ORIGIN", so_file])
            ctx.shell.rename(so_file, ctx.pkg_dir / "lib-dynamic" / os.path.basename(so_file))


def create_tests(pkg_dir: Path, pkg_version: str) -> Type[PackageTestCase]:
    class TestCase(PackageTestCase):
        def test_ncurses_h(self) -> None:
            self.assertTrue(os.path.isfile(pkg_dir / "include" / "ncurses" / "ncurses.h"))
            self.assertTrue(os.path.isfile(pkg_dir / "include" / "ncurses" / "curses.h"))
            self.assertTrue(os.path.isfile(pkg_dir / "include" / "ncursesw" / "ncurses.h"))
            self.assertTrue(os.path.isfile(pkg_dir / "include" / "ncursesw" / "curses.h"))

        def test_libncurses(self) -> None:
            self.assertTrue(os.path.isfile(pkg_dir / "lib" / "libncurses.a"))
            self.assertTrue(os.path.isfile(pkg_dir / "lib" / "libncursesw.a"))

        def test_libncurses_dynamic(self) -> None:
            self.assertTrue(os.path.isfile(pkg_dir / "lib-dynamic" / "libncurses.so"))
            self.assertTrue(os.path.isfile(pkg_dir / "lib-dynamic" / "libncursesw.so"))

    return TestCase


pkg = PackageDescr(
    name="ncurses",
    versions=list(build_configs.keys()),
    make_pkg=make_pkg,
    create_tests=create_tests,
    sha256s={
        "http://ftp.gnu.org/gnu/ncurses/ncurses-6.2.tar.gz": "30306e0c76e0f9f1f0de987cf1c82a5c21e1ce6568b9227f7da5b71cbea86c9d"
    },
)

if __name__ == "__main__":
    main_driver(pkg)
