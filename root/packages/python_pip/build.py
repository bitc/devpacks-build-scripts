import distutils.version
import os.path
import shutil
import textwrap
from collections import OrderedDict
from pathlib import Path
from typing import Dict, NamedTuple, Type

from ...lib.ecosystems.python.pip import PipBuildConfig, make_pip_pkg, pip_sha256s
from ...lib.main_driver import main_driver
from ...lib.pkglib import package_slug
from ...lib.test import BuildContext, PackageDescr, PackageTestCase


class BuildConfig(NamedTuple):
    python_version: str


build_configs: Dict[str, BuildConfig] = OrderedDict(
    [("20.1.1", BuildConfig(python_version="3.6.8"),)]
)


def make_pkg(ctx: BuildContext, pkg_version: str) -> None:
    build_config = build_configs[pkg_version]
    python_dir = ctx.shell.cmd_stdout(
        "devpacks", ["quick-pkg-path", "python", "-t", build_config.python_version]
    )
    python_prog = ctx.shell.cmd_stdout(
        "devpacks",
        ["quick-pkg-path", "python", "-t", build_config.python_version, "-p", "python",],
    )

    pip_url = f"https://pypi.org/packages/source/p/pip/pip-{pkg_version}.tar.gz"
    # setuptools_url = f"https://pypi.org/packages/source/s/setuptools/setuptools-{pip_build_config.setuptools_version}.zip"

    shutil.copytree(
        python_dir,
        ctx.pkg_dir / package_slug("python", build_config.python_version),
        symlinks=True,
    )

    print(f"Extracting pip and setuptools into: {ctx.tmp_dir}")

    os.mkdir(ctx.pkg_dir / f"pip-{pkg_version}")
    ctx.download_extract(pip_url, ctx.pkg_dir / f"pip-{pkg_version}")

    # os.mkdir(ctx.tmp_dir / "setuptools")
    # ctx.download_extract(setuptools_url, ctx.tmp_dir / "setuptools")

    # ctx.shell.env["PYTHONPATH"] = os.path.pathsep.join(
    #    [str(ctx.tmp_dir / "pip" / "src"), str(ctx.tmp_dir / "setuptools")]
    # )

    # ctx.shell.run(
    #     python_prog,
    #     ["-m", "pip", "install", "--root", str(ctx.pkg_dir), pkg_name + "==" + pkg_version,],
    # )
    # os.rename(ctx.pkg_dir / "usr" / "local", ctx.pkg_dir / package_slug(pkg_name, pkg_version))
    # os.rmdir(ctx.pkg_dir / "usr")

    os.makedirs(ctx.pkg_dir / "bin", exist_ok=True)

    python_version = distutils.version.LooseVersion(build_config.python_version)
    python_major = python_version.version[0]
    python_minor = python_version.version[1]
    # site_packages_dir = (
    #     Path(package_slug(pkg_name, pkg_version))
    #     / "lib"
    #     / f"python{python_major}.{python_minor}"
    #     / "site-packages"
    # )
    # if not os.path.isdir(ctx.pkg_dir / site_packages_dir):
    #     raise RuntimeError(f'Couldn\'t find "site-packages" directory for {pkg_name}')

    # Create wrapper script for running the pkg
    ctx.shell.write_file(
        Path("bin") / "pip",
        textwrap.dedent(
            f"""\
            #!/bin/sh
            DIR=`dirname "$0"`
            PYTHONPATH="$DIR"/../pip-{pkg_version}/src exec "$DIR"/../{package_slug('python', build_config.python_version)}/bin/python -m pip "$@"
            """
        ),
    )
    os.chmod(ctx.pkg_dir / "bin" / "pip", 0o775)


def create_tests(pkg_dir: Path, pkg_version: str) -> Type[PackageTestCase]:
    class TestCase(PackageTestCase):
        def test_pip(self) -> None:
            out = self.run_program(pkg_dir, "pip", ["--version"])
            out_version = " ".join(out.split()[0:2])
            self.assertEqual(out_version, f"pip {pkg_version}")

    return TestCase


pkg = PackageDescr(
    name="python_pip",
    versions=list(build_configs.keys()),
    make_pkg=make_pkg,
    create_tests=create_tests,
    sha256s=pip_sha256s,
)

if __name__ == "__main__":
    main_driver(pkg)
