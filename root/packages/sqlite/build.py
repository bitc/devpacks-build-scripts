import distutils.version
import glob
import os
import os.path
import subprocess
from collections import OrderedDict
from pathlib import Path
from typing import Dict, NamedTuple, Tuple, Type

from ...lib.main_driver import main_driver
from ...lib.test import BuildContext, PackageDescr, PackageTestCase


class BuildConfig(NamedTuple):
    gcc_version: str
    make_version: str
    zlib_version: str
    ncurses_version: str
    readline_version: str


build_configs: Dict[str, Tuple[str, BuildConfig]] = OrderedDict(
    [
        (
            "3.34.1",
            (
                "2021",
                BuildConfig(
                    gcc_version="7.5.0",
                    make_version="4.1",
                    zlib_version="1.2.11",
                    ncurses_version="6.2",
                    readline_version="8.0",
                ),
            ),
        ),
    ]
)

# Converts "3.34.1" --> "3340100"
def flat_version(pkg_version: str) -> str:
    version = distutils.version.LooseVersion(pkg_version)
    major = version.version[0]
    minor = version.version[1]
    patch = version.version[2]
    return str(major) + str(minor).ljust(3, "0") + str(patch).ljust(3, "0")


def make_pkg(ctx: BuildContext, pkg_version: str) -> None:
    year, build_config = build_configs[pkg_version]

    gcc_dir = ctx.devpacks_pkg("gcc", build_config.gcc_version)
    make_dir = ctx.devpacks_pkg("make", build_config.make_version)
    zlib_dir = ctx.devpacks_pkg("zlib", build_config.zlib_version)
    readline_dir = ctx.devpacks_pkg("readline", build_config.readline_version)
    ncurses_dir = ctx.devpacks_pkg("ncurses", build_config.ncurses_version)

    url = f"https://www.sqlite.org/{year}/sqlite-autoconf-{flat_version(pkg_version)}.tar.gz"

    ctx.download_extract(url, ctx.tmp_dir)

    with ctx.shell.cd(ctx.tmp_dir):
        ctx.shell.env["PATH"] = os.path.pathsep.join(
            [str(Path(gcc_dir) / "bin"), str(Path(make_dir) / "bin"), ctx.shell.env["PATH"],]
        )

        ctx.shell.env["CFLAGS"] = f"-fPIC " + ctx.shell.env.get("CFLAGS", "")

        ctx.shell.env["CPPFLAGS"] = f'-I{zlib_dir / "include"} ' + ctx.shell.env.get("CPPFLAGS", "")
        ctx.shell.env["LDFLAGS"] = f'-L{zlib_dir / "lib"} ' + ctx.shell.env.get("LDFLAGS", "")

        ctx.shell.env["CPPFLAGS"] = f'-I{readline_dir / "include"} ' + ctx.shell.env.get(
            "CPPFLAGS", ""
        )
        ctx.shell.env["LDFLAGS"] = f'-L{readline_dir / "lib"} ' + ctx.shell.env.get("LDFLAGS", "")

        ctx.shell.env["CPPFLAGS"] = f'-I{ncurses_dir / "include"} ' + ctx.shell.env.get(
            "CPPFLAGS", ""
        )
        ctx.shell.env["LDFLAGS"] = f'-L{ncurses_dir / "lib"} ' + ctx.shell.env.get("LDFLAGS", "")

        ctx.shell.run(
            "./configure",
            [
                "--prefix=" + str(ctx.tmp_dir / "local"),
                "--enable-readline",
                "--enable-threadsafe",
                "--enable-dynamic-extensions",
                "--enable-shared",
                "--enable-static",
                "--enable-static-shell",
            ],
        )

        ctx.shell.run("make", [])
        ctx.shell.run("make", ["install"])

        ctx.shell.rename("local/bin", ctx.pkg_dir / "bin")
        ctx.shell.rename("local/include", ctx.pkg_dir / "include")
        ctx.shell.rename("local/lib", ctx.pkg_dir / "lib")
        ctx.shell.rename("local/share", ctx.pkg_dir / "share")

    ctx.shell.mkdir(ctx.pkg_dir / "lib-dynamic")
    for so_file in glob.glob(str(ctx.pkg_dir / "lib" / "*.so*")):
        ctx.shell.rename(so_file, ctx.pkg_dir / "lib-dynamic" / os.path.basename(so_file))


def create_tests(pkg_dir: Path, pkg_version: str) -> Type[PackageTestCase]:
    class TestCase(PackageTestCase):
        def test_sqlite3(self) -> None:
            self.assert_run_program_first_line_prefix(
                pkg_dir, "sqlite3", ["--version"], f"{pkg_version}"
            )

        def test_sqlite_h(self) -> None:
            self.assertTrue(os.path.isfile(pkg_dir / "include" / "sqlite3.h"))

        def test_lib(self) -> None:
            self.assertTrue(os.path.isfile(pkg_dir / "lib" / "libsqlite3.a"))

        def test_lib_dynamic(self) -> None:
            self.assertTrue(os.path.isfile(pkg_dir / "lib-dynamic" / "libsqlite3.so"))

    return TestCase

    def create_tests(self, pkg_dir):
        class PackageTestCase(pkglib.PackageTestCase):
            def test_xz(self):
                self.assert_run_program(
                    pkg_dir,
                    "xz",
                    ["--version"],
                    f"xz (XZ Utils) {Package.version}\nliblzma {Package.version}",
                )

            def test_lzma_h(self):
                exists = os.path.isfile(os.path.join(pkg_dir, "include", "lzma.h"))
                self.assertTrue(exists, "lzma.h header file exists")

            def test_lzma_a(self):
                exists = os.path.isfile(os.path.join(pkg_dir, "lib", "liblzma.a"))
                self.assertTrue(exists, "liblzma.a lib file exists")

        return PackageTestCase


pkg = PackageDescr(
    name="sqlite",
    versions=list(build_configs.keys()),
    make_pkg=make_pkg,
    create_tests=create_tests,
    sha256s={
        "https://www.sqlite.org/2021/sqlite-autoconf-3340100.tar.gz": "2a3bca581117b3b88e5361d0ef3803ba6d8da604b1c1a47d902ef785c1b53e89",
    },
)

if __name__ == "__main__":
    main_driver(pkg)
