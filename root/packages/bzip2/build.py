import glob
import os
import os.path
import subprocess
import unittest
from collections import OrderedDict
from pathlib import Path
from typing import Dict, NamedTuple, Type

from ...lib.main_driver import main_driver
from ...lib.test import BuildContext, PackageDescr, PackageTestCase


class BuildConfig(NamedTuple):
    gcc_version: str
    make_version: str


build_configs: Dict[str, BuildConfig] = OrderedDict(
    [("1.0.8", BuildConfig(gcc_version="7.5.0", make_version="4.1")),]
)


def make_pkg(ctx: BuildContext, pkg_version: str) -> None:
    build_config = build_configs[pkg_version]

    gcc_dir = ctx.devpacks_pkg("gcc", build_config.gcc_version)
    make_dir = ctx.devpacks_pkg("make", build_config.make_version)

    url = f"https://sourceware.org/pub/bzip2/bzip2-{pkg_version}.tar.gz"

    ctx.download_extract(url, ctx.tmp_dir)

    with ctx.shell.cd(ctx.tmp_dir):
        ctx.shell.env["PATH"] = os.path.pathsep.join(
            [str(Path(gcc_dir) / "bin"), str(Path(make_dir) / "bin"), ctx.shell.env["PATH"],]
        )

        ctx.shell.run("make", ["CC=gcc -fPIC"])
        ctx.shell.run("make", ["install", "PREFIX=" + str(ctx.tmp_dir / "local")])

        ctx.shell.rename("local/bin", ctx.pkg_dir / "bin")
        ctx.shell.rename("local/include", ctx.pkg_dir / "include")
        ctx.shell.rename("local/lib", ctx.pkg_dir / "lib")
        ctx.shell.rename("local/man", ctx.pkg_dir / "man")

        for f in os.listdir(ctx.pkg_dir / "bin"):
            if os.path.islink(ctx.pkg_dir / "bin" / f):
                target = os.readlink(ctx.pkg_dir / "bin" / f)
                ctx.shell.remove(ctx.pkg_dir / "bin" / f)
                ctx.shell.symlink(os.path.basename(target), ctx.pkg_dir / "bin" / f)
                ctx.shell.chmod(ctx.pkg_dir / "bin" / f, 0o755)

        ctx.shell.run("make", ["CC=gcc -fPIC", "-f", "Makefile-libbz2_so"])

        ctx.shell.mkdir(ctx.pkg_dir / "lib-dynamic")
        for so_file in glob.glob(str(ctx.tmp_dir / "*.so*")):
            ctx.shell.rename(so_file, ctx.pkg_dir / "lib-dynamic" / os.path.basename(so_file))


def create_tests(pkg_dir: Path, pkg_version: str) -> Type[PackageTestCase]:
    class TestCase(PackageTestCase):
        @unittest.skip("Broken")
        def test_bzip2(self) -> None:
            self.assert_run_program_stdout_substring(
                pkg_dir, "bzip2", ["--help"], f"Version {pkg_version}"
            )

        def test_zlib_h(self) -> None:
            self.assertTrue(os.path.isfile(pkg_dir / "include" / "bzlib.h"))

        def test_libz(self) -> None:
            self.assertTrue(os.path.isfile(pkg_dir / "lib" / "libbz2.a"))

        def test_libz_dynamic(self) -> None:
            self.assertTrue(os.path.isfile(pkg_dir / "lib-dynamic" / "libbz2.so.1.0"))

    return TestCase


pkg = PackageDescr(
    name="bzip2",
    versions=list(build_configs.keys()),
    make_pkg=make_pkg,
    create_tests=create_tests,
    sha256s={
        "https://sourceware.org/pub/bzip2/bzip2-1.0.8.tar.gz": "ab5a03176ee106d3f0fa90e381da478ddae405918153cca248e682cd0c4a2269",
    },
)

if __name__ == "__main__":
    main_driver(pkg)
