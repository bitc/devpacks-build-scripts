import os
import os.path
import subprocess

import pkglib


class Package(pkglib.PackageBuilder):
    name = "libsdl"
    version = "1.2.15"

    sha256s = {
        "https://www.libsdl.org/release/SDL-1.2.15.tar.gz": "d6d316a793e5e348155f0dd93b979798933fb98aa1edebcc108829d6474aad00"
    }

    def make_pkg(self):
        url = f"https://www.libsdl.org/release/SDL-{self.version}.tar.gz"

        self.download_root(url)

        src_dir = os.path.dirname(os.path.realpath(__file__))

        # Patch to fix <http://bugzilla.libsdl.org/show_bug.cgi?id=1769>
        with open(os.path.join(src_dir, "patch-bug-1769"), "r") as f:
            subprocess.run(["patch", "-p1"], stdin=f, cwd=self.pkg_dir, check=True)

        if self.os == "darwin":
            # Patch to fix <https://bugzilla.libsdl.org/show_bug.cgi?id=2085>
            with open(os.path.join(src_dir, "patch-bug-2085"), "r") as f:
                subprocess.run(["patch", "-p1"], stdin=f, cwd=self.pkg_dir, check=True)

        subprocess.run(
            ["./configure", "--prefix=" + os.path.join(self.pkg_dir, "local")],
            cwd=self.pkg_dir,
            check=True,
        )
        subprocess.run(["make"], cwd=self.pkg_dir, check=True)
        subprocess.run(["make", "install"], cwd=self.pkg_dir, check=True)

        os.rename(
            os.path.join(self.pkg_dir, "include"), os.path.join(self.pkg_dir, "_include"),
        )
        os.rename(
            os.path.join(self.pkg_dir, "local", "include", "SDL"),
            os.path.join(self.pkg_dir, "include"),
        )
        os.rename(
            os.path.join(self.pkg_dir, "local", "lib"), os.path.join(self.pkg_dir, "lib"),
        )

    def create_tests(self, pkg_dir):
        class PackageTestCase(pkglib.PackageTestCase):
            def test_sdl_h(self):
                exists = os.path.isfile(os.path.join(pkg_dir, "include", "SDL.h"))
                self.assertTrue(exists, "SDL.h header file exists")

            def test_libsdl_a(self):
                exists = os.path.isfile(os.path.join(pkg_dir, "lib", "libSDL.a"))
                self.assertTrue(exists, "libSDL.a lib file exists")

        return PackageTestCase


if __name__ == "__main__":
    Package().run()
