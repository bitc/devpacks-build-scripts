import glob
import os
import os.path
from collections import OrderedDict
from pathlib import Path
from typing import Dict, NamedTuple, Type

from ...lib.main_driver import main_driver
from ...lib.test import BuildContext, PackageDescr, PackageTestCase


class BuildConfig(NamedTuple):
    gcc_version: str
    make_version: str
    ncurses_version: str
    readline_version: str
    zlib_version: str
    openssl_version: str
    patchelf_version: str


build_configs: Dict[str, BuildConfig] = OrderedDict(
    [
        (
            "13.2",
            BuildConfig(
                gcc_version="10.2.0",
                make_version="4.1",
                ncurses_version="6.2",
                readline_version="8.0",
                zlib_version="1.2.11",
                openssl_version="1.1.1j",
                patchelf_version="0.12",
            ),
        ),
    ]
)


def make_pkg(ctx: BuildContext, pkg_version: str) -> None:
    build_config = build_configs[pkg_version]

    gcc_dir = ctx.devpacks_pkg("gcc", build_config.gcc_version)
    make_dir = ctx.devpacks_pkg("make", build_config.make_version)
    ncurses_dir = ctx.devpacks_pkg("ncurses", build_config.ncurses_version)
    readline_dir = ctx.devpacks_pkg("readline", build_config.readline_version)
    zlib_dir = ctx.devpacks_pkg("zlib", build_config.zlib_version)
    openssl_dir = ctx.devpacks_pkg("openssl", build_config.openssl_version)
    patchelf_prog = ctx.devpacks_pkg_prog("patchelf", build_config.patchelf_version, "patchelf")

    url = f"https://ftp.postgresql.org/pub/source/v{pkg_version}/postgresql-{pkg_version}.tar.gz"

    src_dir = ctx.tmp_dir / "src"
    ctx.shell.mkdir(src_dir)

    ctx.download_extract(url, src_dir)

    with ctx.shell.cd(src_dir):
        ctx.shell.env["PATH"] = os.path.pathsep.join(
            [str(Path(gcc_dir) / "bin"), str(Path(make_dir) / "bin"), ctx.shell.env["PATH"],]
        )

        ctx.shell.run(
            "./configure",
            [
                # Need to use "-l:libz.a" instead of "-lz" because the
                # Makefile forcibly removes the string "-lz".
                #
                # See: <https://github.com/postgres/postgres/blob/eeb60e45d82d5840b713a8741ae552238d57e8b9/src/backend/Makefile#L51>
                "LIBS=-lpthread -l:libz.a",
                f"LDFLAGS=-L{zlib_dir / 'lib'}",
                "--prefix=" + str(ctx.pkg_dir),
                "--with-zlib",
                "--with-openssl",
                "--with-readline",
                f"--with-includes={ncurses_dir / 'include'}:{readline_dir / 'include'}:{zlib_dir / 'include'}:{openssl_dir / 'include'}",
                f"--with-libraries={ncurses_dir / 'lib'}:{readline_dir / 'lib'}:{zlib_dir / 'lib'}:{openssl_dir / 'lib'}",
            ],
        )
        ctx.shell.run("make", [])
        ctx.shell.run("make", ["install-strip"])

        ctx.shell.mkdir(ctx.pkg_dir / "lib-dynamic")
        for so_file in glob.glob(str(ctx.pkg_dir / "lib" / "*.so*")):
            if not os.path.islink(so_file):
                ctx.shell.chmod(so_file, 0o755)
                ctx.shell.run(patchelf_prog, ["--set-rpath", "$ORIGIN", so_file])
            ctx.shell.rename(so_file, ctx.pkg_dir / "lib-dynamic" / os.path.basename(so_file))

        for exe_file in glob.glob(str(ctx.pkg_dir / "bin" / "*")):
            if not os.path.islink(exe_file):
                ctx.shell.run(patchelf_prog, ["--set-rpath", "$ORIGIN/../lib-dynamic", exe_file])


def create_tests(pkg_dir: Path, pkg_version: str) -> Type[PackageTestCase]:
    class TestCase(PackageTestCase):
        def test_clusterdb(self) -> None:
            self.assert_run_program(
                pkg_dir, "clusterdb", ["--version"], f"clusterdb (PostgreSQL) {pkg_version}",
            )

        def test_initdb(self) -> None:
            self.assert_run_program(
                pkg_dir, "initdb", ["--version"], f"initdb (PostgreSQL) {pkg_version}",
            )

        def test_pg_config(self) -> None:
            self.assert_run_program(
                pkg_dir, "pg_config", ["--version"], f"PostgreSQL {pkg_version}"
            )

        def test_pg_dump(self) -> None:
            self.assert_run_program(
                pkg_dir, "pg_dump", ["--version"], f"pg_dump (PostgreSQL) {pkg_version}",
            )

        def test_pg_restore(self) -> None:
            self.assert_run_program(
                pkg_dir, "pg_restore", ["--version"], f"pg_restore (PostgreSQL) {pkg_version}",
            )

        def test_postgres(self) -> None:
            self.assert_run_program(
                pkg_dir, "postgres", ["--version"], f"postgres (PostgreSQL) {pkg_version}",
            )

        def test_psql(self) -> None:
            self.assert_run_program(
                pkg_dir, "psql", ["--version"], f"psql (PostgreSQL) {pkg_version}",
            )

    return TestCase


pkg = PackageDescr(
    name="postgresql",
    versions=list(build_configs.keys()),
    make_pkg=make_pkg,
    create_tests=create_tests,
    sha256s={
        "https://ftp.postgresql.org/pub/source/v10.3/postgresql-10.3.tar.gz": "87584285af8c5fd6e599b4d9789f455f8cd55759ed81a9e575ebaebc7a03e796",
        "https://ftp.postgresql.org/pub/source/v13.2/postgresql-13.2.tar.gz": "3386a40736332aceb055c7c9012ecc665188536d874d967fcc5a33e7992f8080",
    },
)

if __name__ == "__main__":
    main_driver(pkg)
