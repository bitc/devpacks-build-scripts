import os
import platform
import shutil
import stat
import tempfile
import warnings
import weakref
from pathlib import Path
from typing import Optional


class TmpDir:
    """
    Replacement for tempfile.TemporaryDirectory that supports windows long file paths
    """

    def __init__(
        self,
        suffix: Optional[str] = None,
        prefix: Optional[str] = None,
        dir: Optional[Path] = None,
    ):
        self.name: str = tempfile.mkdtemp(suffix, prefix, dir)
        if platform.system() == "Windows":
            self.name = "\\\\?\\" + self.name
        self._finalizer = weakref.finalize(
            self, self._cleanup, self.name, warn_message="Implicitly cleaning up {!r}".format(self),
        )

    @classmethod
    def _cleanup(cls, name, warn_message) -> None:
        shutil.rmtree(name)
        warnings.warn(warn_message, ResourceWarning)

    def __repr__(self) -> str:
        return "<{} {!r}>".format(self.__class__.__name__, self.name)

    def __enter__(self) -> Path:
        return Path(self.name)

    def __exit__(self, exc, value, tb) -> None:
        self.cleanup()

    def cleanup(self) -> None:
        if self._finalizer.detach():
            # This solves the error:
            #
            #     PermissionError: [WinError 5] Access is denied:
            #
            # See: <https://stackoverflow.com/questions/2656322/shutil-rmtree-fails-on-windows-with-access-is-denied/2656405#2656405>
            def onerror(func, path, exc_info):
                if not os.access(path, os.W_OK):
                    os.chmod(path, stat.S_IWUSR)
                    func(path)
                else:
                    raise

            shutil.rmtree(self.name, onerror=onerror)
