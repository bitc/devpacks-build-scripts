import hashlib
import os
import platform
import shutil
import ssl
import stat
import subprocess
import sys
import tarfile
import tempfile
import traceback
import unittest
import urllib.request
import warnings
import weakref
import zipfile
from pathlib import Path
from tarfile import TarInfo
from typing import ClassVar, Dict, Optional, Union


def package_slug(pkg: str, version: str) -> str:
    f_os = platform.system().lower()
    f_arch = platform.machine().lower()
    if f_os == "windows" and f_arch == "amd64":
        f_arch = "x86_64"
    return f"{pkg}-{version}-{f_os}-{f_arch}"


def download_file(url: str, filename: str) -> None:
    ctx = ssl.create_default_context()

    if platform.system() == "Darwin":
        keychain_file = "/System/Library/Keychains/SystemRootCertificates.keychain"
        # <https://ss64.com/osx/security-find-cert.html>
        cadata = subprocess.run(
            ["security", "find-certificate", "-a", "-p", keychain_file],
            check=True,
            encoding="ascii",
            stdout=subprocess.PIPE,
        )
        if cadata.stdout.strip() == "":
            raise RuntimeError("Error reading root certificates from keychain", keychain_file)

        ctx.load_verify_locations(cadata=cadata.stdout)

    if platform.system() == "Linux":
        ctx.load_verify_locations(capath="/etc/ssl/certs")

    with urllib.request.urlopen(url, context=ctx) as response, open(filename, "wb") as out_file:
        shutil.copyfileobj(response, out_file)


def smart_extract(url: str, filename: str, dest_dir: str) -> None:
    # Verify that dest_dir is empty:
    if len(os.listdir(dest_dir)) > 0:
        raise RuntimeError("Directory is not empty", dest_dir)

    try:
        with zipfile.ZipFile(filename, "r") as zip_ref:
            zip_ref.extractall(dest_dir)
    except zipfile.BadZipFile as zip_err:
        try:
            with tarfile.open(filename, "r") as tar_ref:
                tar_ref.extractall(dest_dir)
        except tarfile.TarError as tar_err:
            raise RuntimeError("Error extracting downloaded file", url, zip_err, tar_err)

    # If the compressed file contained only a single directory then
    # Move all of its contents into the dest directory
    extracted_files = os.listdir(dest_dir)
    if len(extracted_files) == 1 and os.path.isdir(os.path.join(dest_dir, extracted_files[0])):
        for f in os.listdir(os.path.join(dest_dir, extracted_files[0])):
            os.rename(os.path.join(dest_dir, extracted_files[0], f), os.path.join(dest_dir, f))
        os.rmdir(os.path.join(dest_dir, extracted_files[0]))


def file_hash_sha256(filename: str) -> str:
    h = hashlib.sha256()
    with open(filename, "rb", buffering=0) as f:
        for b in iter(lambda: f.read(128 * 1024), b""):
            h.update(b)
        return h.hexdigest()


def verify_downloaded_file(sha256s: Dict[str, str], url: str, filepath: str) -> None:
    file_hash = file_hash_sha256(filepath)
    if url not in sha256s:
        raise RuntimeError(f'Missing sha256 for downloaded file:\n"{url}": "{file_hash}",\n')
    if sha256s[url] != file_hash:
        raise RuntimeError("Downloaded file hash mismatch", url, file_hash, sha256s[url])


def download_file_with_cache(sha256s: Dict[str, str], url: str, filename: str) -> None:
    cache_dir = os.path.join(os.path.expanduser("~"), ".devpacks_build_download_cache")
    if url in sha256s:
        file_hash = sha256s[url]
        if os.path.isfile(os.path.join(cache_dir, file_hash)):
            shutil.copyfile(os.path.join(cache_dir, file_hash), filename)
            return
    download_file(url, filename)
    file_hash = file_hash_sha256(filename)
    os.makedirs(cache_dir, exist_ok=True)
    shutil.copyfile(filename, os.path.join(cache_dir, file_hash))


def download_extract(sha256s: Dict[str, str], url: str, dest_dir: str) -> None:
    with TmpDir() as tmpdir:
        downloaded_file = os.path.join(tmpdir, "tmp")

        print(f"Downloading {url}")
        download_file_with_cache(sha256s, url, downloaded_file)

        print("Verifying downloaded file integrity...")
        verify_downloaded_file(sha256s, url, downloaded_file)

        print(f"Extracting...")
        smart_extract(url, downloaded_file, dest_dir)
        print(f"Succesfully extracted into {dest_dir}")


def download_verify_file(sha256s: Dict[str, str], url: str, dest_file: str) -> None:
    print(f"Downloading {url}")
    download_file_with_cache(sha256s, url, dest_file)
    print("Verifying downloaded file integrity...")
    verify_downloaded_file(sha256s, url, dest_file)


def break_all_hardlinks(dir: Path) -> None:
    for root, path, files in os.walk(dir, topdown=False):
        for file in files:
            current_file = Path(root) / file
            if current_file.exists() and os.stat(current_file).st_nlink > 1:
                # Arbitrary random string that is (hopefully) unique
                shutil.copyfile(current_file, str(current_file) + ".tmp247749953838")
                os.chmod(str(current_file) + ".tmp247749953838", os.stat(current_file).st_mode)
                os.remove(current_file)
                os.rename(str(current_file) + ".tmp247749953838", current_file)


def create_tarball(src_dir: Path, outfile: Path) -> None:
    """
    Will create a tar.xz file in tar PAX_FORMAT
    """
    if not str(outfile).endswith(".tar.xz"):
        raise RuntimeError(f'Tried to create tarball "{outfile}" without .tar.xz extension')

    break_all_hardlinks(src_dir)

    # Reset and normalize all file metadata
    def tarinfo_reset(tarinfo: TarInfo) -> Optional[TarInfo]:
        tarinfo.uid = tarinfo.gid = 0
        tarinfo.uname = tarinfo.gname = "root"
        tarinfo.mtime = 0
        if not (tarinfo.isdir() or tarinfo.isfile() or tarinfo.issym()):
            raise RuntimeError(
                f"File is an invalid type ({str(tarinfo.type)}) and cannot be archived",
                tarinfo.name,
            )
        if tarinfo.isdir():
            tarinfo.mode = 0o755
        else:
            if (
                tarinfo.mode & stat.S_IXUSR != 0
                or tarinfo.mode & stat.S_IXGRP != 0
                or tarinfo.mode & stat.S_IXOTH != 0
            ):
                tarinfo.mode = 0o755
            else:
                tarinfo.mode = 0o644
        return tarinfo

    try:
        with tarfile.open(outfile, "w:xz", format=tarfile.PAX_FORMAT, dereference=False) as tar:
            tar.add(str(src_dir), arcname=os.path.basename(src_dir), filter=tarinfo_reset)
    except:
        try:
            os.remove(outfile)
        except OSError:
            pass
        raise


class PackageBuilder:
    name: ClassVar[str]
    version: ClassVar[str]
    sha256s: ClassVar[Dict[str, str]]

    def make_pkg(self) -> None:
        raise NotImplementedError()

    def create_tests(self, pkg_dir):
        return None

    def download_root(self, url: str) -> None:
        download_extract(self.sha256s, url, self.pkg_dir)

    def download_file(self, url: str, dest_file: str) -> None:
        download_verify_file(self.sha256s, url, dest_file)

    def download_extract(self, url, dest_dir):
        download_extract(self.sha256s, url, dest_dir)

    def debug_pause(self):
        print()
        print("You requested a pause.")
        print("Package dir is:")
        print(self.pkg_dir)
        print()
        print("Press enter to continue")
        input()

    def run(self):
        if not self.name:
            raise NotImplementedError('Subclass did not set the package "name"')
        if not self.version:
            raise NotImplementedError('Subclass did not set the package "version"')
        if self.sha256s is None:
            raise NotImplementedError('Subclass did not set the package "sha256s"')

        self.os = platform.system().lower()
        self.arch = platform.machine().lower()
        if self.os == "windows" and self.arch == "amd64":
            self.arch = "x86_64"

        with TmpDir() as tmpdir:
            self.pkg_dir = os.path.join(tmpdir, package_slug(self.name, self.version))
            os.mkdir(self.pkg_dir)

            def pause_after_error():
                print("Package dir is:")
                print(self.pkg_dir)
                print()
                print("Pausing so you can inspect things.")
                print(
                    "Press enter when you are finished to complete the deletion of the package directory"
                )
                input()

            print(f"Building {package_slug(self.name, self.version)}")
            try:
                self.make_pkg()
            except Exception as err:
                if sys.stdout.isatty():
                    print()
                    print("Error building package.")
                    traceback.print_tb(err.__traceback__)
                    print(err)
                    print()
                    pause_after_error()
                raise
            print("Package built")

            print()
            test_cases = self.create_tests(self.pkg_dir)
            if test_cases:
                print("Running tests...")
                print()
                runner = unittest.TextTestRunner(verbosity=2)
                result = runner.run(unittest.defaultTestLoader.loadTestsFromTestCase(test_cases))
                if not result.wasSuccessful():
                    if sys.stdout.isatty():
                        print("Tests failed.")
                        pause_after_error()
                    raise RuntimeError("Tests failed")
            else:
                print("Warning: No test cases. Package may be broken")

            print()
            print(f"Packaging up final tarball...")
            tarball_file = os.path.join("dist", package_slug(self.name, self.version) + ".tar.xz")
            create_tarball(self.pkg_dir, tarball_file)
            print(f"Created package file:")
            print(f"{tarball_file}")
        self.pkg_dir = None


def strip_win_extended_path(path: Path) -> Path:
    if platform.system().lower() != "windows":
        return path

    path_str = str(path)
    prefix = "\\\\?\\"
    if path_str.startswith(prefix):
        return Path(path_str[len(prefix) :])
    else:
        return path


def prog_file(filepath: str) -> str:
    if platform.system().lower() == "windows":
        if os.path.isfile(filepath):
            return str(strip_win_extended_path(Path(filepath)))
        if os.path.isfile(filepath + ".exe"):
            return str(strip_win_extended_path(Path(filepath + ".exe")))
        if os.path.isfile(filepath + ".cmd"):
            return str(strip_win_extended_path(Path(filepath + ".cmd")))
        if os.path.isfile(filepath + ".bat"):
            return str(strip_win_extended_path(Path(filepath + ".bat")))
    return str(strip_win_extended_path(Path(filepath)))


class PackageTestCase(unittest.TestCase):
    def assert_run_program(self, pkg_dir, prog, args, expected_stdout):
        cmd = subprocess.run(
            [prog_file(os.path.join(pkg_dir, "bin", prog))] + args,
            check=True,
            encoding="utf-8",
            stdout=subprocess.PIPE,
        )
        self.assertEqual(expected_stdout, cmd.stdout.strip())

    def assert_run_program_first_line(self, pkg_dir, prog, args, expected_stdout):
        cmd = subprocess.run(
            [prog_file(os.path.join(pkg_dir, "bin", prog))] + args,
            check=True,
            encoding="utf-8",
            stdout=subprocess.PIPE,
        )
        self.assertEqual(expected_stdout, cmd.stdout.strip().split("\n")[0])

    def run_program(self, pkg_dir, prog, args):
        """
        Returns the stdout of the program
        """
        cmd = subprocess.run(
            [prog_file(os.path.join(pkg_dir, "bin", prog))] + args,
            check=True,
            encoding="utf-8",
            stdout=subprocess.PIPE,
        )
        return cmd.stdout.strip()


class TmpDir:
    """
    Replacement for tempfile.TemporaryDirectory that supports windows long file paths
    """

    def __init__(self, suffix=None, prefix=None, dir=None):
        self.name: str = tempfile.mkdtemp(suffix, prefix, dir)
        if platform.system() == "Windows":
            self.name = "\\\\?\\" + self.name
        self._finalizer = weakref.finalize(
            self, self._cleanup, self.name, warn_message="Implicitly cleaning up {!r}".format(self),
        )

    @classmethod
    def _cleanup(cls, name, warn_message):
        shutil.rmtree(name)
        warnings.warn(warn_message, ResourceWarning)

    def __repr__(self):
        return "<{} {!r}>".format(self.__class__.__name__, self.name)

    def __enter__(self) -> str:
        return self.name

    def __exit__(self, exc, value, tb):
        self.cleanup()

    def cleanup(self):
        if self._finalizer.detach():
            shutil.rmtree(self.name)
