import argparse
from pathlib import Path
from typing import Optional

from .test import PackageDescr, build_package

parser = argparse.ArgumentParser()
parser.set_defaults(cmd="default")
subparsers = parser.add_subparsers(help="sub-command help")

parser_build = subparsers.add_parser("build", help="Build the package (default)")
parser_build.set_defaults(cmd="build")
parser_build.add_argument(
    "--pkg-version",
    dest="pkg_version",
    metavar="VERSION",
    help="Specify which version of the package to build (default is the latest)",
)

parser_list_pkg_versions = subparsers.add_parser(
    "list-pkg-versions", help="List all of the versions that are available for the package",
)
parser_list_pkg_versions.set_defaults(cmd="list-pkg-versions")

parser_run_tests = subparsers.add_parser("run-tests", help="Run the test suite of a built package")
parser_run_tests.set_defaults(cmd="run-tests")
parser_run_tests.add_argument("pkg_file", metavar="FILE", help="Package tarball file")
parser_run_tests.add_argument(
    "--pkg-version",
    dest="pkg_version",
    required=True,
    metavar="VERSION",
    help="Must match the version of the tarball file",
)


def main_driver(pkg: PackageDescr) -> None:
    args = parser.parse_args()
    if args.cmd == "default":
        build(pkg, None)
    elif args.cmd == "build":
        build(pkg, args.pkg_version)
    elif args.cmd == "list-pkg-versions":
        list_versions(pkg)
    elif args.cmd == "run-tests":
        run_tests(pkg, args.pkg_file, args.pkg_version)
    else:
        raise RuntimeError(f"Internal Error: Unknown cmd: {args.cmd}")


def build(pkg: PackageDescr, pkg_version: Optional[str] = None) -> None:
    tarball = build_package(pkg, pkg_version)
    print(tarball)


def list_versions(pkg: PackageDescr) -> None:
    print("\n".join(pkg.versions))


def run_tests(pkg: PackageDescr, file: Path, pkg_version: Optional[str] = None) -> None:
    print("Running tests", file, pkg_version)
    raise RuntimeError("TODO")
