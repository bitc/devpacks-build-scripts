import os.path
import textwrap
from typing import NamedTuple

from ....lib.pkglib import strip_win_extended_path
from ....lib.test import BuildContext


class HackageBuildConfig(NamedTuple):
    ghc_version: str
    cabal_install_version: str


def make_hackage_pkg(
    ctx: BuildContext, pkg_name: str, pkg_version: str, hackage_build_config: HackageBuildConfig,
) -> None:

    ghc_prog = ctx.devpacks_pkg_prog("ghc", hackage_build_config.ghc_version, "ghc")
    cabal_prog = ctx.devpacks_pkg_prog(
        "haskell_cabal_install", hackage_build_config.cabal_install_version, "cabal"
    )

    pkg_url = f"https://hackage.haskell.org/package/{pkg_name}-{pkg_version}/{pkg_name}-{pkg_version}.tar.gz"

    os.mkdir(ctx.tmp_dir / "src")
    ctx.download_extract(pkg_url, ctx.tmp_dir / "src")

    ctx.shell.env["CABAL_DIR"] = f"{strip_win_extended_path(ctx.tmp_dir / 'cabal')}"

    with ctx.shell.cd(strip_win_extended_path(ctx.tmp_dir / "src")):
        ctx.shell.run(cabal_prog, ["v2-update"])
        ctx.shell.run(cabal_prog, ["v2-build", f"--with-ghc={ghc_prog}"])

        if ctx.os == "windows":
            which_cmd = "where"
        else:
            which_cmd = "which"

        output_file = (
            ctx.shell.cmd_stdout(
                cabal_prog, ["v2-exec", f"--with-ghc={ghc_prog}", which_cmd, pkg_name]
            )
            .strip()
            .split("\n")[-1]
        )

        ctx.shell.rename(output_file, ctx.pkg_dir / os.path.basename(output_file))

        if os.path.isdir(ctx.tmp_dir / "src" / "data"):
            ctx.shell.rename(ctx.tmp_dir / "src" / "data", ctx.pkg_dir / "data")

        ctx.shell.mkdir(ctx.pkg_dir / "bin")
        ctx.shell.write_file(
            ctx.pkg_dir / "bin" / pkg_name,
            textwrap.dedent(
                f"""\
                #!/bin/sh
                DIR=`dirname "$0"`
                export {pkg_name}_datadir="$DIR/../data"
                exec "$DIR"/../{os.path.basename(output_file)} "$@"
                """
            ),
        )
        os.chmod(ctx.pkg_dir / "bin" / pkg_name, 0o775)
