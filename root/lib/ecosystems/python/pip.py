import distutils.version
import os
import os.path
import shutil
import subprocess
import textwrap
from pathlib import Path
from typing import ClassVar, Dict, List, NamedTuple

from ....lib.pkglib import package_slug
from ....lib.test import BuildContext, PackageDescr, PackageTestCase


class PipBuildConfig(NamedTuple):
    python_version: str
    pip_version: str
    setuptools_version: str


def make_pip_pkg(
    ctx: BuildContext,
    pkg_name: str,
    pkg_version: str,
    pip_build_config: PipBuildConfig,
    programs: List[str] = None,
) -> None:
    if programs is None:
        programs = [pkg_name]

    python_dir = ctx.devpacks_pkg("python", pip_build_config.python_version)
    python_prog = ctx.devpacks_pkg_prog("python", pip_build_config.python_version, "python")

    pip_url = f"https://pypi.org/packages/source/p/pip/pip-{pip_build_config.pip_version}.tar.gz"
    setuptools_url = f"https://pypi.org/packages/source/s/setuptools/setuptools-{pip_build_config.setuptools_version}.zip"

    shutil.copytree(
        python_dir,
        ctx.pkg_dir / package_slug("python", pip_build_config.python_version),
        symlinks=True,
    )

    print(f"Extracting pip and setuptools into: {ctx.tmp_dir}")

    os.mkdir(ctx.tmp_dir / "pip")
    ctx.download_extract(pip_url, ctx.tmp_dir / "pip")

    os.mkdir(ctx.tmp_dir / "setuptools")
    ctx.download_extract(setuptools_url, ctx.tmp_dir / "setuptools")

    ctx.shell.env["PYTHONPATH"] = os.path.pathsep.join(
        [str(ctx.tmp_dir / "pip" / "src"), str(ctx.tmp_dir / "setuptools")]
    )

    prefix = []
    if ctx.os == "windows":
        prefix = ["--prefix", "prefix"]

    ctx.shell.run(
        python_prog,
        ["-m", "pip", "install"]
        + prefix
        + ["--root", str(ctx.pkg_dir), pkg_name + "==" + pkg_version],
    )

    if ctx.os == "windows":
        os.rename(ctx.pkg_dir / "prefix", ctx.pkg_dir / package_slug(pkg_name, pkg_version))
    else:
        os.rename(ctx.pkg_dir / "usr" / "local", ctx.pkg_dir / package_slug(pkg_name, pkg_version))
        os.rmdir(ctx.pkg_dir / "usr")

    os.makedirs(ctx.pkg_dir / "bin", exist_ok=True)

    if ctx.os == "windows":
        site_packages_dir = Path(package_slug(pkg_name, pkg_version)) / "Lib" / "site-packages"
    else:
        python_version = distutils.version.LooseVersion(pip_build_config.python_version)
        python_major = python_version.version[0]
        python_minor = python_version.version[1]
        site_packages_dir = (
            Path(package_slug(pkg_name, pkg_version))
            / "lib"
            / f"python{python_major}.{python_minor}"
            / "site-packages"
        )
    if not os.path.isdir(ctx.pkg_dir / site_packages_dir):
        raise RuntimeError(f'Couldn\'t find "site-packages" directory for {pkg_name}')

    # Create wrapper scripts for running the pkg
    for prog in programs:
        if ctx.os == "windows":
            script_path = None
            for target_search in [
                Path(package_slug(pkg_name, pkg_version)) / "Scripts" / (prog + ".exe"),
                Path(package_slug(pkg_name, pkg_version)) / "Scripts" / prog,
            ]:
                if os.path.isfile(ctx.pkg_dir / target_search):
                    script_path = str(target_search)
                    break
            if script_path is None:
                raise RuntimeError(f'Couldn\'t find script for "{prog}" in "Scripts"')

            ctx.shell.write_file(
                Path("bin") / (prog + ".cmd"),
                textwrap.dedent(
                    f"""\
                    @ECHO OFF
                    SETLOCAL
                    SET "PYTHONPATH=%~dp0..\\{site_packages_dir}"
                    SET "PYTHON_EXE=%~dp0..\\{package_slug('python', pip_build_config.python_version)}\\bin\\python"
                    SET "SCRIPT_PATH=%~dp0..\\{script_path}"
                    "%PYTHON_EXE%" "%SCRIPT_PATH%" %*
                    """
                ),
            )
        else:
            ctx.shell.write_file(
                Path("bin") / prog,
                textwrap.dedent(
                    f"""\
                    #!/bin/sh
                    DIR=`dirname "$0"`
                    PYTHONPATH="$DIR"/../{site_packages_dir} exec "$DIR"/../{package_slug('python', pip_build_config.python_version)}/bin/python "$DIR"/../{package_slug(pkg_name, pkg_version)}/bin/{prog} "$@"
                    """
                ),
            )
            os.chmod(ctx.pkg_dir / "bin" / prog, 0o775)


pip_sha256s: Dict[str, str] = {
    "https://pypi.org/packages/source/p/pip/pip-10.0.1.tar.gz": "f2bd08e0cd1b06e10218feaf6fef299f473ba706582eb3bd9d52203fdbd7ee68",
    "https://pypi.org/packages/source/p/pip/pip-20.1.1.tar.gz": "27f8dc29387dd83249e06e681ce087e6061826582198a425085e0bf4c1cf3a55",
    "https://pypi.org/packages/source/p/pip/pip-21.0.1.tar.gz": "99bbde183ec5ec037318e774b0d8ae0a64352fe53b2c7fd630be1d07e94f41e5",
    "https://pypi.org/packages/source/s/setuptools/setuptools-39.2.0.zip": "f7cddbb5f5c640311eb00eab6e849f7701fa70bf6a183fc8a2c33dd1d1672fb2",
    "https://pypi.org/packages/source/s/setuptools/setuptools-49.2.0.zip": "afe9e81fee0270d3f60d52608549cc8ec4c46dada8c95640c1a00160f577acf2",
    "https://pypi.org/packages/source/s/setuptools/setuptools-50.3.2.zip": "ed0519d27a243843b05d82a5e9d01b0b083d9934eaa3d02779a23da18077bd3c",
}
