import glob
import os
import stat
import subprocess
import tarfile
from typing import List, NamedTuple

from .tmp_dir import TmpDir


def create_tar_gz(src_dir: str, outfile: str) -> None:
    # Reset and normalize all file metadata
    def tarinfo_reset(tarinfo):
        tarinfo.uid = tarinfo.gid = 0
        tarinfo.uname = tarinfo.gname = "root"
        tarinfo.mtime = 0
        if not (tarinfo.isdir() or tarinfo.isfile() or tarinfo.issym()):
            raise RuntimeError(
                f"File is an invalid type ({tarinfo.type}) and cannot be archived", tarinfo.name,
            )
        if tarinfo.isdir():
            tarinfo.mode = 0o755
        else:
            if (
                tarinfo.mode & stat.S_IXUSR != 0
                or tarinfo.mode & stat.S_IXGRP != 0
                or tarinfo.mode & stat.S_IXOTH != 0
            ):
                tarinfo.mode = 0o755
            else:
                tarinfo.mode = 0o644
        return tarinfo

    try:
        with tarfile.open(outfile, "w:gz", format=tarfile.PAX_FORMAT, dereference=False) as tar:
            tar.add(src_dir, arcname=os.path.basename(src_dir), filter=tarinfo_reset)
    except:
        try:
            os.remove(outfile)
        except OSError:
            pass
        raise


class SSHConfig(NamedTuple):
    hostname: str
    flags: List[str]


def remote_run(ssh_config: SSHConfig, package: str, pkg_version: str) -> None:
    print("Creating remote tmp dir")
    remote_tmp_dir = subprocess.run(
        ["ssh"] + ssh_config.flags + [ssh_config.hostname] + ["mktemp -d"],
        check=True,
        encoding="utf-8",
        stdout=subprocess.PIPE,
    ).stdout.strip()
    try:
        with TmpDir() as tmpdir:
            bundle_file = tmpdir / "root.tar.gz"
            print("Creating bundle")
            create_tar_gz("root", str(bundle_file))
            print("Uploading bundle")
            subprocess.run(
                ["scp"]
                + ssh_config.flags
                + [str(bundle_file), f"{ssh_config.hostname}:{remote_tmp_dir}/root.tar.gz"],
                check=True,
            )

        print("Extracting remote bundle")
        subprocess.run(
            ["ssh"]
            + ssh_config.flags
            + [ssh_config.hostname]
            + [f"tar xf {remote_tmp_dir}/root.tar.gz -C {remote_tmp_dir}",],
            check=True,
        )

        print("Running build")
        subprocess.run(
            ["ssh"]
            + ssh_config.flags
            + [ssh_config.hostname]
            + [
                f"cd {remote_tmp_dir} ; `devpacks quick-pkg-path python -t 3.6.4 -p python` -u -m root.packages.{package}.build build --pkg-version {pkg_version}",
            ],
            check=True,
        )

        print("Downloading artifact")
        subprocess.run(
            ["scp"]
            + ssh_config.flags
            + ["-r", f"{ssh_config.hostname}:{remote_tmp_dir}/dist/*", "dist"],
            check=True,
        )
    finally:
        print("Cleaning up")
        subprocess.run(
            ["ssh"] + ssh_config.flags + [ssh_config.hostname] + [f"rm -rf {remote_tmp_dir}"],
            check=True,
        )


def remote_build_and_upload(ssh_config: SSHConfig, package: str, pkg_version: str) -> None:
    remote_run(ssh_config, package, pkg_version)
    files = glob.glob(f"./dist/{package}-{pkg_version}-*.tar.xz")
    aws_prog = subprocess.run(
        ["devpacks", "quick-pkg-path", "awscli", "-t", "1.15.42", "-p", "aws"],
        check=True,
        encoding="utf-8",
        stdout=subprocess.PIPE,
    ).stdout
    for file in files:
        subprocess.run(
            [aws_prog, "s3", "cp", "--acl", "public-read"]
            + [file]
            + [f"s3://devpacks-0/{package}/"],
            check=True,
        )


if __name__ == "__main__":
    debian_ssh_config = SSHConfig(hostname="default", flags=["-F", "vagrant-ssh-config"])

    # remote_build_and_upload("debian8@192.168.14.19", "ghc", "8.10.1")
    # remote_build_and_upload("viper@192.168.14.11", "python", "2.7.15")

    # remote_build_and_upload(debian_ssh_config, "libmpfr", "4.1.0")
    # remote_build_and_upload(debian_ssh_config, "libmpc", "1.1.0")
    # remote_build_and_upload(debian_ssh_config, "gawk", "4.0.2")
    # remote_build_and_upload(debian_ssh_config, "binutils", "2.34")

    # remote_build_and_upload(debian_ssh_config, "libglibc", "2.13")
    # remote_build_and_upload(debian_ssh_config, "make", "3.82")
    remote_build_and_upload(debian_ssh_config, "gcc", "10.2.0")
