import os
import platform
import shutil
import subprocess
import sys
import textwrap
import traceback
import unittest
from pathlib import Path
from typing import Callable, Dict, List, NamedTuple, Optional, Type, Union

from .pkglib import (
    create_tarball,
    download_extract,
    download_verify_file,
    package_slug,
    prog_file,
)
from .tmp_dir import TmpDir


class PackageTestCase(unittest.TestCase):
    def assert_run_program(self, pkg_dir, prog, args, expected_stdout) -> None:
        cmd = subprocess.run(
            [prog_file(os.path.join(pkg_dir, "bin", prog))] + args,
            check=True,
            encoding="utf-8",
            stdout=subprocess.PIPE,
        )
        self.assertEqual(expected_stdout, cmd.stdout.strip())

    def run_program(self, pkg_dir: Path, prog: str, args: List[str]) -> str:
        """
        Returns the stdout of the program
        """
        cmd = subprocess.run(
            [prog_file(os.path.join(pkg_dir, "bin", prog))] + args,
            check=True,
            encoding="utf-8",
            stdout=subprocess.PIPE,
        )
        return cmd.stdout.strip()

    def assert_run_program_stdout_substring(
        self, pkg_dir: Path, prog: str, args: List[str], substr: str
    ) -> None:
        cmd = subprocess.run(
            [prog_file(os.path.join(pkg_dir, "bin", prog))] + args,
            check=True,
            encoding="utf-8",
            stdout=subprocess.PIPE,
        )
        self.assertIn(substr, cmd.stdout.strip())

    def assert_run_program_first_line(
        self, pkg_dir: Path, prog: str, args: List[str], expected_stdout: str
    ) -> None:
        cmd = subprocess.run(
            [prog_file(os.path.join(pkg_dir, "bin", prog))] + args,
            check=True,
            encoding="utf-8",
            stdout=subprocess.PIPE,
        )
        self.assertEqual(expected_stdout, cmd.stdout.strip().split("\n")[0])

    def assert_run_program_first_line_prefix(
        self, pkg_dir: Path, prog: str, args: List[str], expected_stdout_prefix: str
    ) -> None:
        cmd = subprocess.run(
            [prog_file(os.path.join(pkg_dir, "bin", prog))] + args,
            check=True,
            encoding="utf-8",
            stdout=subprocess.PIPE,
        )
        self.assertEqual(
            expected_stdout_prefix,
            cmd.stdout.strip().split("\n")[0][0 : len(expected_stdout_prefix)],
        )


def echo_cmd(prog: str, args: List[str]):
    print("+", prog, args)


class ShellContext:
    def __init__(self, cwd: Path):
        self.cwd: Path = cwd
        self.env: Dict[str, str] = dict(os.environ)

    def cd(self, dir: Union[Path, str]):
        print("[+] cd", dir)

        class DirContext:
            def __init__(self, shell_context):
                self.shell_context = shell_context
                self.saved_cwd = shell_context.cwd

            def __enter__(self) -> None:
                pass

            def __exit__(self, exc, value, tb) -> None:
                self.shell_context.cwd = self.saved_cwd

        dir_context = DirContext(self)
        self.cwd = self.cwd / dir
        return dir_context

    def cmd_stdout(self, prog: Union[Path, str], args: List[str]) -> str:
        echo_cmd(str(prog), args)
        return subprocess.run(
            [str(prog)] + args,
            cwd=self.cwd,
            env=self.env,
            check=True,
            encoding="utf-8",
            stdout=subprocess.PIPE,
        ).stdout

    def run(self, prog: Union[Path, str], args: List[str]) -> None:
        echo_cmd(str(prog), args)
        subprocess.run([str(prog)] + args, cwd=self.cwd, env=self.env, check=True)

    def chmod(self, src: Union[Path, str], mode: int) -> None:
        print("[+] chmod", src, oct(mode))
        os.chmod(self.cwd / src, mode)

    def rename(self, src: Union[Path, str], dst: Union[Path, str]) -> None:
        print("[+] rename", src, "-->", dst)
        os.rename(self.cwd / src, self.cwd / dst)

    def copy(self, src: Union[Path, str], dst: Union[Path, str]) -> None:
        print("[+] copy", src, "-->", dst)
        shutil.copyfile(self.cwd / src, self.cwd / dst)
        os.chmod(self.cwd / dst, os.stat(self.cwd / src).st_mode)

    def copytree(
        self,
        src: Union[Path, str],
        dst: Union[Path, str],
        symlinks=False,
        ignore_dangling_symlinks=False,
    ) -> None:
        print("[+] copytree", src, "-->", dst)
        shutil.copytree(
            src, dst, symlinks=symlinks, ignore_dangling_symlinks=ignore_dangling_symlinks
        )

    def remove(self, path: Union[Path, str]) -> None:
        print("[+] remove", path)
        os.remove(self.cwd / path)

    def rmtree(self, path: Union[Path, str]) -> None:
        print("[+] rmtree", path)
        shutil.rmtree(self.cwd / path)

    def write_file(self, path: Union[Path, str], contents: str) -> None:
        print("[+] Writing File:", path)
        with open(self.cwd / path, "w", encoding="utf-8") as f:
            f.write(contents)

    def append_file(self, path: Union[Path, str], contents: str) -> None:
        print("[+] Appending File:", path)
        with open(self.cwd / path, "a", encoding="utf-8") as f:
            f.write(contents)

    def mkdir(self, path: Union[Path, str]) -> None:
        print("[+] mkdir", path)
        os.mkdir(self.cwd / path)

    def symlink(self, src: str, dst: Union[Path, str]) -> None:
        """Create a symbolic link pointing to `src` named `dst`

        Parameters:
        src (str): Should be a relative path (may include ".." segments)
        dst: The name of the symbolic link
        """
        print("[+] symlink", dst, "-->", src)
        os.symlink(src, self.cwd / dst)

    def create_exe_shortcut(self, src: Union[Path, str], dst: Union[Path, str]) -> None:
        print("[+] exe shortcut", src, "-->", dst)
        with open(self.cwd / dst, "w", encoding="utf-8") as f:
            f.write(
                textwrap.dedent(
                    f"""\
                #!/bin/sh
                exec {self.cwd / src} "$@"
                """
                )
            )
        os.chmod(self.cwd / dst, 0o775)


class BuildContext:
    def __init__(self, tmp_dir: Path, pkg_dir: Path, sha256s: Dict[str, str]):
        self.tmp_dir: Path = tmp_dir
        self.pkg_dir: Path = pkg_dir
        self._sha256s = sha256s
        self.os: str = platform.system().lower()
        self.arch: str = platform.machine().lower()
        if self.os == "windows" and self.arch == "amd64":
            self.arch = "x86_64"

        self.shell = ShellContext(pkg_dir)
        self.use_system_gcc: bool = False

    def debug_pause(self):
        print()
        print("You requested a pause.")
        print("Package dir is:")
        print(self.pkg_dir)
        print()
        print("Press enter to continue")
        input()

    def devpacks_pkg(self, pkg_name: str, pkg_version: str) -> Path:
        return Path(
            self.shell.cmd_stdout("devpacks", ["quick-pkg-path", pkg_name, "-t", pkg_version])
        )

    def devpacks_pkg_prog(self, pkg_name: str, pkg_version: str, prog: str) -> Path:
        return Path(
            self.shell.cmd_stdout(
                "devpacks", ["quick-pkg-path", pkg_name, "-t", pkg_version, "-p", prog]
            )
        )

    def download_file(self, url: str, dest_file: Path) -> None:
        download_verify_file(self._sha256s, url, str(dest_file))

    def download_extract(self, url: str, dest_dir: Path) -> None:
        download_extract(self._sha256s, url, str(dest_dir))

    def download_root(self, url: str) -> None:
        download_extract(self._sha256s, url, str(self.pkg_dir))


class PackageDescr(NamedTuple):
    name: str
    versions: List[str]
    make_pkg: Callable[[BuildContext, str], None]
    create_tests: Callable[[Path, str], Type[PackageTestCase]]
    sha256s: Dict[str, str]


def make_pkg(ctx: BuildContext, version: str) -> None:
    print("hi")


def create_tests(pkg_dir: Path, pkg_version: str) -> Type[PackageTestCase]:
    class TestCase(PackageTestCase):
        pass

    return TestCase


p = PackageDescr(
    name="mypy", versions=["1", "1", "1"], make_pkg=make_pkg, create_tests=create_tests, sha256s={},
)


def build_package(pkg: PackageDescr, version: Optional[str] = None) -> Path:
    """
    Returns the Path of the tarball file that was created
    """
    if version is None:
        version = pkg.versions[-1]

    print(f"Building {package_slug(pkg.name, version)}")

    with TmpDir() as tmpdir:
        tmp_dir = tmpdir / "tmp"
        pkg_dir = tmpdir / package_slug(pkg.name, version)
        os.mkdir(tmp_dir)
        os.mkdir(pkg_dir)

        def pause_after_error() -> None:
            print("Package dir is:")
            print(pkg_dir)
            print()
            print("Pausing so you can inspect things.")
            print(
                "Press enter when you are finished to complete the deletion of the package directory"
            )
            input()

        ctx = BuildContext(tmp_dir, pkg_dir, pkg.sha256s)
        try:
            pkg.make_pkg(ctx, version)
        except Exception as err:
            if True or sys.stdout.isatty():
                print()
                print("Error building package.")
                traceback.print_tb(err.__traceback__)
                print(err)
                print()
                pause_after_error()
            raise
        print("Package built")

        print()

        test_cases = pkg.create_tests(pkg_dir, version)

        print("Running tests...")
        print()
        runner = unittest.TextTestRunner(verbosity=2)
        result = runner.run(unittest.defaultTestLoader.loadTestsFromTestCase(test_cases))
        if not result.wasSuccessful():
            if True or sys.stdout.isatty():
                print("Tests failed.")
                pause_after_error()
            raise RuntimeError("Tests failed")

        sys.stdout.flush()
        sys.stderr.flush()

        os.makedirs("dist", exist_ok=True)
        tarball_file = Path("dist") / (package_slug(pkg.name, version) + ".tar.xz")
        print(f"Packaging up final tarball...")
        create_tarball(pkg_dir, tarball_file)
        print(f"Created package file:")
        print(f"{tarball_file}")

    return tarball_file
