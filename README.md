# devpacks build scripts

## Requirements

### Ubuntu Linux

You must install the `devpacks` binary somewhere in your `PATH`

Required system packages:

    # apt install build-essential
    # apt install git

## Instructions

Currently a bit hacky. You must have `python` in your path.
Replace "xz" with the name of package:

    $ PYTHONPATH=`pwd`/lib python packages/xz/build.py

On Windows:

    > set "PYTHONPATH=%cd%\lib"
    > python packages\xz\build.py


## Updated Instructions!

    $ `devpacks quick-pkg-path python-mypy -t 0.770 -p mypy` -p root --show-column-numbers
    $ `devpacks quick-pkg-path python-black -t 19.10b0 -p black` .
    $ `devpacks quick-pkg-path python-isort -t 5.2.0 -p isort` --py 36 .
    $ `devpacks quick-pkg-path python -t 3.6.4 -p python` -m root.packages.xz.build
